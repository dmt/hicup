#pragma once

#define __HIP_PLATFORM_AMD__
#define ROCM_MATHLIBS_API_USE_HIP_COMPLEX

#include "cuComplex.h"

#include <hipblas/hipblas.h>

#define cublasOperation_t              hipblasOperation_t
#define CUBLAS_OP_N                    HIPBLAS_OP_N
#define CUBLAS_OP_T                    HIPBLAS_OP_T
#define CUBLAS_OP_C                    HIPBLAS_OP_C
#define CUBLAS_OP_HERMITAN             HIPBLAS_OP_C
#define CUBLAS_OP_CONJG                HIPBLAS_OP_CONJG
  // Blas statuse
#define cublasStatus                   hipblasStatus_t
#define cublasStatus_t                 hipblasStatus_t
#define CUBLAS_STATUS_SUCCESS          HIPBLAS_STATUS_SUCCESS
#define CUBLAS_STATUS_NOT_INITIALIZED  HIPBLAS_STATUS_NOT_INITIALIZED
#define CUBLAS_STATUS_ALLOC_FAILED     HIPBLAS_STATUS_ALLOC_FAILED
#define CUBLAS_STATUS_INVALID_VALUE    HIPBLAS_STATUS_INVALID_VALUE
#define CUBLAS_STATUS_MAPPING_ERROR    HIPBLAS_STATUS_MAPPING_ERROR
#define CUBLAS_STATUS_EXECUTION_FAILED HIPBLAS_STATUS_EXECUTION_FAILED
#define CUBLAS_STATUS_INTERNAL_ERROR   HIPBLAS_STATUS_INTERNAL_ERROR
#define CUBLAS_STATUS_NOT_SUPPORTED    HIPBLAS_STATUS_NOT_SUPPORTED
#define CUBLAS_STATUS_ARCH_MISMATCH    HIPBLAS_STATUS_ARCH_MISMATCH
#define CUBLAS_STATUS_LICENSE_ERROR    HIPBLAS_STATUS_UNKNOWN
  // Blas Fill Mode
#define cublasFillMode_t               hipblasFillMode_t
#define CUBLAS_FILL_MODE_LOWER         HIPBLAS_FILL_MODE_LOWER
#define CUBLAS_FILL_MODE_UPPER         HIPBLAS_FILL_MODE_UPPER
#define CUBLAS_FILL_MODE_FULL          HIPBLAS_FILL_MODE_FULL
  // Blas Diag Type
#define cublasDiagType_t               hipblasDiagType_t
#define CUBLAS_DIAG_NON_UNIT           HIPBLAS_DIAG_NON_UNIT
#define CUBLAS_DIAG_UNIT               HIPBLAS_DIAG_UNIT
  // Blas Side Mode
#define cublasSideMode_t               hipblasSideMode_t
#define CUBLAS_SIDE_LEFT               HIPBLAS_SIDE_LEFT
#define CUBLAS_SIDE_RIGHT              HIPBLAS_SIDE_RIGHT
  // Blas Pointer Mode
#define cublasPointerMode_t            hipblasPointerMode_t
#define CUBLAS_POINTER_MODE_HOST       HIPBLAS_POINTER_MODE_HOST
#define CUBLAS_POINTER_MODE_DEVICE     HIPBLAS_POINTER_MODE_DEVICE
  // Blas Atomics Mode
#define cublasAtomicsMode_t            hipblasAtomicsMode_t
#define CUBLAS_ATOMICS_NOT_ALLOWED     HIPBLAS_ATOMICS_NOT_ALLOWED
#define CUBLAS_ATOMICS_ALLOWED         HIPBLAS_ATOMICS_ALLOWED

  // Blas Math mode/tensor operation
#define cublasMath_t                                     hipblasMath_t
#define CUBLAS_DEFAULT_MATH                              HIPBLAS_DEFAULT_MATH
#define CUBLAS_TENSOR_OP_MATH                            HIPBLAS_TENSOR_OP_MATH
#define CUBLAS_PEDANTIC_MATH                             HIPBLAS_PEDANTIC_MATH
#define CUBLAS_TF32_TENSOR_OP_MATH                       HIPBLAS_TF32_TENSOR_OP_MATH
#define CUBLAS_MATH_DISALLOW_REDUCED_PRECISION_REDUCTION HIPBLAS_MATH_DISALLOW_REDUCED_PRECISION_REDUCTION

  // Blass different GEMM algorithms
#define cublasGemmAlgo_t               hipblasGemmAlgo_t
#define CUBLAS_GEMM_DFALT              HIPBLAS_GEMM_DEFAULT
#define CUBLAS_GEMM_DEFAULT            HIPBLAS_GEMM_DEFAULT
#define CUBLAS_GEMM_ALGO0              HIPBLAS_GEMM_ALGO0
#define CUBLAS_GEMM_ALGO1              HIPBLAS_GEMM_ALGO1
#define CUBLAS_GEMM_ALGO2              HIPBLAS_GEMM_ALGO2
#define CUBLAS_GEMM_ALGO3              HIPBLAS_GEMM_ALGO3
#define CUBLAS_GEMM_ALGO4              HIPBLAS_GEMM_ALGO4
#define CUBLAS_GEMM_ALGO5              HIPBLAS_GEMM_ALGO5
#define CUBLAS_GEMM_ALGO6              HIPBLAS_GEMM_ALGO6
#define CUBLAS_GEMM_ALGO7              HIPBLAS_GEMM_ALGO7
#define CUBLAS_GEMM_ALGO8              HIPBLAS_GEMM_ALGO8
#define CUBLAS_GEMM_ALGO9              HIPBLAS_GEMM_ALGO9
#define CUBLAS_GEMM_ALGO10             HIPBLAS_GEMM_ALGO10
#define CUBLAS_GEMM_ALGO11             HIPBLAS_GEMM_ALGO11
#define CUBLAS_GEMM_ALGO12             HIPBLAS_GEMM_ALGO12
#define CUBLAS_GEMM_ALGO13             HIPBLAS_GEMM_ALGO13
#define CUBLAS_GEMM_ALGO14             HIPBLAS_GEMM_ALGO14
#define CUBLAS_GEMM_ALGO15             HIPBLAS_GEMM_ALGO15
#define CUBLAS_GEMM_ALGO16             HIPBLAS_GEMM_ALGO16
#define CUBLAS_GEMM_ALGO17             HIPBLAS_GEMM_ALGO17
#define CUBLAS_GEMM_ALGO18             HIPBLAS_GEMM_ALGO18
#define CUBLAS_GEMM_ALGO19             HIPBLAS_GEMM_ALGO19
#define CUBLAS_GEMM_ALGO20             HIPBLAS_GEMM_ALGO20
#define CUBLAS_GEMM_ALGO21             HIPBLAS_GEMM_ALGO21
#define CUBLAS_GEMM_ALGO22             HIPBLAS_GEMM_ALGO22
#define CUBLAS_GEMM_ALGO23             HIPBLAS_GEMM_ALGO23
#define CUBLAS_GEMM_DEFAULT_TENSOR_OP  HIPBLAS_GEMM_DEFAULT_TENSOR_OP
#define CUBLAS_GEMM_DFALT_TENSOR_OP    HIPBLAS_GEMM_DFALT_TENSOR_OP
#define CUBLAS_GEMM_ALGO0_TENSOR_OP    HIPBLAS_GEMM_ALGO0_TENSOR_OP
#define CUBLAS_GEMM_ALGO1_TENSOR_OP    HIPBLAS_GEMM_ALGO1_TENSOR_OP
#define CUBLAS_GEMM_ALGO2_TENSOR_OP    HIPBLAS_GEMM_ALGO2_TENSOR_OP
#define CUBLAS_GEMM_ALGO3_TENSOR_OP    HIPBLAS_GEMM_ALGO3_TENSOR_OP
#define CUBLAS_GEMM_ALGO4_TENSOR_OP    HIPBLAS_GEMM_ALGO4_TENSOR_OP
#define CUBLAS_GEMM_ALGO5_TENSOR_OP    HIPBLAS_GEMM_ALGO5_TENSOR_OP
#define CUBLAS_GEMM_ALGO6_TENSOR_OP    HIPBLAS_GEMM_ALGO6_TENSOR_OP
#define CUBLAS_GEMM_ALGO7_TENSOR_OP    HIPBLAS_GEMM_ALGO7_TENSOR_OP
#define CUBLAS_GEMM_ALGO8_TENSOR_OP    HIPBLAS_GEMM_ALGO8_TENSOR_OP
#define CUBLAS_GEMM_ALGO9_TENSOR_OP    HIPBLAS_GEMM_ALGO9_TENSOR_OP
#define CUBLAS_GEMM_ALGO10_TENSOR_OP   HIPBLAS_GEMM_ALGO10_TENSOR_OP
#define CUBLAS_GEMM_ALGO11_TENSOR_OP   HIPBLAS_GEMM_ALGO11_TENSOR_OP
#define CUBLAS_GEMM_ALGO12_TENSOR_OP   HIPBLAS_GEMM_ALGO12_TENSOR_OP
#define CUBLAS_GEMM_ALGO13_TENSOR_OP   HIPBLAS_GEMM_ALGO13_TENSOR_OP
#define CUBLAS_GEMM_ALGO14_TENSOR_OP   HIPBLAS_GEMM_ALGO14_TENSOR_OP
#define CUBLAS_GEMM_ALGO15_TENSOR_OP   HIPBLAS_GEMM_ALGO15_TENSOR_OP

  // CUDA Library Data Types

#define cublasDataType_t               hipDataType
#define cudaDataType_t                 hipDataType
#define cudaDataType                   hipDataType
#define CUDA_R_16F                     HIP_R_16F
#define CUDA_C_16F                     HIP_C_16F
#define CUDA_R_32F                     HIP_R_32F
#define CUDA_C_32F                     HIP_C_32F
#define CUDA_R_64F                     HIP_R_64F
#define CUDA_C_64F                     HIP_C_64F
#define CUDA_R_8I                      HIP_R_8I
#define CUDA_C_8I                      HIP_C_8I
#define CUDA_R_8U                      HIP_R_8U
#define CUDA_C_8U                      HIP_C_8U
#define CUDA_R_32I                     HIP_R_32I
#define CUDA_C_32I                     HIP_C_32I
#define CUDA_R_32U                     HIP_R_32U
#define CUDA_C_32U                     HIP_C_32U
#define CUDA_R_16BF                    HIP_R_16BF
#define CUDA_C_16BF                    HIP_C_16BF
#define CUDA_R_4I                      HIPBLAS_R_4I
#define CUDA_C_4I                      HIPBLAS_C_4I
#define CUDA_R_4U                      HIPBLAS_R_4U
#define CUDA_C_4U                      HIPBLAS_C_4U
#define CUDA_R_16I                     HIPBLAS_R_16I
#define CUDA_C_16I                     HIPBLAS_C_16I
#define CUDA_R_16U                     HIPBLAS_R_16U
#define CUDA_C_16U                     HIPBLAS_C_16U
#define CUDA_R_64I                     HIPBLAS_R_64I
#define CUDA_C_64I                     HIPBLAS_C_64I
#define CUDA_R_64U                     HIPBLAS_R_64U
#define CUDA_C_64U                     HIPBLAS_C_64U
#define CUDA_R_8F_E4M3                 HIPBLAS_R_8F_E4M3
#define CUDA_R_8F_E5M2                 HIPBLAS_R_8F_E5M2

  // CUBLAS Data Types

#define cublasHandle_t                 hipblasHandle_t
#define cublasContext                  hipblasContext
#define cublasComputeType_t            hipblasComputeType_t
#define CUBLAS_COMPUTE_16F             HIPBLAS_COMPUTE_16F
#define CUBLAS_COMPUTE_16F_PEDANTIC    HIPBLAS_COMPUTE_16F_PEDANTIC
#define CUBLAS_COMPUTE_32F             HIPBLAS_COMPUTE_32F
#define CUBLAS_COMPUTE_32F_PEDANTIC    HIPBLAS_COMPUTE_32F_PEDANTIC
#define CUBLAS_COMPUTE_32F_FAST_16F    HIPBLAS_COMPUTE_32F_FAST_16F
#define CUBLAS_COMPUTE_32F_FAST_16BF   HIPBLAS_COMPUTE_32F_FAST_16BF
#define CUBLAS_COMPUTE_32F_FAST_TF32   HIPBLAS_COMPUTE_32F_FAST_TF32
#define CUBLAS_COMPUTE_64F             HIPBLAS_COMPUTE_64F
#define CUBLAS_COMPUTE_64F_PEDANTIC    HIPBLAS_COMPUTE_64F_PEDANTIC
#define CUBLAS_COMPUTE_32I             HIPBLAS_COMPUTE_32I
#define CUBLAS_COMPUTE_32I_PEDANTIC    HIPBLAS_COMPUTE_32I_PEDANTIC





















#define cublasInit                     hipblasInit
#define cublasShutdown                 hipblasShutdown
#define cublasGetVersion               hipblasGetVersion
#define cublasGetError                 hipblasGetError
#define cublasAlloc                    hipblasAlloc
#define cublasFree                     hipblasFree
#define cublasSetKernelStream          hipblasSetKernelStream
#define cublasGetAtomicsMode           hipblasGetAtomicsMode
#define cublasSetAtomicsMode           hipblasSetAtomicsMode
#define cublasGetMathMode              hipblasGetMathMode
#define cublasSetMathMode              hipblasSetMathMode
#define cublasMigrateComputeType       hipblasMigrateComputeType
#define cublasGetSmCountTarget         hipblasGetSmCountTarget
#define cublasSetSmCountTarget         hipblasSetSmCountTarget
#define cublasGetStatusName            hipblasGetStatusName
#define cublasGetStatusString          hipblasGetStatusString
  // Blas loggin
#define cublasLogCallback              hipblasLogCallback
#define cublasLoggerConfigure          hipblasLoggerConfigure
#define cublasSetLoggerCallback        hipblasSetLoggerCallback
#define cublasGetLoggerCallback        hipblasGetLoggerCallback
  // Blas1 (v1) Routine
#define cublasCreate                   hipblasCreate
#define cublasDestroy                  hipblasDestroy
#define cublasSetStream                hipblasSetStream
#define cublasGetStream                hipblasGetStream
#define cublasSetPointerMode           hipblasSetPointerMode
#define cublasGetPointerMode           hipblasGetPointerMode
#define cublasSetVector                hipblasSetVector
#define cublasSetVector_64             hipblasSetVector_64
#define cublasGetVector                hipblasGetVector
#define cublasGetVector_64             hipblasGetVector_64
#define cublasSetVectorAsync           hipblasSetVectorAsync
#define cublasSetVectorAsync_64        hipblasSetVectorAsync_64
#define cublasGetVectorAsync           hipblasGetVectorAsync
#define cublasGetVectorAsync_64        hipblasGetVectorAsync_64
#define cublasSetMatrix                hipblasSetMatrix
#define cublasSetMatrix_64             hipblasSetMatrix_64
#define cublasGetMatrix                hipblasGetMatrix
#define cublasGetMatrix_64             hipblasGetMatrix_64
#define cublasSetMatrixAsync           hipblasSetMatrixAsync
#define cublasSetMatrixAsync_64        hipblasSetMatrixAsync_64
#define cublasGetMatrixAsync           hipblasGetMatrixAsync
#define cublasGetMatrixAsync_64        hipblasGetMatrixAsync_64
#define cublasXerbla                   hipblasXerbla
  // Blas2 (v2) Routine
#define cublasCreate_v2                hipblasCreate
#define cublasDestroy_v2               hipblasDestroy
#define cublasGetVersion_v2            hipblasGetVersion
#define cublasGetProperty              hipblasGetProperty
#define cublasSetStream_v2             hipblasSetStream
#define cublasGetStream_v2             hipblasGetStream
#define cublasSetPointerMode_v2        hipblasSetPointerMode
#define cublasGetPointerMode_v2        hipblasGetPointerMode
#define cublasGetCudartVersion         hipblasGetCudartVersion
  // NRM
  // NRM2 functions' signatures differ from _v2 ones, hipblas and rocblas
#define cublasSnrm2                    hipblasSnrm2
#define cublasSnrm2_64                 hipblasSnrm2_64
#define cublasDnrm2                    hipblasDnrm2
#define cublasDnrm2_64                 hipblasDnrm2_64
#define cublasScnrm2                   hipblasScnrm2_v2
#define cublasScnrm2_64                hipblasScnrm2_64
#define cublasDznrm2                   hipblasDznrm2_v2
#define cublasDznrm2_64                hipblasDznrm2_64
#define cublasNrm2Ex                   hipblasNrm2Ex_v2
#define cublasNrm2Ex_64                hipblasNrm2Ex_64
  // DO
  // DOT functions' signatures differ from _v2 ones, hipblas and rocblas
#define cublasSdot                     hipblasSdot
#define cublasSdot_64                  hipblasSdot_64
#define cublasDdot                     hipblasDdot
#define cublasDdot_64                  hipblasDdot_64
#define cublasCdotu                    hipblasCdotu_v2
#define cublasCdotu_64                 hipblasCdotu_64
#define cublasCdotc                    hipblasCdotc_v2
#define cublasCdotc_64                 hipblasCdotc_64
#define cublasZdotu                    hipblasZdotu_v2
#define cublasZdotu_64                 hipblasZdotu_64
#define cublasZdotc                    hipblasZdotc_v2
#define cublasZdotc_64                 hipblasZdotc_64
  // SCA
  // SCAL functions' signatures differ from _v2 ones, hipblas and rocblas
#define cublasSscal                    hipblasSscal
#define cublasSscal_64                 hipblasSscal_64
#define cublasDscal                    hipblasDscal
#define cublasDscal_64                 hipblasDscal_64
#define cublasCscal                    hipblasCscal_v2
#define cublasCscal_64                 hipblasCscal_64
#define cublasCsscal                   hipblasCsscal_v2
#define cublasCsscal_64                hipblasCsscal_64
#define cublasZscal                    hipblasZscal_v2
#define cublasZscal_64                 hipblasZscal_64
#define cublasZdscal                   hipblasZdscal_v2
#define cublasZdscal_64                hipblasZdscal_64
  // AXP
#define cublasSaxpy                    hipblasSaxpy
#define cublasSaxpy_64                 hipblasSaxpy_64
#define cublasDaxpy                    hipblasDaxpy
#define cublasDaxpy_64                 hipblasDaxpy_64
#define cublasCaxpy                    hipblasCaxpy_v2
#define cublasCaxpy_64                 hipblasCaxpy_64
#define cublasZaxpy                    hipblasZaxpy_v2
#define cublasZaxpy_64                 hipblasZaxpy_64
  // COP
#define cublasScopy                    hipblasScopy
#define cublasScopy_64                 hipblasScopy_64
#define cublasDcopy                    hipblasDcopy
#define cublasDcopy_64                 hipblasDcopy_64
#define cublasCcopy                    hipblasCcopy_v2
#define cublasCcopy_64                 hipblasCcopy_64
#define cublasZcopy                    hipblasZcopy_v2
#define cublasZcopy_64                 hipblasZcopy_64
  // SWA
#define cublasSswap                    hipblasSswap
#define cublasSswap_64                 hipblasSswap_64
#define cublasDswap                    hipblasDswap
#define cublasDswap_64                 hipblasDswap_64
#define cublasCswap                    hipblasCswap_v2
#define cublasCswap_64                 hipblasCswap_64
#define cublasZswap                    hipblasZswap_v2
#define cublasZswap_64                 hipblasZswap_64
  // AMA
#define cublasIsamax                   hipblasIsamax
#define cublasIsamax_64                hipblasIsamax_64
#define cublasIdamax                   hipblasIdamax
#define cublasIdamax_64                hipblasIdamax_64
#define cublasIcamax                   hipblasIcamax_v2
#define cublasIcamax_64                hipblasIcamax_64
#define cublasIzamax                   hipblasIzamax_v2
#define cublasIzamax_64                hipblasIzamax_64
  // AMI
#define cublasIsamin                   hipblasIsamin
#define cublasIsamin_64                hipblasIsamin_64
#define cublasIdamin                   hipblasIdamin
#define cublasIdamin_64                hipblasIdamin_64
#define cublasIcamin                   hipblasIcamin_v2
#define cublasIcamin_64                hipblasIcamin_64
#define cublasIzamin                   hipblasIzamin_v2
#define cublasIzamin_64                hipblasIzamin_64
  // ASU
#define cublasSasum                    hipblasSasum
#define cublasSasum_64                 hipblasSasum_64
#define cublasDasum                    hipblasDasum
#define cublasDasum_64                 hipblasDasum_64
#define cublasScasum                   hipblasScasum_v2
#define cublasScasum_64                hipblasScasum_64
#define cublasDzasum                   hipblasDzasum_v2
#define cublasDzasum_64                hipblasDzasum_64
  // RO
#define cublasSrot                     hipblasSrot
#define cublasSrot_64                  hipblasSrot_64
#define cublasDrot                     hipblasDrot
#define cublasDrot_64                  hipblasDrot_64
#define cublasCrot                     hipblasCrot_v2
#define cublasCrot_64                  hipblasCrot_64
#define cublasCsrot                    hipblasCsrot_v2
#define cublasCsrot_64                 hipblasCsrot_64
#define cublasZrot                     hipblasZrot_v2
#define cublasZrot_64                  hipblasZrot_64
#define cublasZdrot                    hipblasZdrot_v2
#define cublasZdrot_64                 hipblasZdrot_64
  // ROT
#define cublasSrotg                    hipblasSrotg
#define cublasDrotg                    hipblasDrotg
#define cublasCrotg                    hipblasCrotg_v2
#define cublasZrotg                    hipblasZrotg_v2
  // ROT
#define cublasSrotm                    hipblasSrotm
#define cublasSrotm_64                 hipblasSrotm_64
#define cublasDrotm                    hipblasDrotm
#define cublasDrotm_64                 hipblasDrotm_64
  // ROTM
#define cublasSrotmg                   hipblasSrotmg
#define cublasDrotmg                   hipblasDrotmg
  // GEM
#define cublasSgemv                    hipblasSgemv
#define cublasSgemv_64                 hipblasSgemv_64
#define cublasDgemv                    hipblasDgemv
#define cublasDgemv_64                 hipblasDgemv_64
#define cublasCgemv                    hipblasCgemv_v2
#define cublasCgemv_64                 hipblasCgemv_64
#define cublasZgemv                    hipblasZgemv_v2
#define cublasZgemv_64                 hipblasZgemv_64
  // GBM
#define cublasSgbmv                    hipblasSgbmv
#define cublasSgbmv_64                 hipblasSgbmv_64
#define cublasDgbmv                    hipblasDgbmv
#define cublasDgbmv_64                 hipblasDgbmv_64
#define cublasCgbmv                    hipblasCgbmv_v2
#define cublasCgbmv_64                 hipblasCgbmv_64
#define cublasZgbmv                    hipblasZgbmv_v2
#define cublasZgbmv_64                 hipblasZgbmv_64
  // TRM
#define cublasStrmv                    hipblasStrmv
#define cublasStrmv_64                 hipblasStrmv_64
#define cublasDtrmv                    hipblasDtrmv
#define cublasDtrmv_64                 hipblasDtrmv_64
#define cublasCtrmv                    hipblasCtrmv_v2
#define cublasCtrmv_64                 hipblasCtrmv_64
#define cublasZtrmv                    hipblasZtrmv_v2
#define cublasZtrmv_64                 hipblasZtrmv_64
  // TBM
#define cublasStbmv                    hipblasStbmv
#define cublasStbmv_64                 hipblasStbmv_64
#define cublasDtbmv                    hipblasDtbmv
#define cublasDtbmv_64                 hipblasDtbmv_64
#define cublasCtbmv                    hipblasCtbmv_v2
#define cublasCtbmv_64                 hipblasCtbmv_64
#define cublasZtbmv                    hipblasZtbmv_v2
#define cublasZtbmv_64                 hipblasZtbmv_64
  // TPM
#define cublasStpmv                    hipblasStpmv
#define cublasStpmv_64                 hipblasStpmv_64
#define cublasDtpmv                    hipblasDtpmv
#define cublasDtpmv_64                 hipblasDtpmv_64
#define cublasCtpmv                    hipblasCtpmv_v2
#define cublasCtpmv_64                 hipblasCtpmv_64
#define cublasZtpmv                    hipblasZtpmv_v2
#define cublasZtpmv_64                 hipblasZtpmv_64
  // TRS
#define cublasStrsv                    hipblasStrsv
#define cublasStrsv_64                 hipblasStrsv_64
#define cublasDtrsv                    hipblasDtrsv
#define cublasDtrsv_64                 hipblasDtrsv_64
#define cublasCtrsv                    hipblasCtrsv_v2
#define cublasCtrsv_64                 hipblasCtrsv_64
#define cublasZtrsv                    hipblasZtrsv_v2
#define cublasZtrsv_64                 hipblasZtrsv_64
  // TPS
#define cublasStpsv                    hipblasStpsv
#define cublasStpsv_64                 hipblasStpsv_64
#define cublasDtpsv                    hipblasDtpsv
#define cublasDtpsv_64                 hipblasDtpsv_64
#define cublasCtpsv                    hipblasCtpsv_v2
#define cublasCtpsv_64                 hipblasCtpsv_64
#define cublasZtpsv                    hipblasZtpsv_v2
#define cublasZtpsv_64                 hipblasZtpsv_64
  // TBS
#define cublasStbsv                    hipblasStbsv
#define cublasStbsv_64                 hipblasStbsv_64
#define cublasDtbsv                    hipblasDtbsv
#define cublasDtbsv_64                 hipblasDtbsv_64
#define cublasCtbsv                    hipblasCtbsv_v2
#define cublasCtbsv_64                 hipblasCtbsv_64
#define cublasZtbsv                    hipblasZtbsv_v2
#define cublasZtbsv_64                 hipblasZtbsv_64
  // SYMV/HEM
#define cublasSsymv                    hipblasSsymv
#define cublasSsymv_64                 hipblasSsymv_64
#define cublasDsymv                    hipblasDsymv
#define cublasDsymv_64                 hipblasDsymv_64
#define cublasCsymv                    hipblasCsymv_v2
#define cublasCsymv_64                 hipblasCsymv_64
#define cublasZsymv                    hipblasZsymv_v2
#define cublasZsymv_64                 hipblasZsymv_64
#define cublasChemv                    hipblasChemv_v2
#define cublasChemv_64                 hipblasChemv_64
#define cublasZhemv                    hipblasZhemv_v2
#define cublasZhemv_64                 hipblasZhemv_64
  // SBMV/HBM
#define cublasSsbmv                    hipblasSsbmv
#define cublasSsbmv_64                 hipblasSsbmv_64
#define cublasDsbmv                    hipblasDsbmv
#define cublasDsbmv_64                 hipblasDsbmv_64
#define cublasChbmv                    hipblasChbmv_v2
#define cublasChbmv_64                 hipblasChbmv_64
#define cublasZhbmv                    hipblasZhbmv_v2
#define cublasZhbmv_64                 hipblasZhbmv_64
  // SPMV/HPM
#define cublasSspmv                    hipblasSspmv
#define cublasSspmv_64                 hipblasSspmv_64
#define cublasDspmv                    hipblasDspmv
#define cublasDspmv_64                 hipblasDspmv_64
#define cublasChpmv                    hipblasChpmv_v2
#define cublasChpmv_64                 hipblasChpmv_64
#define cublasZhpmv                    hipblasZhpmv_v2
#define cublasZhpmv_64                 hipblasZhpmv_64
  // GE
#define cublasSger                     hipblasSger
#define cublasSger_64                  hipblasSger_64
#define cublasDger                     hipblasDger
#define cublasDger_64                  hipblasDger_64
#define cublasCgeru                    hipblasCgeru_v2
#define cublasCgeru_64                 hipblasCgeru_64
#define cublasCgerc                    hipblasCgerc_v2
#define cublasCgerc_64                 hipblasCgerc_64
#define cublasZgeru                    hipblasZgeru_v2
#define cublasZgeru_64                 hipblasZgeru_64
#define cublasZgerc                    hipblasZgerc_v2
#define cublasZgerc_64                 hipblasZgerc_64
  // SYR/HE
#define cublasSsyr                     hipblasSsyr
#define cublasSsyr_64                  hipblasSsyr_64
#define cublasDsyr                     hipblasDsyr
#define cublasDsyr_64                  hipblasDsyr_64
#define cublasCsyr                     hipblasCsyr_v2
#define cublasCsyr_64                  hipblasCsyr_64
#define cublasZsyr                     hipblasZsyr_v2
#define cublasZsyr_64                  hipblasZsyr_64
#define cublasCher                     hipblasCher_v2
#define cublasCher_64                  hipblasCher_64
#define cublasZher                     hipblasZher_v2
#define cublasZher_64                  hipblasZher_64
  // SPR/HP
#define cublasSspr                     hipblasSspr
#define cublasSspr_64                  hipblasSspr_64
#define cublasDspr                     hipblasDspr
#define cublasDspr_64                  hipblasDspr_64
#define cublasChpr                     hipblasChpr_v2
#define cublasChpr_64                  hipblasChpr_64
#define cublasZhpr                     hipblasZhpr_v2
#define cublasZhpr_64                  hipblasZhpr_64
  // SYR2/HER
#define cublasSsyr2                    hipblasSsyr2
#define cublasSsyr2_64                 hipblasSsyr2_64
#define cublasDsyr2                    hipblasDsyr2
#define cublasDsyr2_64                 hipblasDsyr2_64
#define cublasCsyr2                    hipblasCsyr2_v2
#define cublasCsyr2_64                 hipblasCsyr2_64
#define cublasZsyr2                    hipblasZsyr2_v2
#define cublasZsyr2_64                 hipblasZsyr2_64
#define cublasCher2                    hipblasCher2_v2
#define cublasCher2_64                 hipblasCher2_64
#define cublasZher2                    hipblasZher2_v2
#define cublasZher2_64                 hipblasZher2_64
  // SPR2/HPR
#define cublasSspr2                    hipblasSspr2
#define cublasSspr2_64                 hipblasSspr2_64
#define cublasDspr2                    hipblasDspr2
#define cublasDspr2_64                 hipblasDspr2_64
#define cublasChpr2                    hipblasChpr2_v2
#define cublasChpr2_64                 hipblasChpr2_64
#define cublasZhpr2                    hipblasZhpr2_v2
#define cublasZhpr2_64                 hipblasZhpr2_64
  // Blas3 (v1) Routine
  // GEM
#define cublasSgemm                    hipblasSgemm
#define cublasSgemm_64                 hipblasSgemm_64
#define cublasDgemm                    hipblasDgemm
#define cublasDgemm_64                 hipblasDgemm_64
#define cublasCgemm                    hipblasCgemm_v2
#define cublasCgemm_64                 hipblasCgemm_64
#define cublasZgemm                    hipblasZgemm_v2
#define cublasZgemm_64                 hipblasZgemm_64
#define cublasHgemm                    hipblasHgemm
#define cublasHgemm_64                 hipblasHgemm_64
  // BATCH GEM
#define cublasSgemmBatched             hipblasSgemmBatched
#define cublasSgemmBatched_64          hipblasSgemmBatched_64
#define cublasDgemmBatched             hipblasDgemmBatched
#define cublasDgemmBatched_64          hipblasDgemmBatched_64
#define cublasHgemmBatched             hipblasHgemmBatched
#define cublasHgemmBatched_64          hipblasHgemmBatched_64
#define cublasSgemmStridedBatched      hipblasSgemmStridedBatched
#define cublasSgemmStridedBatched_64   hipblasSgemmStridedBatched_64
#define cublasDgemmStridedBatched      hipblasDgemmStridedBatched
#define cublasDgemmStridedBatched_64   hipblasDgemmStridedBatched_64
#define cublasCgemmBatched             hipblasCgemmBatched_v2
#define cublasCgemmBatched_64          hipblasCgemmBatched_64
#define cublasCgemm3mBatched           hipblasCgemm3mBatched
#define cublasCgemm3mBatched_64        hipblasCgemm3mBatched_64
#define cublasZgemmBatched             hipblasZgemmBatched_v2
#define cublasZgemmBatched_64          hipblasZgemmBatched_64
#define cublasCgemmStridedBatched      hipblasCgemmStridedBatched_v2
#define cublasCgemmStridedBatched_64   hipblasCgemmStridedBatched_64
#define cublasCgemm3mStridedBatched    hipblasCgemm3mStridedBatched
#define cublasCgemm3mStridedBatched_64 hipblasCgemm3mStridedBatched_64
#define cublasZgemmStridedBatched      hipblasZgemmStridedBatched_v2
#define cublasZgemmStridedBatched_64   hipblasZgemmStridedBatched_64
#define cublasHgemmStridedBatched      hipblasHgemmStridedBatched
#define cublasHgemmStridedBatched_64   hipblasHgemmStridedBatched_64
  // BATCH GEM
#define cublasSgemvBatched             hipblasSgemvBatched
#define cublasSgemvBatched_64          hipblasSgemvBatched_64
#define cublasDgemvBatched             hipblasDgemvBatched
#define cublasDgemvBatched_64          hipblasDgemvBatched_64
#define cublasCgemvBatched             hipblasCgemvBatched_v2
#define cublasCgemvBatched_64          hipblasCgemvBatched_64
#define cublasZgemvBatched             hipblasZgemvBatched_v2
#define cublasZgemvBatched_64          hipblasZgemvBatched_64
#define cublasHSHgemvBatched           hipblasHSHgemvBatched
#define cublasHSHgemvBatched_64        hipblasHSHgemvBatched_64
#define cublasHSSgemvBatched           hipblasHSSgemvBatched
#define cublasHSSgemvBatched_64        hipblasHSSgemvBatched_64
#define cublasTSTgemvBatched           hipblasTSTgemvBatched
#define cublasTSTgemvBatched_64        hipblasTSTgemvBatched_64
#define cublasTSSgemvBatched           hipblasTSSgemvBatched
#define cublasTSSgemvBatched_64        hipblasTSSgemvBatched_64
#define cublasSgemvStridedBatched      hipblasSgemvStridedBatched
#define cublasSgemvStridedBatched_64   hipblasSgemvStridedBatched_64
#define cublasDgemvStridedBatched      hipblasDgemvStridedBatched
#define cublasDgemvStridedBatched_64   hipblasDgemvStridedBatched_64
#define cublasCgemvStridedBatched      hipblasCgemvStridedBatched_v2
#define cublasCgemvStridedBatched_64   hipblasCgemvStridedBatched_64
#define cublasZgemvStridedBatched      hipblasZgemvStridedBatched_v2
#define cublasZgemvStridedBatched_64   hipblasZgemvStridedBatched_64
#define cublasHSHgemvStridedBatched    hipblasHSHgemvStridedBatched
#define cublasHSHgemvStridedBatched_64 hipblasHSHgemvStridedBatched_64
#define cublasHSSgemvStridedBatched    hipblasHSSgemvStridedBatched
#define cublasHSSgemvStridedBatched_64 hipblasHSSgemvStridedBatched_64
#define cublasTSTgemvStridedBatched    hipblasTSTgemvStridedBatched
#define cublasTSTgemvStridedBatched_64 hipblasTSTgemvStridedBatched_64
#define cublasTSSgemvStridedBatched    hipblasTSSgemvStridedBatched
#define cublasTSSgemvStridedBatched_64 hipblasTSSgemvStridedBatched_64
  // SYR
#define cublasSsyrk                    hipblasSsyrk
#define cublasSsyrk_64                 hipblasSsyrk_64
#define cublasDsyrk                    hipblasDsyrk
#define cublasDsyrk_64                 hipblasDsyrk_64
#define cublasCsyrk                    hipblasCsyrk_v2
#define cublasCsyrk_64                 hipblasCsyrk_64
#define cublasZsyrk                    hipblasZsyrk_v2
#define cublasZsyrk_64                 hipblasZsyrk_64
  // HER
#define cublasCherk                    hipblasCherk_v2
#define cublasCherk_64                 hipblasCherk_64
#define cublasZherk                    hipblasZherk_v2
#define cublasZherk_64                 hipblasZherk_64
  // SYR2
#define cublasSsyr2k                   hipblasSsyr2k
#define cublasSsyr2k_64                hipblasSsyr2k_64
#define cublasDsyr2k                   hipblasDsyr2k
#define cublasDsyr2k_64                hipblasDsyr2k_64
#define cublasCsyr2k                   hipblasCsyr2k_v2
#define cublasCsyr2k_64                hipblasCsyr2k_64
#define cublasZsyr2k                   hipblasZsyr2k_v2
#define cublasZsyr2k_64                hipblasZsyr2k_64
  // SYRKX - eXtended SYR
#define cublasSsyrkx                   hipblasSsyrkx
#define cublasSsyrkx_64                hipblasSsyrkx_64
#define cublasDsyrkx                   hipblasDsyrkx
#define cublasDsyrkx_64                hipblasDsyrkx_64
#define cublasCsyrkx                   hipblasCsyrkx_v2
#define cublasCsyrkx_64                hipblasCsyrkx_64
#define cublasZsyrkx                   hipblasZsyrkx_v2
#define cublasZsyrkx_64                hipblasZsyrkx_64
  // HER2
#define cublasCher2k                   hipblasCher2k_v2
#define cublasCher2k_64                hipblasCher2k_64
#define cublasZher2k                   hipblasZher2k_v2
#define cublasZher2k_64                hipblasZher2k_64
  // HERKX - eXtended HER
#define cublasCherkx                   hipblasCherkx_v2
#define cublasCherkx_64                hipblasCherkx_64
#define cublasZherkx                   hipblasZherkx_v2
#define cublasZherkx_64                hipblasZherkx_64
  // SYM
#define cublasSsymm                    hipblasSsymm
#define cublasSsymm_64                 hipblasSsymm_64
#define cublasDsymm                    hipblasDsymm
#define cublasDsymm_64                 hipblasDsymm_64
#define cublasCsymm                    hipblasCsymm_v2
#define cublasCsymm_64                 hipblasCsymm_64
#define cublasZsymm                    hipblasZsymm_v2
#define cublasZsymm_64                 hipblasZsymm_64
  // HEM
#define cublasChemm                    hipblasChemm_v2
#define cublasChemm_64                 hipblasChemm_64
#define cublasZhemm                    hipblasZhemm_v2
#define cublasZhemm_64                 hipblasZhemm_64
  // TRS
#define cublasStrsm                    hipblasStrsm
#define cublasStrsm_64                 hipblasStrsm_64
#define cublasDtrsm                    hipblasDtrsm
#define cublasDtrsm_64                 hipblasDtrsm_64
#define cublasCtrsm                    hipblasCtrsm_v2
#define cublasCtrsm_64                 hipblasCtrsm_64
#define cublasZtrsm                    hipblasZtrsm_v2
#define cublasZtrsm_64                 hipblasZtrsm_64
  // TRM
#define cublasStrmm                    hipblasStrmm
#define cublasStrmm_64                 hipblasStrmm_64
#define cublasDtrmm                    hipblasDtrmm
#define cublasDtrmm_64                 hipblasDtrmm_64
#define cublasCtrmm                    hipblasCtrmm_v2
#define cublasCtrmm_64                 hipblasCtrmm_64
#define cublasZtrmm                    hipblasZtrmm_v2
#define cublasZtrmm_64                 hipblasZtrmm_64
  // ------------------------ CUBLAS BLAS - like extension (cublas_api.h
  // GEA
#define cublasSgeam                    hipblasSgeam
#define cublasSgeam_64                 hipblasSgeam_64
#define cublasDgeam                    hipblasDgeam
#define cublasDgeam_64                 hipblasDgeam_64
#define cublasCgeam                    hipblasCgeam_v2
#define cublasCgeam_64                 hipblasCgeam_64
#define cublasZgeam                    hipblasZgeam_v2
#define cublasZgeam_64                 hipblasZgeam_64
  // GETRF - Batched L
#define cublasSgetrfBatched            hipblasSgetrfBatched
#define cublasDgetrfBatched            hipblasDgetrfBatched
#define cublasCgetrfBatched            hipblasCgetrfBatched_v2
#define cublasZgetrfBatched            hipblasZgetrfBatched_v2
  // Batched inversion based on LU factorization from getr
#define cublasSgetriBatched            hipblasSgetriBatched
#define cublasDgetriBatched            hipblasDgetriBatched
#define cublasCgetriBatched            hipblasCgetriBatched_v2
#define cublasZgetriBatched            hipblasZgetriBatched_v2
  // Batched solver based on LU factorization from getr
#define cublasSgetrsBatched            hipblasSgetrsBatched
#define cublasDgetrsBatched            hipblasDgetrsBatched
#define cublasCgetrsBatched            hipblasCgetrsBatched_v2
#define cublasZgetrsBatched            hipblasZgetrsBatched_v2
  // TRSM - Batched Triangular Solve
#define cublasStrsmBatched             hipblasStrsmBatched
#define cublasStrsmBatched_64          hipblasStrsmBatched_64
#define cublasDtrsmBatched             hipblasDtrsmBatched
#define cublasDtrsmBatched_64          hipblasDtrsmBatched_64
#define cublasCtrsmBatched             hipblasCtrsmBatched_v2
#define cublasCtrsmBatched_64          hipblasCtrsmBatched_64
#define cublasZtrsmBatched             hipblasZtrsmBatched_v2
#define cublasZtrsmBatched_64          hipblasZtrsmBatched_64
  // MATINV - Batche
#define cublasSmatinvBatched           hipblasSmatinvBatched
#define cublasDmatinvBatched           hipblasDmatinvBatched
#define cublasCmatinvBatched           hipblasCmatinvBatched
#define cublasZmatinvBatched           hipblasZmatinvBatched
  // Batch QR Factorizatio
#define cublasSgeqrfBatched            hipblasSgeqrfBatched
#define cublasDgeqrfBatched            hipblasDgeqrfBatched
#define cublasCgeqrfBatched            hipblasCgeqrfBatched_v2
#define cublasZgeqrfBatched            hipblasZgeqrfBatched_v2
  // Least Square Min only m >= n and Non-transpose supporte
#define cublasSgelsBatched             hipblasSgelsBatched
#define cublasDgelsBatched             hipblasDgelsBatched
#define cublasCgelsBatched             hipblasCgelsBatched_v2
#define cublasZgelsBatched             hipblasZgelsBatched_v2
  // DGM
#define cublasSdgmm                    hipblasSdgmm
#define cublasSdgmm_64                 hipblasSdgmm_64
#define cublasDdgmm                    hipblasDdgmm
#define cublasDdgmm_64                 hipblasDdgmm_64
#define cublasCdgmm                    hipblasCdgmm_v2
#define cublasCdgmm_64                 hipblasCdgmm_64
#define cublasZdgmm                    hipblasZdgmm_v2
#define cublasZdgmm_64                 hipblasZdgmm_64
  // TPTTR - Triangular Pack format to Triangular forma
#define cublasStpttr                   hipblasStpttr
#define cublasDtpttr                   hipblasDtpttr
#define cublasCtpttr                   hipblasCtpttr
#define cublasZtpttr                   hipblasZtpttr
  // TRTTP - Triangular format to Triangular Pack forma
#define cublasStrttp                   hipblasStrttp
#define cublasDtrttp                   hipblasDtrttp
#define cublasCtrttp                   hipblasCtrttp
#define cublasZtrttp                   hipblasZtrttp
  // GEM
#define cublasSgemv_v2                 hipblasSgemv
#define cublasSgemv_v2_64              hipblasSgemv_64
#define cublasDgemv_v2                 hipblasDgemv
#define cublasDgemv_v2_64              hipblasDgemv_64
#define cublasCgemv_v2                 hipblasCgemv_v2
#define cublasCgemv_v2_64              hipblasCgemv_64
#define cublasZgemv_v2                 hipblasZgemv_v2
#define cublasZgemv_v2_64              hipblasZgemv_64
  // GBM
#define cublasSgbmv_v2                 hipblasSgbmv
#define cublasSgbmv_v2_64              hipblasSgbmv_64
#define cublasDgbmv_v2                 hipblasDgbmv
#define cublasDgbmv_v2_64              hipblasDgbmv_64
#define cublasCgbmv_v2                 hipblasCgbmv_v2
#define cublasCgbmv_v2_64              hipblasCgbmv_64
#define cublasZgbmv_v2                 hipblasZgbmv_v2
#define cublasZgbmv_v2_64              hipblasZgbmv_64
  // TRM
#define cublasStrmv_v2                 hipblasStrmv
#define cublasStrmv_v2_64              hipblasStrmv_64
#define cublasDtrmv_v2                 hipblasDtrmv
#define cublasDtrmv_v2_64              hipblasDtrmv_64
#define cublasCtrmv_v2                 hipblasCtrmv_v2
#define cublasCtrmv_v2_64              hipblasCtrmv_64
#define cublasZtrmv_v2                 hipblasZtrmv_v2
#define cublasZtrmv_v2_64              hipblasZtrmv_64
  // TBM
#define cublasStbmv_v2                 hipblasStbmv
#define cublasStbmv_v2_64              hipblasStbmv_64
#define cublasDtbmv_v2                 hipblasDtbmv
#define cublasDtbmv_v2_64              hipblasDtbmv_64
#define cublasCtbmv_v2                 hipblasCtbmv_v2
#define cublasCtbmv_v2_64              hipblasCtbmv_64
#define cublasZtbmv_v2                 hipblasZtbmv_v2
#define cublasZtbmv_v2_64              hipblasZtbmv_64
  // TPM
#define cublasStpmv_v2                 hipblasStpmv
#define cublasStpmv_v2_64              hipblasStpmv_64
#define cublasDtpmv_v2                 hipblasDtpmv
#define cublasDtpmv_v2_64              hipblasDtpmv_64
#define cublasCtpmv_v2                 hipblasCtpmv_v2
#define cublasCtpmv_v2_64              hipblasCtpmv_64
#define cublasZtpmv_v2                 hipblasZtpmv_v2
#define cublasZtpmv_v2_64              hipblasZtpmv_64
  // TRS
#define cublasStrsv_v2                 hipblasStrsv
#define cublasStrsv_v2_64              hipblasStrsv_64
#define cublasDtrsv_v2                 hipblasDtrsv
#define cublasDtrsv_v2_64              hipblasDtrsv_64
#define cublasCtrsv_v2                 hipblasCtrsv_v2
#define cublasCtrsv_v2_64              hipblasCtrsv_64
#define cublasZtrsv_v2                 hipblasZtrsv_v2
#define cublasZtrsv_v2_64              hipblasZtrsv_64
  // TPS
#define cublasStpsv_v2                 hipblasStpsv
#define cublasStpsv_v2_64              hipblasStpsv_64
#define cublasDtpsv_v2                 hipblasDtpsv
#define cublasDtpsv_v2_64              hipblasDtpsv_64
#define cublasCtpsv_v2                 hipblasCtpsv_v2
#define cublasCtpsv_v2_64              hipblasCtpsv_64
#define cublasZtpsv_v2                 hipblasZtpsv_v2
#define cublasZtpsv_v2_64              hipblasZtpsv_64
  // TBS
#define cublasStbsv_v2                 hipblasStbsv
#define cublasStbsv_v2_64              hipblasStbsv_64
#define cublasDtbsv_v2                 hipblasDtbsv
#define cublasDtbsv_v2_64              hipblasDtbsv_64
#define cublasCtbsv_v2                 hipblasCtbsv_v2
#define cublasCtbsv_v2_64              hipblasCtbsv_64
#define cublasZtbsv_v2                 hipblasZtbsv_v2
#define cublasZtbsv_v2_64              hipblasZtbsv_64
  // SYMV/HEM
#define cublasSsymv_v2                 hipblasSsymv
#define cublasSsymv_v2_64              hipblasSsymv_64
#define cublasDsymv_v2                 hipblasDsymv
#define cublasDsymv_v2_64              hipblasDsymv_64
#define cublasCsymv_v2                 hipblasCsymv_v2
#define cublasCsymv_v2_64              hipblasCsymv_64
#define cublasZsymv_v2                 hipblasZsymv_v2
#define cublasZsymv_v2_64              hipblasZsymv_64
#define cublasChemv_v2                 hipblasChemv_v2
#define cublasChemv_v2_64              hipblasChemv_64
#define cublasZhemv_v2                 hipblasZhemv_v2
#define cublasZhemv_v2_64              hipblasZhemv_64
  // SBMV/HBM
#define cublasSsbmv_v2                 hipblasSsbmv
#define cublasSsbmv_v2_64              hipblasSsbmv_64
#define cublasDsbmv_v2                 hipblasDsbmv
#define cublasDsbmv_v2_64              hipblasDsbmv_64
#define cublasChbmv_v2                 hipblasChbmv_v2
#define cublasChbmv_v2_64              hipblasChbmv_64
#define cublasZhbmv_v2                 hipblasZhbmv_v2
#define cublasZhbmv_v2_64              hipblasZhbmv_64
  // SPMV/HPM
#define cublasSspmv_v2                 hipblasSspmv
#define cublasSspmv_v2_64              hipblasSspmv_64
#define cublasDspmv_v2                 hipblasDspmv
#define cublasDspmv_v2_64              hipblasDspmv_64
#define cublasChpmv_v2                 hipblasChpmv_v2
#define cublasChpmv_v2_64              hipblasChpmv_64
#define cublasZhpmv_v2                 hipblasZhpmv_v2
#define cublasZhpmv_v2_64              hipblasZhpmv_64
  // GE
#define cublasSger_v2                  hipblasSger
#define cublasSger_v2_64               hipblasSger_64
#define cublasDger_v2                  hipblasDger
#define cublasDger_v2_64               hipblasDger_64
#define cublasCgeru_v2                 hipblasCgeru_v2
#define cublasCgeru_v2_64              hipblasCgeru_64
#define cublasCgerc_v2                 hipblasCgerc_v2
#define cublasCgerc_v2_64              hipblasCgerc_64
#define cublasZgeru_v2                 hipblasZgeru_v2
#define cublasZgeru_v2_64              hipblasZgeru_64
#define cublasZgerc_v2                 hipblasZgerc_v2
#define cublasZgerc_v2_64              hipblasZgerc_64
  // SYR/HE
#define cublasSsyr_v2                  hipblasSsyr
#define cublasSsyr_v2_64               hipblasSsyr_64
#define cublasDsyr_v2                  hipblasDsyr
#define cublasDsyr_v2_64               hipblasDsyr_64
#define cublasCsyr_v2                  hipblasCsyr_v2
#define cublasCsyr_v2_64               hipblasCsyr_64
#define cublasZsyr_v2                  hipblasZsyr_v2
#define cublasZsyr_v2_64               hipblasZsyr_64
#define cublasCher_v2                  hipblasCher_v2
#define cublasCher_v2_64               hipblasCher_64
#define cublasZher_v2                  hipblasZher_v2
#define cublasZher_v2_64               hipblasZher_64
  // SPR/HP
#define cublasSspr_v2                  hipblasSspr
#define cublasSspr_v2_64               hipblasSspr_64
#define cublasDspr_v2                  hipblasDspr
#define cublasDspr_v2_64               hipblasDspr_64
#define cublasChpr_v2                  hipblasChpr_v2
#define cublasChpr_v2_64               hipblasChpr_64
#define cublasZhpr_v2                  hipblasZhpr_v2
#define cublasZhpr_v2_64               hipblasZhpr_64
  // SYR2/HER
#define cublasSsyr2_v2                 hipblasSsyr2
#define cublasSsyr2_v2_64              hipblasSsyr2_64
#define cublasDsyr2_v2                 hipblasDsyr2
#define cublasDsyr2_v2_64              hipblasDsyr2_64
#define cublasCsyr2_v2                 hipblasCsyr2_v2
#define cublasCsyr2_v2_64              hipblasCsyr2_64
#define cublasZsyr2_v2                 hipblasZsyr2_v2
#define cublasZsyr2_v2_64              hipblasZsyr2_64
#define cublasCher2_v2                 hipblasCher2_v2
#define cublasCher2_v2_64              hipblasCher2_64
#define cublasZher2_v2                 hipblasZher2_v2
#define cublasZher2_v2_64              hipblasZher2_64
  // SPR2/HPR
#define cublasSspr2_v2                 hipblasSspr2
#define cublasSspr2_v2_64              hipblasSspr2_64
#define cublasDspr2_v2                 hipblasDspr2
#define cublasDspr2_v2_64              hipblasDspr2_64
#define cublasChpr2_v2                 hipblasChpr2_v2
#define cublasChpr2_v2_64              hipblasChpr2_64
#define cublasZhpr2_v2                 hipblasZhpr2_v2
#define cublasZhpr2_v2_64              hipblasZhpr2_64
  // Blas3 (v2) Routine
  // GEM
#define cublasSgemm_v2                 hipblasSgemm
#define cublasSgemm_v2_64              hipblasSgemm_64
#define cublasDgemm_v2                 hipblasDgemm
#define cublasDgemm_v2_64              hipblasDgemm_64
#define cublasCgemm_v2                 hipblasCgemm_v2
#define cublasCgemm_v2_64              hipblasCgemm_64
#define cublasCgemm3m                  hipblasCgemm3m
#define cublasCgemm3m_64               hipblasCgemm3m_64
#define cublasCgemm3mEx                hipblasCgemm3mEx
#define cublasCgemm3mEx_64             hipblasCgemm3mEx_64
#define cublasZgemm_v2                 hipblasZgemm_v2
#define cublasZgemm_v2_64              hipblasZgemm_64
#define cublasZgemm3m                  hipblasZgemm3m
#define cublasZgemm3m_64               hipblasZgemm3m_64
  //IO in FP16 / FP32, computation in floa
#define cublasSgemmEx                  hipblasSgemmEx
#define cublasSgemmEx_64               hipblasSgemmEx_64
#define cublasGemmEx                   hipblasGemmEx_v2
#define cublasGemmEx_64                hipblasGemmEx_64
#define cublasGemmBatchedEx            hipblasGemmBatchedEx_v2
#define cublasGemmBatchedEx_64         hipblasGemmBatchedEx_64
#define cublasGemmStridedBatchedEx     hipblasGemmStridedBatchedEx_v2
#define cublasGemmStridedBatchedEx_64  hipblasGemmStridedBatchedEx_64
  // IO in Int8 complex/cuComplex, computation in cuComple
#define cublasCgemmEx                  hipblasCgemmEx
#define cublasCgemmEx_64               hipblasCgemmEx_64
#define cublasUint8gemmBias            hipblasUint8gemmBias
  // SYR
#define cublasSsyrk_v2                 hipblasSsyrk
#define cublasSsyrk_v2_64              hipblasSsyrk_64
#define cublasDsyrk_v2                 hipblasDsyrk
#define cublasDsyrk_v2_64              hipblasDsyrk_64
#define cublasCsyrk_v2                 hipblasCsyrk_v2
#define cublasCsyrk_v2_64              hipblasCsyrk_64
#define cublasZsyrk_v2                 hipblasZsyrk_v2
#define cublasZsyrk_v2_64              hipblasZsyrk_64
  // IO in Int8 complex/cuComplex, computation in cuComple
#define cublasCsyrkEx                  hipblasCsyrkEx
#define cublasCsyrkEx_64               hipblasCsyrkEx_64
  // IO in Int8 complex/cuComplex, computation in cuComplex, Gaussian mat
#define cublasCsyrk3mEx                hipblasCsyrk3mEx
#define cublasCsyrk3mEx_64             hipblasCsyrk3mEx_64
  // HER
#define cublasCherk_v2                 hipblasCherk_v2
#define cublasCherk_v2_64              hipblasCherk_64
  // IO in Int8 complex/cuComplex, computation in cuComple
#define cublasCherkEx                  hipblasCherkEx
#define cublasCherkEx_64               hipblasCherkEx_64
  // IO in Int8 complex/cuComplex, computation in cuComplex, Gaussian mat
#define cublasCherk3mEx                hipblasCherk3mEx
#define cublasCherk3mEx_64             hipblasCherk3mEx_64
#define cublasZherk_v2                 hipblasZherk_v2
#define cublasZherk_v2_64              hipblasZherk_64
  // SYR2
#define cublasSsyr2k_v2                hipblasSsyr2k
#define cublasSsyr2k_v2_64             hipblasSsyr2k_64
#define cublasDsyr2k_v2                hipblasDsyr2k
#define cublasDsyr2k_v2_64             hipblasDsyr2k_64
#define cublasCsyr2k_v2                hipblasCsyr2k_v2
#define cublasCsyr2k_v2_64             hipblasCsyr2k_64
#define cublasZsyr2k_v2                hipblasZsyr2k_v2
#define cublasZsyr2k_v2_64             hipblasZsyr2k_64
  // HER2
#define cublasCher2k_v2                hipblasCher2k_v2
#define cublasCher2k_v2_64             hipblasCher2k_64
#define cublasZher2k_v2                hipblasZher2k_v2
#define cublasZher2k_v2_64             hipblasZher2k_64
  // SYM
#define cublasSsymm_v2                 hipblasSsymm
#define cublasSsymm_v2_64              hipblasSsymm_64
#define cublasDsymm_v2                 hipblasDsymm
#define cublasDsymm_v2_64              hipblasDsymm_64
#define cublasCsymm_v2                 hipblasCsymm_v2
#define cublasCsymm_v2_64              hipblasCsymm_64
#define cublasZsymm_v2                 hipblasZsymm_v2
#define cublasZsymm_v2_64              hipblasZsymm_64
  // HEM
#define cublasChemm_v2                 hipblasChemm_v2
#define cublasChemm_v2_64              hipblasChemm_64
#define cublasZhemm_v2                 hipblasZhemm_v2
#define cublasZhemm_v2_64              hipblasZhemm_64
  // TRS
#define cublasStrsm_v2                 hipblasStrsm
#define cublasStrsm_v2_64              hipblasStrsm_64
#define cublasDtrsm_v2                 hipblasDtrsm
#define cublasDtrsm_v2_64              hipblasDtrsm_64
#define cublasCtrsm_v2                 hipblasCtrsm_v2
#define cublasCtrsm_v2_64              hipblasCtrsm_64
#define cublasZtrsm_v2                 hipblasZtrsm_v2
#define cublasZtrsm_v2_64              hipblasZtrsm_64
  // TRM
#define cublasStrmm_v2                 hipblasStrmm
#define cublasStrmm_v2_64              hipblasStrmm_64
#define cublasDtrmm_v2                 hipblasDtrmm
#define cublasDtrmm_v2_64              hipblasDtrmm_64
#define cublasCtrmm_v2                 hipblasCtrmm_v2
#define cublasCtrmm_v2_64              hipblasCtrmm_64
#define cublasZtrmm_v2                 hipblasZtrmm_v2
#define cublasZtrmm_v2_64              hipblasZtrmm_64
  // NRM
#define cublasSnrm2_v2                 hipblasSnrm2
#define cublasSnrm2_v2_64              hipblasSnrm2_64
#define cublasDnrm2_v2                 hipblasDnrm2
#define cublasDnrm2_v2_64              hipblasDnrm2_64
#define cublasScnrm2_v2                hipblasScnrm2_v2
#define cublasScnrm2_v2_64             hipblasScnrm2_64
#define cublasDznrm2_v2                hipblasDznrm2_v2
#define cublasDznrm2_v2_64             hipblasDznrm2_64
  // DO
#define cublasDotEx                    hipblasDotEx_v2
#define cublasDotEx_64                 hipblasDotEx_64
#define cublasDotcEx                   hipblasDotcEx_v2
#define cublasDotcEx_64                hipblasDotcEx_64
#define cublasSdot_v2                  hipblasSdot
#define cublasSdot_v2_64               hipblasSdot_64
#define cublasDdot_v2                  hipblasDdot
#define cublasDdot_v2_64               hipblasDdot_64
#define cublasCdotu_v2                 hipblasCdotu_v2
#define cublasCdotu_v2_64              hipblasCdotu_64
#define cublasCdotc_v2                 hipblasCdotc_v2
#define cublasCdotc_v2_64              hipblasCdotc_64
#define cublasZdotu_v2                 hipblasZdotu_v2
#define cublasZdotu_v2_64              hipblasZdotu_64
#define cublasZdotc_v2                 hipblasZdotc_v2
#define cublasZdotc_v2_64              hipblasZdotc_64
  // SCA
#define cublasScalEx                   hipblasScalEx_v2
#define cublasScalEx_64                hipblasScalEx_64
#define cublasSscal_v2                 hipblasSscal
#define cublasSscal_v2_64              hipblasSscal_64
#define cublasDscal_v2                 hipblasDscal
#define cublasDscal_v2_64              hipblasDscal_64
#define cublasCscal_v2                 hipblasCscal_v2
#define cublasCscal_v2_64              hipblasCscal_64
#define cublasCsscal_v2                hipblasCsscal_v2
#define cublasCsscal_v2_64             hipblasCsscal_64
#define cublasZscal_v2                 hipblasZscal_v2
#define cublasZscal_v2_64              hipblasZscal_64
#define cublasZdscal_v2                hipblasZdscal_v2
#define cublasZdscal_v2_64             hipblasZdscal_64
  // AXP
#define cublasAxpyEx                   hipblasAxpyEx_v2
#define cublasAxpyEx_64                hipblasAxpyEx_64
#define cublasSaxpy_v2                 hipblasSaxpy
#define cublasSaxpy_v2_64              hipblasSaxpy_64
#define cublasDaxpy_v2                 hipblasDaxpy
#define cublasDaxpy_v2_64              hipblasDaxpy_64
#define cublasCaxpy_v2                 hipblasCaxpy_v2
#define cublasCaxpy_v2_64              hipblasCaxpy_64
#define cublasZaxpy_v2                 hipblasZaxpy_v2
#define cublasZaxpy_v2_64              hipblasZaxpy_64
  // COP
#define cublasCopyEx                   hipblasCopyEx
#define cublasCopyEx_64                hipblasCopyEx_64
#define cublasScopy_v2                 hipblasScopy
#define cublasScopy_v2_64              hipblasScopy_64
#define cublasDcopy_v2                 hipblasDcopy
#define cublasDcopy_v2_64              hipblasDcopy_64
#define cublasCcopy_v2                 hipblasCcopy_v2
#define cublasCcopy_v2_64              hipblasCcopy_64
#define cublasZcopy_v2                 hipblasZcopy_v2
#define cublasZcopy_v2_64              hipblasZcopy_64
  // SWA
#define cublasSwapEx                   hipblasSwapEx
#define cublasSwapEx_64                hipblasSwapEx_64
#define cublasSswap_v2                 hipblasSswap
#define cublasSswap_v2_64              hipblasSswap_64
#define cublasDswap_v2                 hipblasDswap
#define cublasDswap_v2_64              hipblasDswap_64
#define cublasCswap_v2                 hipblasCswap_v2
#define cublasCswap_v2_64              hipblasCswap_64
#define cublasZswap_v2                 hipblasZswap_v2
#define cublasZswap_v2_64              hipblasZswap_64
  // AMA
#define cublasIamaxEx                  hipblasIamaxEx
#define cublasIamaxEx_64               hipblasIamaxEx_64
#define cublasIsamax_v2                hipblasIsamax
#define cublasIsamax_v2_64             hipblasIsamax_64
#define cublasIdamax_v2                hipblasIdamax
#define cublasIdamax_v2_64             hipblasIdamax_64
#define cublasIcamax_v2                hipblasIcamax_v2
#define cublasIcamax_v2_64             hipblasIcamax_64
#define cublasIzamax_v2                hipblasIzamax_v2
#define cublasIzamax_v2_64             hipblasIzamax_64
  // AMI
#define cublasIaminEx                  hipblasIaminEx
#define cublasIaminEx_64               hipblasIaminEx_64
#define cublasIsamin_v2                hipblasIsamin
#define cublasIsamin_v2_64             hipblasIsamin_64
#define cublasIdamin_v2                hipblasIdamin
#define cublasIdamin_v2_64             hipblasIdamin_64
#define cublasIcamin_v2                hipblasIcamin_v2
#define cublasIcamin_v2_64             hipblasIcamin_64
#define cublasIzamin_v2                hipblasIzamin_v2
#define cublasIzamin_v2_64             hipblasIzamin_64
  // ASU
#define cublasAsumEx                   hipblasAsumEx
#define cublasAsumEx_64                hipblasAsumEx_64
#define cublasSasum_v2                 hipblasSasum
#define cublasSasum_v2_64              hipblasSasum_64
#define cublasDasum_v2                 hipblasDasum
#define cublasDasum_v2_64              hipblasDasum_64
#define cublasScasum_v2                hipblasScasum_v2
#define cublasScasum_v2_64             hipblasScasum_64
#define cublasDzasum_v2                hipblasDzasum_v2
#define cublasDzasum_v2_64             hipblasDzasum_64
  // RO
#define cublasRotEx                    hipblasRotEx_v2
#define cublasRotEx_64                 hipblasRotEx_64
#define cublasSrot_v2                  hipblasSrot
#define cublasSrot_v2_64               hipblasSrot_64
#define cublasDrot_v2                  hipblasDrot
#define cublasDrot_v2_64               hipblasDrot_64
#define cublasCrot_v2                  hipblasCrot_v2
#define cublasCrot_v2_64               hipblasCrot_64
#define cublasCsrot_v2                 hipblasCsrot_v2
#define cublasCsrot_v2_64              hipblasCsrot_64
#define cublasZrot_v2                  hipblasZrot_v2
#define cublasZrot_v2_64               hipblasZrot_64
#define cublasZdrot_v2                 hipblasZdrot_v2
#define cublasZdrot_v2_64              hipblasZdrot_64
  // ROT
#define cublasRotgEx                   hipblasRotgEx
#define cublasSrotg_v2                 hipblasSrotg
#define cublasDrotg_v2                 hipblasDrotg
#define cublasCrotg_v2                 hipblasCrotg_v2
#define cublasZrotg_v2                 hipblasZrotg_v2
  // ROT
#define cublasRotmEx                   hipblasRotmEx
#define cublasRotmEx_64                hipblasRotmEx_64
#define cublasSrotm_v2                 hipblasSrotm
#define cublasSrotm_v2_64              hipblasSrotm_64
#define cublasDrotm_v2                 hipblasDrotm
#define cublasDrotm_v2_64              hipblasDrotm_64
  // ROTM
#define cublasRotmgEx                  hipblasRotmgEx
#define cublasSrotmg_v2                hipblasSrotmg
#define cublasDrotmg_v2                hipblasDrotmg



  // #include <rocblas/rocblas.h>
  // #define hipblasMath_t rocblas_math_mode
  // #define HIPBLAS_TF32_TENSOR_OP_MATH rocblas_xf32_xdl_math_op
  // hipblasStatus_t hipblasSetMathMode(hipblasHandle_t handle, hipblasMath_t mode);

  // rocblas_set_workspace

#define cublasSetWorkspace(...) CUBLAS_STATUS_SUCCESS
