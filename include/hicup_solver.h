
#ifndef HICUP_SOLVER_H
#define HICUP_SOLVER_H

#define __HIP_PLATFORM_AMD__
#define ROCM_MATHLIBS_API_USE_HIP_COMPLEX

#include "cuComplex.h"

#include <hipsolver/hipsolver.h>

#define cusolverStatus_t                                         hipsolverStatus_t
#define CUSOLVER_STATUS_SUCCESS                                  HIPSOLVER_STATUS_SUCCESS
#define CUSOLVER_STATUS_NOT_INITIALIZED                          HIPSOLVER_STATUS_NOT_INITIALIZED
#define CUSOLVER_STATUS_ALLOC_FAILED                             HIPSOLVER_STATUS_ALLOC_FAILED
#define CUSOLVER_STATUS_INVALID_VALUE                            HIPSOLVER_STATUS_INVALID_VALUE
#define CUSOLVER_STATUS_ARCH_MISMATCH                            HIPSOLVER_STATUS_ARCH_MISMATCH
#define CUSOLVER_STATUS_MAPPING_ERROR                            HIPSOLVER_STATUS_MAPPING_ERROR
#define CUSOLVER_STATUS_EXECUTION_FAILED                         HIPSOLVER_STATUS_EXECUTION_FAILED
#define CUSOLVER_STATUS_INTERNAL_ERROR                           HIPSOLVER_STATUS_INTERNAL_ERROR
#define CUSOLVER_STATUS_MATRIX_TYPE_NOT_SUPPORTED                HIPSOLVER_STATUS_MATRIX_TYPE_NOT_SUPPORTED
#define CUSOLVER_STATUS_NOT_SUPPORTED                            HIPSOLVER_STATUS_NOT_SUPPORTED
#define CUSOLVER_STATUS_ZERO_PIVOT                               HIPSOLVER_STATUS_ZERO_PIVOT
#define CUSOLVER_STATUS_INVALID_LICENSE                          HIPSOLVER_STATUS_INVALID_LICENSE
#define CUSOLVER_STATUS_IRS_PARAMS_NOT_INITIALIZED               HIPSOLVER_STATUS_IRS_PARAMS_NOT_INITIALIZED
#define CUSOLVER_STATUS_IRS_PARAMS_INVALID                       HIPSOLVER_STATUS_INVALID_VALUE
#define CUSOLVER_STATUS_IRS_PARAMS_INVALID_PREC                  HIPSOLVER_STATUS_IRS_PARAMS_INVALID_PREC
#define CUSOLVER_STATUS_IRS_PARAMS_INVALID_REFINE                HIPSOLVER_STATUS_IRS_PARAMS_INVALID_REFINE
#define CUSOLVER_STATUS_IRS_PARAMS_INVALID_MAXITER               HIPSOLVER_STATUS_IRS_PARAMS_INVALID_MAXITER
#define CUSOLVER_STATUS_IRS_INTERNAL_ERROR                       HIPSOLVER_STATUS_INTERNAL_ERROR
#define CUSOLVER_STATUS_IRS_NOT_SUPPORTED                        HIPSOLVER_STATUS_NOT_SUPPORTED
#define CUSOLVER_STATUS_IRS_OUT_OF_RANGE                         HIPSOLVER_STATUS_IRS_OUT_OF_RANGE
#define CUSOLVER_STATUS_IRS_NRHS_NOT_SUPPORTED_FOR_REFINE_GMRES  HIPSOLVER_STATUS_IRS_NRHS_NOT_SUPPORTED_FOR_REFINE_GMRES
#define CUSOLVER_STATUS_IRS_INFOS_NOT_INITIALIZED                HIPSOLVER_STATUS_IRS_INFOS_NOT_INITIALIZED
#define CUSOLVER_STATUS_IRS_INFOS_NOT_DESTROYED                  HIPSOLVER_STATUS_IRS_INFOS_NOT_DESTROYED
#define CUSOLVER_STATUS_IRS_MATRIX_SINGULAR                      HIPSOLVER_STATUS_IRS_MATRIX_SINGULAR
#define CUSOLVER_STATUS_INVALID_WORKSPACE                        HIPSOLVER_STATUS_INVALID_WORKSPACE
#define cusolverDnContext                                        hipsolverDnContext
#define cusolverDnHandle_t                                       hipsolverHandle_t
#define cusolverEigType_t                                        hipsolverEigType_t
#define CUSOLVER_EIG_TYPE_1                                      HIPSOLVER_EIG_TYPE_1
#define CUSOLVER_EIG_TYPE_2                                      HIPSOLVER_EIG_TYPE_2
#define CUSOLVER_EIG_TYPE_3                                      HIPSOLVER_EIG_TYPE_3
#define cusolverEigMode_t                                        hipsolverEigMode_t
#define CUSOLVER_EIG_MODE_NOVECTOR                               HIPSOLVER_EIG_MODE_NOVECTOR
#define CUSOLVER_EIG_MODE_VECTOR                                 HIPSOLVER_EIG_MODE_VECTOR
#define cusolverEigRange_t                                       hipsolverEigRange_t
#define CUSOLVER_EIG_RANGE_ALL                                   HIPSOLVER_EIG_RANGE_ALL
#define CUSOLVER_EIG_RANGE_I                                     HIPSOLVER_EIG_RANGE_I
#define CUSOLVER_EIG_RANGE_V                                     HIPSOLVER_EIG_RANGE_V
#define cusolverNorm_t                                           hipsolverNorm_t
#define CUSOLVER_INF_NORM                                        HIPSOLVER_INF_NORM
#define CUSOLVER_MAX_NORM                                        HIPSOLVER_MAX_NORM
#define CUSOLVER_ONE_NORM                                        HIPSOLVER_ONE_NORM
#define CUSOLVER_FRO_NORM                                        HIPSOLVER_FRO_NORM
#define cusolverIRSRefinement_t                                  hipsolverIRSRefinement_t
#define CUSOLVER_IRS_REFINE_NOT_SET                              HIPSOLVER_IRS_REFINE_NOT_SET
#define CUSOLVER_IRS_REFINE_NONE                                 HIPSOLVER_IRS_REFINE_NONE
#define CUSOLVER_IRS_REFINE_CLASSICAL                            HIPSOLVER_IRS_REFINE_CLASSICAL
#define CUSOLVER_IRS_REFINE_CLASSICAL_GMRES                      HIPSOLVER_IRS_REFINE_CLASSICAL_GMRES
#define CUSOLVER_IRS_REFINE_GMRES                                HIPSOLVER_IRS_REFINE_GMRES
#define CUSOLVER_IRS_REFINE_GMRES_GMRES                          HIPSOLVER_IRS_REFINE_GMRES_GMRES
#define CUSOLVER_IRS_REFINE_GMRES_NOPCOND                        HIPSOLVER_IRS_REFINE_GMRES_NOPCOND
#define CUSOLVER_PREC_DD                                         HIPSOLVER_PREC_DD
#define CUSOLVER_PREC_SS                                         HIPSOLVER_PREC_SS
#define CUSOLVER_PREC_SHT                                        HIPSOLVER_PREC_SHT
#define cusolverDirectMode_t                                     hipsolverDirectMode_t
#define CUBLAS_DIRECT_FORWARD                                    HIPBLAS_DIRECT_FORWARD
#define CUBLAS_DIRECT_BACKWARD                                   HIPBLAS_DIRECT_BACKWARD
#define cusolverStorevMode_t                                     hipsolverStorevMode_t
#define CUBLAS_STOREV_COLUMNWISE                                 HIPBLAS_STOREV_COLUMNWISE
#define CUBLAS_STOREV_ROWWISE                                    HIPBLAS_STOREV_ROWWISE
#define cusolverAlgMode_t                                        hipsolverAlgMode_t
#define CUSOLVER_ALG_0                                           HIPSOLVER_ALG_0
#define CUSOLVER_ALG_1                                           HIPSOLVER_ALG_1
#define CUSOLVER_ALG_2                                           HIPSOLVER_ALG_2
#define cusolverPrecType_t                                       hipsolverPrecType_t
#define CUSOLVER_R_8I                                            HIPSOLVER_R_8I
#define CUSOLVER_R_8U                                            HIPSOLVER_R_8U
#define CUSOLVER_R_64F                                           HIPSOLVER_R_64F
#define CUSOLVER_R_32F                                           HIPSOLVER_R_32F
#define CUSOLVER_R_16F                                           HIPSOLVER_R_16F
#define CUSOLVER_R_16BF                                          HIPSOLVER_R_16BF
#define CUSOLVER_R_TF32                                          HIPSOLVER_R_TF32
#define CUSOLVER_R_AP                                            HIPSOLVER_R_AP
#define CUSOLVER_C_8I                                            HIPSOLVER_C_8I
#define CUSOLVER_C_8U                                            HIPSOLVER_C_8U
#define CUSOLVER_C_64F                                           HIPSOLVER_C_64F
#define CUSOLVER_C_32F                                           HIPSOLVER_C_32F
#define CUSOLVER_C_16F                                           HIPSOLVER_C_16F
#define CUSOLVER_C_16BF                                          HIPSOLVER_C_16BF
#define CUSOLVER_C_TF32                                          HIPSOLVER_C_TF32
#define CUSOLVER_C_AP                                            HIPSOLVER_C_AP
#define syevjInfo                                                hipsyevjInfo
#define syevjInfo_t                                              hipsolverSyevjInfo_t
#define gesvdjInfo                                               hipgesvdjInfo
#define gesvdjInfo_t                                             hipsolverGesvdjInfo_t
#define cusolverDnIRSParams                                      hipsolverDnIRSParams
#define cusolverDnIRSParams_t                                    hipsolverDnIRSParams_t
#define cusolverDnIRSInfos                                       hipsolverDnIRSInfos
#define cusolverDnIRSInfos_t                                     hipsolverDnIRSInfos_t
#define cusolverDnParams                                         hipsolverDnParams
#define cusolverDnParams_t                                       hipsolverDnParams_t
#define cusolverDnFunction_t                                     hipsolverDnFunction_t
#define CUSOLVERDN_GETRF                                         HIPSOLVERDN_GETRF
#define CUSOLVERDN_POTRF                                         HIPSOLVERDN_POTRF
#define cusolverDeterministicMode_t                              hipsolverDeterministicMode_t
#define CUSOLVER_DETERMINISTIC_RESULTS                           HIPSOLVER_DETERMINISTIC_RESULTS
#define CUSOLVER_ALLOW_NON_DETERMINISTIC_RESULTS                 HIPSOLVER_ALLOW_NON_DETERMINISTIC_RESULTS
#define cusolver_int_t                                           int
#define cusolverDnLoggerCallback_t                               hipsolverDnLoggerCallback_t
#define cusolverMgContext                                        hipsolverMgContext
#define cusolverMgHandle_t                                       hipsolverMgHandle_t
#define cusolverMgGridMapping_t                                  hipsolverMgGridMapping_t
#define CUDALIBMG_GRID_MAPPING_ROW_MAJOR                         HIP_LIBMG_GRID_MAPPING_ROW_MAJOR
#define CUDALIBMG_GRID_MAPPING_COL_MAJOR                         HIP_LIBMG_GRID_MAPPING_COL_MAJOR
#define cudaLibMgGrid_t                                          hipLibMgGrid_t
#define cudaLibMgMatrixDesc_t                                    hipLibMgMatrixDesc_t
#define cusolverRfResetValuesFastMode_t                          hipsolverRfResetValuesFastMode_t
#define CUSOLVERRF_RESET_VALUES_FAST_MODE_OFF                    HIPSOLVERRF_RESET_VALUES_FAST_MODE_OFF
#define CUSOLVERRF_RESET_VALUES_FAST_MODE_ON                     HIPSOLVERRF_RESET_VALUES_FAST_MODE_ON
#define cusolverRfMatrixFormat_t                                 hipsolverRfMatrixFormat_t
#define CUSOLVERRF_MATRIX_FORMAT_CSR                             HIPSOLVERRF_MATRIX_FORMAT_CSR
#define CUSOLVERRF_MATRIX_FORMAT_CSC                             HIPSOLVERRF_MATRIX_FORMAT_CSC
#define cusolverRfUnitDiagonal_t                                 hipsolverRfUnitDiagonal_t
#define CUSOLVERRF_UNIT_DIAGONAL_STORED_L                        HIPSOLVERRF_UNIT_DIAGONAL_STORED_L
#define CUSOLVERRF_UNIT_DIAGONAL_STORED_U                        HIPSOLVERRF_UNIT_DIAGONAL_STORED_U
#define CUSOLVERRF_UNIT_DIAGONAL_ASSUMED_L                       HIPSOLVERRF_UNIT_DIAGONAL_ASSUMED_L
#define CUSOLVERRF_UNIT_DIAGONAL_ASSUMED_U                       HIPSOLVERRF_UNIT_DIAGONAL_ASSUMED_U
#define cusolverRfFactorization_t                                hipsolverRfFactorization_t
#define CUSOLVERRF_FACTORIZATION_ALG0                            HIPSOLVERRF_FACTORIZATION_ALG0
#define CUSOLVERRF_FACTORIZATION_ALG1                            HIPSOLVERRF_FACTORIZATION_ALG1
#define CUSOLVERRF_FACTORIZATION_ALG2                            HIPSOLVERRF_FACTORIZATION_ALG2
#define cusolverRfTriangularSolve_t                              hipsolverRfTriangularSolve_t
#define CUSOLVERRF_TRIANGULAR_SOLVE_ALG1                         HIPSOLVERRF_TRIANGULAR_SOLVE_ALG1
#define CUSOLVERRF_TRIANGULAR_SOLVE_ALG2                         HIPSOLVERRF_TRIANGULAR_SOLVE_ALG2
#define CUSOLVERRF_TRIANGULAR_SOLVE_ALG3                         HIPSOLVERRF_TRIANGULAR_SOLVE_ALG3
#define cusolverRfNumericBoostReport_t                           hipsolverRfNumericBoostReport_t
#define CUSOLVERRF_NUMERIC_BOOST_NOT_USED                        HIPSOLVERRF_NUMERIC_BOOST_NOT_USED
#define CUSOLVERRF_NUMERIC_BOOST_USED                            HIPSOLVERRF_NUMERIC_BOOST_USED
#define cusolverRfCommon                                         hipsolverRfCommon
#define cusolverRfHandle_t                                       hipsolverRfHandle_t
#define cusolverSpContext                                        hipsolverSpContext
#define cusolverSpHandle_t                                       hipsolverSpHandle_t
#define csrqrInfo                                                hipsolvercsrqrInfo
#define csrqrInfo_t                                              hipsolvercsrqrInfo_t
#define csrluInfoHost                                            hipsolvercsrluInfoHost
#define csrluInfoHost_t                                          hipsolvercsrluInfoHost_t
#define csrqrInfoHost                                            hipsolvercsrqrInfoHost
#define csrqrInfoHost_t                                          hipsolvercsrqrInfoHost_t
#define csrcholInfoHost                                          hipsolvercsrcholInfoHost
#define csrcholInfoHost_t                                        hipsolvercsrcholInfoHost_t
#define csrcholInfo                                              hipsolvercsrcholInfo
#define csrcholInfo_t                                            hipsolvercsrcholInfo_t


#define cusolverDnCreate                                    hipsolverDnCreate
#define cusolverDnDestroy                                   hipsolverDnDestroy
// NOTE: cusolverDn(S|D|C|Z)getrf -> rocsolver_(s|d|c|z)getrf + harness of other API call
#define cusolverDnSgetrf                                    hipsolverDnSgetrf
#define cusolverDnDgetrf                                    hipsolverDnDgetrf
#define cusolverDnCgetrf                                    hipsolverDnCgetrf
#define cusolverDnZgetrf                                    hipsolverDnZgetrf
// NOTE: cusolverDn(S|D|C|Z)getrf_bufferSize -> rocsolver_(s|d|c|z)getrf + harness of other API call
#define cusolverDnSgetrf_bufferSize                         hipsolverDnSgetrf_bufferSize
#define cusolverDnDgetrf_bufferSize                         hipsolverDnDgetrf_bufferSize
#define cusolverDnCgetrf_bufferSize                         hipsolverDnCgetrf_bufferSize
#define cusolverDnZgetrf_bufferSize                         hipsolverDnZgetrf_bufferSize
// NOTE: cusolverDn(S|D|C|Z)getrs -> rocsolver_(s|d|c|z)getrs + harness of other API call
#define cusolverDnSgetrs                                    hipsolverDnSgetrs
#define cusolverDnDgetrs                                    hipsolverDnDgetrs
#define cusolverDnCgetrs                                    hipsolverDnCgetrs
#define cusolverDnZgetrs                                    hipsolverDnZgetrs
#define cusolverDnXgetrf                                    hipsolverDnXgetrf
#define cusolverDnXgetrf_bufferSize                         hipsolverDnXgetrf_bufferSize
#define cusolverDnXgetrs                                    hipsolverDnXgetrs
// #define cusolverDnCreateParams                              hipsolverDnCreateParams
// #define cusolverDnDestroyParams                             hipsolverDnDestroyParams
#define cusolverDnSetAdvOptions                             hipsolverDnSetAdvOptions
#define cusolverDnSetStream                                 hipsolverSetStream
#define cusolverDnGetStream                                 hipsolverGetStream
#define cusolverDnSetDeterministicMode                      hipsolverDnSetDeterministicMode
#define cusolverDnGetDeterministicMode                      hipsolverDnGetDeterministicMode
#define cusolverDnIRSParamsCreate                           hipsolverDnIRSParamsCreate
#define cusolverDnIRSParamsDestroy                          hipsolverDnIRSParamsDestroy
#define cusolverDnIRSParamsSetRefinementSolver              hipsolverDnIRSParamsSetRefinementSolver
#define cusolverDnIRSParamsSetSolverMainPrecision           hipsolverDnIRSParamsSetSolverMainPrecision
#define cusolverDnIRSParamsSetSolverLowestPrecision         hipsolverDnIRSParamsSetSolverLowestPrecision
#define cusolverDnIRSParamsSetSolverPrecisions              hipsolverDnIRSParamsSetSolverPrecisions
#define cusolverDnIRSParamsSetTol                           hipsolverDnIRSParamsSetTol
#define cusolverDnIRSParamsSetTolInner                      hipsolverDnIRSParamsSetTolInner
#define cusolverDnIRSParamsSetMaxIters                      hipsolverDnIRSParamsSetMaxIters
#define cusolverDnIRSParamsSetMaxItersInner                 hipsolverDnIRSParamsSetMaxItersInner
#define cusolverDnIRSParamsGetMaxIters                      hipsolverDnIRSParamsGetMaxIters
#define cusolverDnIRSParamsEnableFallback                   hipsolverDnIRSParamsEnableFallback
#define cusolverDnIRSParamsDisableFallback                  hipsolverDnIRSParamsDisableFallback
#define cusolverDnIRSInfosCreate                            hipsolverDnIRSInfosCreate
#define cusolverDnIRSInfosDestroy                           hipsolverDnIRSInfosDestroy
#define cusolverDnIRSInfosGetNiters                         hipsolverDnIRSInfosGetNiters
#define cusolverDnIRSInfosGetOuterNiters                    hipsolverDnIRSInfosGetOuterNiters
#define cusolverDnIRSInfosRequestResidual                   hipsolverDnIRSInfosRequestResidual
#define cusolverDnIRSInfosGetResidualHistory                hipsolverDnIRSInfosGetResidualHistory
#define cusolverDnIRSInfosGetMaxIters                       hipsolverDnIRSInfosGetMaxIters
// NOTE: rocsolver_zgesv has a harness of rocblas_set_workspace, hipsolverZZgesv_bufferSize, and rocsolver_zgesv_
#define cusolverDnZZgesv                                    hipsolverDnZZgesv
#define cusolverDnZCgesv                                    hipsolverDnZCgesv
#define cusolverDnZKgesv                                    hipsolverDnZKgesv
#define cusolverDnZEgesv                                    hipsolverDnZEgesv
#define cusolverDnZYgesv                                    hipsolverDnZYgesv
// NOTE: rocsolver_cgesv has a harness of rocblas_set_workspace, hipsolverCCgesv_bufferSize, and rocsolver_cgesv_
#define cusolverDnCCgesv                                    hipsolverDnCCgesv
#define cusolverDnCEgesv                                    hipsolverDnCEgesv
#define cusolverDnCKgesv                                    hipsolverDnCKgesv
#define cusolverDnCYgesv                                    hipsolverDnCYgesv
// NOTE: rocsolver_dgesv has a harness of rocblas_set_workspace, hipsolverDDgesv_bufferSize, and rocsolver_dgesv_
#define cusolverDnDDgesv                                    hipsolverDnDDgesv
#define cusolverDnDSgesv                                    hipsolverDnDSgesv
#define cusolverDnDHgesv                                    hipsolverDnDHgesv
#define cusolverDnDBgesv                                    hipsolverDnDBgesv
#define cusolverDnDXgesv                                    hipsolverDnDXgesv
// NOTE: rocsolver_sgesv has a harness of rocblas_set_workspace, hipsolverSSgesv_bufferSize, and rocsolver_sgesv_
#define cusolverDnSSgesv                                    hipsolverDnSSgesv
#define cusolverDnSHgesv                                    hipsolverDnSHgesv
#define cusolverDnSBgesv                                    hipsolverDnSBgesv
#define cusolverDnSXgesv                                    hipsolverDnSXgesv
// NOTE: rocsolver_zgesv has a harness of rocblas_start_device_memory_size_query, rocsolver_zgesv_outofplace, and
#define cusolverDnZZgesv_bufferSize                         hipsolverDnZZgesv_bufferSize
#define cusolverDnZCgesv_bufferSize                         hipsolverDnZCgesv_bufferSize
#define cusolverDnZKgesv_bufferSize                         hipsolverDnZKgesv_bufferSize
#define cusolverDnZEgesv_bufferSize                         hipsolverDnZEgesv_bufferSize
#define cusolverDnZYgesv_bufferSize                         hipsolverDnZYgesv_bufferSize
// NOTE: rocsolver_cgesv has a harness of rocblas_start_device_memory_size_query, rocsolver_cgesv_outofplace, and
#define cusolverDnCCgesv_bufferSize                         hipsolverDnCCgesv_bufferSize
#define cusolverDnCKgesv_bufferSize                         hipsolverDnCKgesv_bufferSize
#define cusolverDnCEgesv_bufferSize                         hipsolverDnCEgesv_bufferSize
#define cusolverDnCYgesv_bufferSize                         hipsolverDnCYgesv_bufferSize
// NOTE: rocsolver_dgesv has a harness of rocblas_start_device_memory_size_query, rocsolver_dgesv_outofplace, and
#define cusolverDnDDgesv_bufferSize                         hipsolverDnDDgesv_bufferSize
#define cusolverDnDSgesv_bufferSize                         hipsolverDnDSgesv_bufferSize
#define cusolverDnDHgesv_bufferSize                         hipsolverDnDHgesv_bufferSize
#define cusolverDnDBgesv_bufferSize                         hipsolverDnDBgesv_bufferSize
#define cusolverDnDXgesv_bufferSize                         hipsolverDnDXgesv_bufferSize
// NOTE: rocsolver_sgesv has a harness of rocblas_start_device_memory_size_query, rocsolver_sgesv_outofplace, and
#define cusolverDnSSgesv_bufferSize                         hipsolverDnSSgesv_bufferSize
#define cusolverDnSHgesv_bufferSize                         hipsolverDnSHgesv_bufferSize
#define cusolverDnSBgesv_bufferSize                         hipsolverDnSBgesv_bufferSize
#define cusolverDnSXgesv_bufferSize                         hipsolverDnSXgesv_bufferSize
// NOTE: rocsolver_zgels has a harness of rocblas_set_workspace, hipsolverZZgels_bufferSize, hipsolverManageWorks
#define cusolverDnZZgels                                    hipsolverDnZZgels
#define cusolverDnZCgels                                    hipsolverDnZCgels
#define cusolverDnZKgels                                    hipsolverDnZKgels
#define cusolverDnZEgels                                    hipsolverDnZEgels
#define cusolverDnZYgels                                    hipsolverDnZYgels
// NOTE: rocsolver_cgels has a harness of rocblas_set_workspace, hipsolverCCgels_bufferSize, hipsolverManageWorks
#define cusolverDnCCgels                                    hipsolverDnCCgels
#define cusolverDnCKgels                                    hipsolverDnCKgels
#define cusolverDnCEgels                                    hipsolverDnCEgels
#define cusolverDnCYgels                                    hipsolverDnCYgels
// NOTE: rocsolver_dgels has a harness of rocblas_set_workspace, hipsolverDDgels_bufferSize, hipsolverManageWorks
#define cusolverDnDDgels                                    hipsolverDnDDgels
#define cusolverDnDSgels                                    hipsolverDnDSgels
#define cusolverDnDHgels                                    hipsolverDnDHgels
#define cusolverDnDBgels                                    hipsolverDnDBgels
#define cusolverDnDXgels                                    hipsolverDnDXgels
// NOTE: rocsolver_sgels has a harness of rocblas_set_workspace, hipsolverSSgels_bufferSize, hipsolverManageWorks
#define cusolverDnSSgels                                    hipsolverDnSSgels
#define cusolverDnSHgels                                    hipsolverDnSHgels
#define cusolverDnSBgels                                    hipsolverDnSBgels
#define cusolverDnSXgels                                    hipsolverDnSXgels
// NOTE: rocsolver_zgels has a harness of rocblas_start_device_memory_size_query, rocsolver_zgels_outofplace, and
#define cusolverDnZZgels_bufferSize                         hipsolverDnZZgels_bufferSize
#define cusolverDnZCgels_bufferSize                         hipsolverDnZCgels_bufferSize
#define cusolverDnZKgels_bufferSize                         hipsolverDnZKgels_bufferSize
#define cusolverDnZEgels_bufferSize                         hipsolverDnZEgels_bufferSize
#define cusolverDnZYgels_bufferSize                         hipsolverDnZYgels_bufferSize
// NOTE: rocsolver_cgels has a harness of rocblas_start_device_memory_size_query, rocsolver_cgels_outofplace, and
#define cusolverDnCCgels_bufferSize                         hipsolverDnCCgels_bufferSize
#define cusolverDnCKgels_bufferSize                         hipsolverDnCKgels_bufferSize
#define cusolverDnCEgels_bufferSize                         hipsolverDnCEgels_bufferSize
#define cusolverDnCYgels_bufferSize                         hipsolverDnCYgels_bufferSize
// NOTE: rocsolver_dgels has a harness of rocblas_start_device_memory_size_query, rocsolver_dgels_outofplace, and
#define cusolverDnDDgels_bufferSize                         hipsolverDnDDgels_bufferSize
#define cusolverDnDSgels_bufferSize                         hipsolverDnDSgels_bufferSize
#define cusolverDnDHgels_bufferSize                         hipsolverDnDHgels_bufferSize
#define cusolverDnDBgels_bufferSize                         hipsolverDnDBgels_bufferSize
#define cusolverDnDXgels_bufferSize                         hipsolverDnDXgels_bufferSize
// NOTE: rocsolver_sgels has a harness of rocblas_start_device_memory_size_query, rocsolver_sgels_outofplace, and
#define cusolverDnSSgels_bufferSize                         hipsolverDnSSgels_bufferSize
#define cusolverDnSHgels_bufferSize                         hipsolverDnSHgels_bufferSize
#define cusolverDnSBgels_bufferSize                         hipsolverDnSBgels_bufferSize
#define cusolverDnSXgels_bufferSize                         hipsolverDnSXgels_bufferSize
#define cusolverDnIRSXgesv                                  hipsolverDnIRSXgesv
#define cusolverDnIRSXgesv_bufferSize                       hipsolverDnIRSXgesv_bufferSize
#define cusolverDnIRSXgels                                  hipsolverDnIRSXgels
#define cusolverDnIRSXgels_bufferSize                       hipsolverDnIRSXgels_bufferSize
// NOTE: rocsolver_(s|d|c|z)potrf has a harness of rocblas_start_device_memory_size_query and rocblas_stop_device
#define cusolverDnSpotrf_bufferSize                         hipsolverDnSpotrf_bufferSize
#define cusolverDnDpotrf_bufferSize                         hipsolverDnDpotrf_bufferSize
#define cusolverDnCpotrf_bufferSize                         hipsolverDnCpotrf_bufferSize
#define cusolverDnZpotrf_bufferSize                         hipsolverDnZpotrf_bufferSize
// TODO: rocsolver_(s|d|c|z)potrf needs second call to calculate workspace
#define cusolverDnSpotrf                                    hipsolverDnSpotrf
#define cusolverDnDpotrf                                    hipsolverDnDpotrf
#define cusolverDnCpotrf                                    hipsolverDnCpotrf
#define cusolverDnZpotrf                                    hipsolverDnZpotrf
// NOTE: rocsolver_(s|d|c|z)potrs has a harness of rocblas_set_workspace, hipsolver(S|D|C|Z)potrs_bufferSize, hip
#define cusolverDnSpotrs                                    hipsolverDnSpotrs
#define cusolverDnDpotrs                                    hipsolverDnDpotrs
#define cusolverDnCpotrs                                    hipsolverDnCpotrs
#define cusolverDnZpotrs                                    hipsolverDnZpotrs
// NOTE: rocsolver_(s|d|c|z)potrf_batched has a harness of rocblas_set_workspace, hipsolver(S|D|C|Z)potrfBatched_
#define cusolverDnSpotrfBatched                             hipsolverDnSpotrfBatched
#define cusolverDnDpotrfBatched                             hipsolverDnDpotrfBatched
#define cusolverDnCpotrfBatched                             hipsolverDnCpotrfBatched
#define cusolverDnZpotrfBatched                             hipsolverDnZpotrfBatched
// NOTE: rocsolver_(s|d|c|z)potrs_batched has a harness of rocblas_set_workspace, hipsolver(S|D|C|Z)potrsBatched_
#define cusolverDnSpotrsBatched                             hipsolverDnSpotrsBatched
#define cusolverDnDpotrsBatched                             hipsolverDnDpotrsBatched
#define cusolverDnCpotrsBatched                             hipsolverDnCpotrsBatched
#define cusolverDnZpotrsBatched                             hipsolverDnZpotrsBatched
// NOTE: rocsolver_(s|d|c|z)potri has a harness of rocblas_start_device_memory_size_query and rocblas_stop_device
#define cusolverDnSpotri_bufferSize                         hipsolverDnSpotri_bufferSize
#define cusolverDnDpotri_bufferSize                         hipsolverDnDpotri_bufferSize
#define cusolverDnCpotri_bufferSize                         hipsolverDnCpotri_bufferSize
#define cusolverDnZpotri_bufferSize                         hipsolverDnZpotri_bufferSize
// NOTE: rocsolver_(s|d|c|z)potri has a harness of rocblas_set_workspace, hipsolver(S|D|C|Z)potri_bufferSize and
#define cusolverDnSpotri                                    hipsolverDnSpotri
#define cusolverDnDpotri                                    hipsolverDnDpotri
#define cusolverDnCpotri                                    hipsolverDnCpotri
#define cusolverDnZpotri                                    hipsolverDnZpotri
#define cusolverDnXtrtri_bufferSize                         hipsolverDnXtrtri_bufferSize
#define cusolverDnXtrtri                                    hipsolverDnXtrtri
#define cusolverDnSlauum_bufferSize                         hipsolverDnSlauum_bufferSize
#define cusolverDnDlauum_bufferSize                         hipsolverDnDlauum_bufferSize
#define cusolverDnClauum_bufferSize                         hipsolverDnClauum_bufferSize
#define cusolverDnZlauum_bufferSize                         hipsolverDnZlauum_bufferSize
#define cusolverDnSlauum                                    hipsolverDnSlauum
#define cusolverDnDlauum                                    hipsolverDnDlauum
#define cusolverDnClauum                                    hipsolverDnClauum
#define cusolverDnZlauum                                    hipsolverDnZlauum
#define cusolverDnSlaswp                                    hipsolverDnSlaswp
#define cusolverDnDlaswp                                    hipsolverDnDlaswp
#define cusolverDnClaswp                                    hipsolverDnClaswp
#define cusolverDnZlaswp                                    hipsolverDnZlaswp
// NOTE: rocsolver_(s|d|c|z)geqrf have a harness of other HIP and ROC API call
#define cusolverDnSgeqrf_bufferSize                         hipsolverDnSgeqrf_bufferSize
#define cusolverDnDgeqrf_bufferSize                         hipsolverDnDgeqrf_bufferSize
#define cusolverDnCgeqrf_bufferSize                         hipsolverDnCgeqrf_bufferSize
#define cusolverDnZgeqrf_bufferSize                         hipsolverDnZgeqrf_bufferSize
// NOTE: rocsolver_(s|d|c|z)geqrf have a harness of other HIP and ROC API call
#define cusolverDnSgeqrf                                    hipsolverDnSgeqrf
#define cusolverDnDgeqrf                                    hipsolverDnDgeqrf
#define cusolverDnCgeqrf                                    hipsolverDnCgeqrf
#define cusolverDnZgeqrf                                    hipsolverDnZgeqrf
// NOTE: rocsolver_(s|d)orgqr and rocsolver_(c|z)ungqr have a harness of other HIP and ROC API call
#define cusolverDnSorgqr_bufferSize                         hipsolverDnSorgqr_bufferSize
#define cusolverDnDorgqr_bufferSize                         hipsolverDnDorgqr_bufferSize
#define cusolverDnCungqr_bufferSize                         hipsolverDnCungqr_bufferSize
#define cusolverDnZungqr_bufferSize                         hipsolverDnZungqr_bufferSize
// NOTE: rocsolver_(s|d)orgqr and rocsolver_(c|z)ungqr have a harness of other HIP and ROC API call
#define cusolverDnSorgqr                                    hipsolverDnSorgqr
#define cusolverDnDorgqr                                    hipsolverDnDorgqr
#define cusolverDnCungqr                                    hipsolverDnCungqr
#define cusolverDnZungqr                                    hipsolverDnZungqr
// NOTE: rocsolver_(s|d)ormqr and rocsolver_(c|z)unmqr have a harness of other HIP and ROC API call
#define cusolverDnSormqr_bufferSize                         hipsolverDnSormqr_bufferSize
#define cusolverDnDormqr_bufferSize                         hipsolverDnDormqr_bufferSize
#define cusolverDnCunmqr_bufferSize                         hipsolverDnCunmqr_bufferSize
#define cusolverDnZunmqr_bufferSize                         hipsolverDnZunmqr_bufferSize
// NOTE: rocsolver_(s|d)ormqr and rocsolver_(c|z)unmqr have a harness of other HIP and ROC API call
#define cusolverDnSormqr                                    hipsolverDnSormqr
#define cusolverDnDormqr                                    hipsolverDnDormqr
#define cusolverDnCunmqr                                    hipsolverDnCunmqr
#define cusolverDnZunmqr                                    hipsolverDnZunmqr
// NOTE: rocsolver_(s|d|c|z)sytrf have a harness of other HIP and ROC API call
#define cusolverDnSsytrf_bufferSize                        hipsolverDnSsytrf_bufferSize
#define cusolverDnDsytrf_bufferSize                        hipsolverDnDsytrf_bufferSize
#define cusolverDnCsytrf_bufferSize                        hipsolverDnCsytrf_bufferSize
#define cusolverDnZsytrf_bufferSize                        hipsolverDnZsytrf_bufferSize
// NOTE: rocsolver_(s|d|c|z)sytrf have a harness of other HIP and ROC API call
#define cusolverDnSsytrf                                   hipsolverDnSsytrf
#define cusolverDnDsytrf                                   hipsolverDnDsytrf
#define cusolverDnCsytrf                                   hipsolverDnCsytrf
#define cusolverDnZsytrf                                   hipsolverDnZsytrf
#define cusolverDnXsytrs_bufferSize                        hipsolverDnXsytrs_bufferSize
#define cusolverDnXsytrs                                   hipsolverDnXsytrs
#define cusolverDnSsytri_bufferSize                        hipsolverDnSsytri_bufferSize
#define cusolverDnDsytri_bufferSize                        hipsolverDnDsytri_bufferSize
#define cusolverDnCsytri_bufferSize                        hipsolverDnCsytri_bufferSize
#define cusolverDnZsytri_bufferSize                        hipsolverDnZsytri_bufferSize
#define cusolverDnSsytri                                   hipsolverDnSsytri
#define cusolverDnDsytri                                   hipsolverDnDsytri
#define cusolverDnCsytri                                   hipsolverDnCsytri
#define cusolverDnZsytri                                   hipsolverDnZsytri
// NOTE: rocsolver_(s|d|c|z)gebrd have a harness of other HIP and ROC API call
#define cusolverDnSgebrd_bufferSize                        hipsolverDnSgebrd_bufferSize
#define cusolverDnDgebrd_bufferSize                        hipsolverDnDgebrd_bufferSize
#define cusolverDnCgebrd_bufferSize                        hipsolverDnCgebrd_bufferSize
#define cusolverDnZgebrd_bufferSize                        hipsolverDnZgebrd_bufferSize
// NOTE: rocsolver_(s|d|c|z)gebrd have a harness of other HIP and ROC API call
#define cusolverDnSgebrd                                   hipsolverDnSgebrd
#define cusolverDnDgebrd                                   hipsolverDnDgebrd
#define cusolverDnCgebrd                                   hipsolverDnCgebrd
#define cusolverDnZgebrd                                   hipsolverDnZgebrd
// NOTE: rocsolver_(s|d)orgbr and rocsolver_(c|z)ungbr have a harness of other HIP and ROC API call
#define cusolverDnSorgbr_bufferSize                        hipsolverDnSorgbr_bufferSize
#define cusolverDnDorgbr_bufferSize                        hipsolverDnDorgbr_bufferSize
#define cusolverDnCungbr_bufferSize                        hipsolverDnCungbr_bufferSize
#define cusolverDnZungbr_bufferSize                        hipsolverDnZungbr_bufferSize
// NOTE: rocsolver_(s|d)orgbr and rocsolver_(c|z)ungbr have a harness of other HIP and ROC API call
#define cusolverDnSorgbr                                   hipsolverDnSorgbr
#define cusolverDnDorgbr                                   hipsolverDnDorgbr
#define cusolverDnCungbr                                   hipsolverDnCungbr
#define cusolverDnZungbr                                   hipsolverDnZungbr
// NOTE: rocsolver_(s|d)sytrd and rocsolver_(c|z)hetrd have a harness of other HIP and ROC API call
#define cusolverDnSsytrd_bufferSize                        hipsolverDnSsytrd_bufferSize
#define cusolverDnDsytrd_bufferSize                        hipsolverDnDsytrd_bufferSize
#define cusolverDnChetrd_bufferSize                        hipsolverDnChetrd_bufferSize
#define cusolverDnZhetrd_bufferSize                        hipsolverDnZhetrd_bufferSize
// NOTE: rocsolver_(s|d)sytrd and rocsolver_(c|z)hetrd have a harness of other HIP and ROC API call
#define cusolverDnSsytrd                                   hipsolverDnSsytrd
#define cusolverDnDsytrd                                   hipsolverDnDsytrd
#define cusolverDnChetrd                                   hipsolverDnChetrd
#define cusolverDnZhetrd                                   hipsolverDnZhetrd
// NOTE: rocsolver_(s|d)orgtr and rocsolver_(c|z)ungtr have a harness of other HIP and ROC API call
#define cusolverDnSorgtr_bufferSize                        hipsolverDnSorgtr_bufferSize
#define cusolverDnDorgtr_bufferSize                        hipsolverDnDorgtr_bufferSize
#define cusolverDnCungtr_bufferSize                        hipsolverDnCungtr_bufferSize
#define cusolverDnZungtr_bufferSize                        hipsolverDnZungtr_bufferSize
// NOTE: rocsolver_(s|d)orgtr and rocsolver_(c|z)ungtr have a harness of other HIP and ROC API call
#define cusolverDnSorgtr                                   hipsolverDnSorgtr
#define cusolverDnDorgtr                                   hipsolverDnDorgtr
#define cusolverDnCungtr                                   hipsolverDnCungtr
#define cusolverDnZungtr                                   hipsolverDnZungtr
// NOTE: rocsolver_(s|d)ormtr and rocsolver_(c|z)unmtr have a harness of other HIP and ROC API call
#define cusolverDnSormtr_bufferSize                        hipsolverDnSormtr_bufferSize
#define cusolverDnDormtr_bufferSize                        hipsolverDnDormtr_bufferSize
#define cusolverDnCunmtr_bufferSize                        hipsolverDnCunmtr_bufferSize
#define cusolverDnZunmtr_bufferSize                        hipsolverDnZunmtr_bufferSize
// NOTE: rocsolver_(s|d)ormtr and rocsolver_(c|z)unmtr have a harness of other HIP and ROC API call
#define cusolverDnSormtr                                   hipsolverDnSormtr
#define cusolverDnDormtr                                   hipsolverDnDormtr
#define cusolverDnCunmtr                                   hipsolverDnCunmtr
#define cusolverDnZunmtr                                   hipsolverDnZunmtr
// NOTE: rocsolver_(s|d|c|z)gesvd have a harness of other HIP and ROC API call
#define cusolverDnSgesvd_bufferSize                        hipsolverDnSgesvd_bufferSize
#define cusolverDnDgesvd_bufferSize                        hipsolverDnDgesvd_bufferSize
#define cusolverDnCgesvd_bufferSize                        hipsolverDnCgesvd_bufferSize
#define cusolverDnZgesvd_bufferSize                        hipsolverDnZgesvd_bufferSize
// NOTE: rocsolver_(s|d|c|z)gesvd have a harness of other HIP and ROC API call
#define cusolverDnSgesvd                                   hipsolverDnSgesvd
#define cusolverDnDgesvd                                   hipsolverDnDgesvd
#define cusolverDnCgesvd                                   hipsolverDnCgesvd
#define cusolverDnZgesvd                                   hipsolverDnZgesvd
// NOTE: rocsolver_(s|d)syevd and rocsolver_(c|z)heevd have a harness of other HIP and ROC API call
#define cusolverDnSsyevd_bufferSize                        hipsolverDnSsyevd_bufferSize
#define cusolverDnDsyevd_bufferSize                        hipsolverDnDsyevd_bufferSize
#define cusolverDnCheevd_bufferSize                        hipsolverDnCheevd_bufferSize
#define cusolverDnZheevd_bufferSize                        hipsolverDnZheevd_bufferSize
// NOTE: rocsolver_(s|d)syevd and rocsolver_(c|z)heevd have a harness of other HIP and ROC API call
#define cusolverDnSsyevd                                   hipsolverDnSsyevd
#define cusolverDnDsyevd                                   hipsolverDnDsyevd
#define cusolverDnCheevd                                   hipsolverDnCheevd
#define cusolverDnZheevd                                   hipsolverDnZheevd
// NOTE: rocsolver_(s|d)syevdx_inplace and rocsolver_(c|z)heevdx_inplace have a harness of other ROC API call
#define cusolverDnSsyevdx_bufferSize                       hipsolverDnSsyevdx_bufferSize
#define cusolverDnDsyevdx_bufferSize                       hipsolverDnDsyevdx_bufferSize
#define cusolverDnCheevdx_bufferSize                       hipsolverDnCheevdx_bufferSize
#define cusolverDnZheevdx_bufferSize                       hipsolverDnZheevdx_bufferSize
// NOTE: rocsolver_(s|d)syevdx_inplace and rocsolver_(c|z)heevdx_inplace have a harness of other ROC and HIP API
#define cusolverDnSsyevdx                                  hipsolverDnSsyevdx
#define cusolverDnDsyevdx                                  hipsolverDnDsyevdx
#define cusolverDnCheevdx                                  hipsolverDnCheevdx
#define cusolverDnZheevdx                                  hipsolverDnZheevdx
// NOTE: rocsolver_(s|d)sygvdx_inplace and rocsolver_(c|z)hegvdx_inplace have a harness of other ROC and HIP API
#define cusolverDnSsygvdx_bufferSize                       hipsolverDnSsygvdx_bufferSize
#define cusolverDnDsygvdx_bufferSize                       hipsolverDnDsygvdx_bufferSize
#define cusolverDnChegvdx_bufferSize                       hipsolverDnChegvdx_bufferSize
#define cusolverDnZhegvdx_bufferSize                       hipsolverDnZhegvdx_bufferSize
// NOTE: rocsolver_(s|d)sygvdx_inplace and rocsolver_(c|z)hegvdx_inplace have a harness of other ROC and HIP API
#define cusolverDnSsygvdx                                  hipsolverDnSsygvdx
#define cusolverDnDsygvdx                                  hipsolverDnDsygvdx
#define cusolverDnChegvdx                                  hipsolverDnChegvdx
#define cusolverDnZhegvdx                                  hipsolverDnZhegvdx
// NOTE: rocsolver_(s|d)sygvd and rocsolver_(c|z)hegvd have a harness of other ROC and HIP API call
#define cusolverDnSsygvd_bufferSize                        hipsolverDnSsygvd_bufferSize
#define cusolverDnDsygvd_bufferSize                        hipsolverDnDsygvd_bufferSize
#define cusolverDnChegvd_bufferSize                        hipsolverDnChegvd_bufferSize
#define cusolverDnZhegvd_bufferSize                        hipsolverDnZhegvd_bufferSize
// NOTE: rocsolver_(s|d)sygvd and rocsolver_(c|z)hegvd have a harness of other ROC and HIP API call
#define cusolverDnSsygvd                                   hipsolverDnSsygvd
#define cusolverDnDsygvd                                   hipsolverDnDsygvd
#define cusolverDnChegvd                                   hipsolverDnChegvd
#define cusolverDnZhegvd                                   hipsolverDnZhegvd
// no ROC analogue
#define cusolverDnCreateSyevjInfo                          hipsolverDnCreateSyevjInfo
#define cusolverDnDestroySyevjInfo                         hipsolverDnDestroySyevjInfo
#define cusolverDnXsyevjSetTolerance                       hipsolverDnXsyevjSetTolerance
#define cusolverDnXsyevjSetMaxSweeps                       hipsolverDnXsyevjSetMaxSweeps
#define cusolverDnXsyevjSetSortEig                         hipsolverDnXsyevjSetSortEig
#define cusolverDnXsyevjGetResidual                        hipsolverDnXsyevjGetResidual
#define cusolverDnXsyevjGetSweeps                          hipsolverDnXsyevjGetSweeps
// NOTE: rocsolver_(s|d)syevj_strided_batched and rocsolver_(c|z)heevj_strided_batched have a harness of other RO
#define cusolverDnSsyevjBatched_bufferSize                 hipsolverDnSsyevjBatched_bufferSize
#define cusolverDnDsyevjBatched_bufferSize                 hipsolverDnDsyevjBatched_bufferSize
#define cusolverDnCheevjBatched_bufferSize                 hipsolverDnCheevjBatched_bufferSize
#define cusolverDnZheevjBatched_bufferSize                 hipsolverDnZheevjBatched_bufferSize
// NOTE: rocsolver_(s|d)syevj_strided_batched and rocsolver_(c|z)heevj_strided_batched have a harness of other RO
#define cusolverDnSsyevjBatched                            hipsolverDnSsyevjBatched
#define cusolverDnDsyevjBatched                            hipsolverDnDsyevjBatched
#define cusolverDnCheevjBatched                            hipsolverDnCheevjBatched
#define cusolverDnZheevjBatched                            hipsolverDnZheevjBatched
// NOTE: rocsolver_(s|d)syevj and rocsolver_(c|z)heevj have a harness of other ROC and HIP API call
#define cusolverDnSsyevj_bufferSize                        hipsolverDnSsyevj_bufferSize
#define cusolverDnDsyevj_bufferSize                        hipsolverDnDsyevj_bufferSize
#define cusolverDnCheevj_bufferSize                        hipsolverDnCheevj_bufferSize
#define cusolverDnZheevj_bufferSize                        hipsolverDnZheevj_bufferSize
// NOTE: rocsolver_(s|d)syevj and rocsolver_(c|z)heevj have a harness of other ROC and HIP API call
#define cusolverDnSsyevj                                   hipsolverDnSsyevj
#define cusolverDnDsyevj                                   hipsolverDnDsyevj
#define cusolverDnCheevj                                   hipsolverDnCheevj
#define cusolverDnZheevj                                   hipsolverDnZheevj
// NOTE: rocsolver_(s|d)sygvj and rocsolver_(c|z)hegvj have a harness of other ROC and HIP API call
#define cusolverDnSsygvj_bufferSize                        hipsolverDnSsygvj_bufferSize
#define cusolverDnDsygvj_bufferSize                        hipsolverDnDsygvj_bufferSize
#define cusolverDnChegvj_bufferSize                        hipsolverDnChegvj_bufferSize
#define cusolverDnZhegvj_bufferSize                        hipsolverDnZhegvj_bufferSize
// NOTE: rocsolver_(s|d)sygvj and rocsolver_(c|z)hegvj have a harness of other ROC and HIP API call
#define cusolverDnSsygvj                                   hipsolverDnSsygvj
#define cusolverDnDsygvj                                   hipsolverDnDsygvj
#define cusolverDnChegvj                                   hipsolverDnChegvj
#define cusolverDnZhegvj                                   hipsolverDnZhegvj
// no ROC analogue
#define cusolverDnCreateGesvdjInfo                         hipsolverDnCreateGesvdjInfo
#define cusolverDnDestroyGesvdjInfo                        hipsolverDnDestroyGesvdjInfo
#define cusolverDnXgesvdjSetTolerance                      hipsolverDnXgesvdjSetTolerance
#define cusolverDnXgesvdjSetMaxSweeps                      hipsolverDnXgesvdjSetMaxSweeps
#define cusolverDnXgesvdjSetSortEig                        hipsolverDnXgesvdjSetSortEig
#define cusolverDnXgesvdjGetResidual                       hipsolverDnXgesvdjGetResidual
#define cusolverDnXgesvdjGetSweeps                         hipsolverDnXgesvdjGetSweeps
// NOTE: rocsolver_(s|d|c|z)gesvdj_notransv_strided_batched have a harness of other ROC and HIP API call
#define cusolverDnSgesvdjBatched_bufferSize                hipsolverDnSgesvdjBatched_bufferSize
#define cusolverDnDgesvdjBatched_bufferSize                hipsolverDnDgesvdjBatched_bufferSize
#define cusolverDnCgesvdjBatched_bufferSize                hipsolverDnCgesvdjBatched_bufferSize
#define cusolverDnZgesvdjBatched_bufferSize                hipsolverDnZgesvdjBatched_bufferSize
// NOTE: rocsolver_(s|d|c|z)gesvdj_notransv_strided_batched have a harness of other ROC and HIP API call
#define cusolverDnSgesvdjBatched                           hipsolverDnSgesvdjBatched
#define cusolverDnDgesvdjBatched                           hipsolverDnDgesvdjBatched
#define cusolverDnCgesvdjBatched                           hipsolverDnCgesvdjBatched
#define cusolverDnZgesvdjBatched                           hipsolverDnZgesvdjBatched
// NOTE: rocsolver_(s|d|c|z)gesvdj_notransv have a harness of other ROC and HIP API call
#define cusolverDnSgesvdj_bufferSize                       hipsolverDnSgesvdj_bufferSize
#define cusolverDnDgesvdj_bufferSize                       hipsolverDnDgesvdj_bufferSize
#define cusolverDnCgesvdj_bufferSize                       hipsolverDnCgesvdj_bufferSize
#define cusolverDnZgesvdj_bufferSize                       hipsolverDnZgesvdj_bufferSize
// NOTE: rocsolver_(s|d|c|z)gesvdj_notransv have a harness of other ROC and HIP API call
#define cusolverDnSgesvdj                                  hipsolverDnSgesvdj
#define cusolverDnDgesvdj                                  hipsolverDnDgesvdj
#define cusolverDnCgesvdj                                  hipsolverDnCgesvdj
#define cusolverDnZgesvdj                                  hipsolverDnZgesvdj
// NOTE: rocsolver_(s|d|c|z)gesvdx_strided_batched have a harness of other ROC and HIP API call
#define cusolverDnSgesvdaStridedBatched_bufferSize         hipsolverDnSgesvdaStridedBatched_bufferSize
#define cusolverDnDgesvdaStridedBatched_bufferSize         hipsolverDnDgesvdaStridedBatched_bufferSize
#define cusolverDnCgesvdaStridedBatched_bufferSize         hipsolverDnCgesvdaStridedBatched_bufferSize
#define cusolverDnZgesvdaStridedBatched_bufferSize         hipsolverDnZgesvdaStridedBatched_bufferSize
// NOTE: rocsolver_(s|d|c|z)gesvdx_strided_batched have a harness of other ROC and HIP API call
#define cusolverDnSgesvdaStridedBatched                    hipsolverDnSgesvdaStridedBatched
#define cusolverDnDgesvdaStridedBatched                    hipsolverDnDgesvdaStridedBatched
#define cusolverDnCgesvdaStridedBatched                    hipsolverDnCgesvdaStridedBatched
#define cusolverDnZgesvdaStridedBatched                    hipsolverDnZgesvdaStridedBatched
#define cusolverDnPotrf_bufferSize                         hipsolverDnPotrf_bufferSize
#define cusolverDnPotrf                                    hipsolverDnPotrf
#define cusolverDnPotrs                                    hipsolverDnPotrs
#define cusolverDnGeqrf_bufferSize                         hipsolverDnGeqrf_bufferSize
#define cusolverDnGeqrf                                    hipsolverDnGeqrf
#define cusolverDnGetrf_bufferSize                         hipsolverDnGetrf_bufferSize
#define cusolverDnGetrf                                    hipsolverDnGetrf
#define cusolverDnGetrs                                    hipsolverDnGetrs
#define cusolverDnSyevd_bufferSize                         hipsolverDnSyevd_bufferSize
#define cusolverDnSyevd                                    hipsolverDnSyevd
#define cusolverDnSyevdx_bufferSize                        hipsolverDnSyevdx_bufferSize
#define cusolverDnSyevdx                                   hipsolverDnSyevdx
#define cusolverDnGesvd_bufferSize                         hipsolverDnGesvd_bufferSize
#define cusolverDnGesvd                                    hipsolverDnGesvd
#define cusolverDnXpotrf_bufferSize                        hipsolverDnXpotrf_bufferSize
#define cusolverDnXpotrf                                   hipsolverDnXpotrf
#define cusolverDnXpotrs                                   hipsolverDnXpotrs
#define cusolverDnXgeqrf_bufferSize                        hipsolverDnXgeqrf_bufferSize
#define cusolverDnXgeqrf                                   hipsolverDnXgeqrf
#define cusolverDnXsyevd_bufferSize                        hipsolverDnXsyevd_bufferSize
#define cusolverDnXsyevd                                   hipsolverDnXsyevd
#define cusolverDnXsyevdx_bufferSize                       hipsolverDnXsyevdx_bufferSize
#define cusolverDnXsyevdx                                  hipsolverDnXsyevdx
#define cusolverDnXgesvd_bufferSize                        hipsolverDnXgesvd_bufferSize
#define cusolverDnXgesvd                                   hipsolverDnXgesvd
#define cusolverDnXgesvdp_bufferSize                       hipsolverDnXgesvdp_bufferSize
#define cusolverDnXgesvdp                                  hipsolverDnXgesvdp
#define cusolverDnXgesvdr_bufferSize                       hipsolverDnXgesvdr_bufferSize
#define cusolverDnXgesvdr                                  hipsolverDnXgesvdr
#define cusolverDnLoggerSetCallback                        hipsolverDnLoggerSetCallback
#define cusolverDnLoggerSetFile                            hipsolverDnLoggerSetFile
#define cusolverDnLoggerOpenFile                           hipsolverDnLoggerOpenFile
#define cusolverDnLoggerSetLevel                           hipsolverDnLoggerSetLevel
#define cusolverDnLoggerSetMask                            hipsolverDnLoggerSetMask
#define cusolverDnLoggerForceDisable                       hipsolverDnLoggerForceDisable
#define cusolverMgCreate                                   hipsolverMgCreate
#define cusolverMgDestroy                                  hipsolverMgDestroy
#define cusolverMgDeviceSelect                             hipsolverMgDeviceSelect
#define cusolverMgCreateDeviceGrid                         hipsolverMgCreateDeviceGrid
#define cusolverMgDestroyGrid                              hipsolverMgDestroyGrid
#define cusolverMgCreateMatrixDesc                         hipsolverMgCreateMatrixDesc
#define cusolverMgSyevd_bufferSize                         hipsolverMgSyevd_bufferSize
#define cusolverMgSyevd                                    hipsolverMgSyevd
#define cusolverMgGetrf_bufferSize                         hipsolverMgGetrf_bufferSize
#define cusolverMgGetrf                                    hipsolverMgGetrf
#define cusolverMgGetrs_bufferSize                         hipsolverMgGetrs_bufferSize
#define cusolverMgGetrs                                    hipsolverMgGetrs
#define cusolverMgPotrf_bufferSize                         hipsolverMgPotrf_bufferSize
#define cusolverMgPotrf                                    hipsolverMgPotrf
#define cusolverMgPotrs_bufferSize                         hipsolverMgPotrs_bufferSize
#define cusolverMgPotrs                                    hipsolverMgPotrs
#define cusolverMgPotri_bufferSize                         hipsolverMgPotri_bufferSize
#define cusolverMgPotri                                    hipsolverMgPotri
// NOTE: rocsolver_create_rfinfo have a harness of other ROC and HIP API call
#define cusolverRfCreate                                   hipsolverRfCreate
// NOTE: rocsolver_destroy_rfinfo have a harness of other ROC and HIP API call
#define cusolverRfDestroy                                  hipsolverRfDestroy
// no ROC analogue
#define cusolverRfGetMatrixFormat                          hipsolverRfGetMatrixFormat
#define cusolverRfSetMatrixFormat                          hipsolverRfSetMatrixFormat
#define cusolverRfSetNumericProperties                     hipsolverRfSetNumericProperties
#define cusolverRfGetNumericProperties                     hipsolverRfGetNumericProperties
#define cusolverRfGetNumericBoostReport                    hipsolverRfGetNumericBoostReport
#define cusolverRfSetAlgs                                  hipsolverRfSetAlgs
#define cusolverRfGetAlgs                                  hipsolverRfGetAlgs
#define cusolverRfGetResetValuesFastMode                   hipsolverRfGetResetValuesFastMode
#define cusolverRfSetResetValuesFastMode                   hipsolverRfSetResetValuesFastMode
// NOTE: rocsolver_dcsrrf_sumlu have a harness of other ROC and HIP API call
#define cusolverRfSetupHost                                hipsolverRfSetupHost
// NOTE: rocsolver_dcsrrf_sumlu have a harness of other ROC and HIP API call
#define cusolverRfSetupDevice                              hipsolverRfSetupDevice
// no ROC analogue
#define cusolverRfResetValues                              hipsolverRfResetValues
// NOTE: can't call rocsolver_dcsrrf_analysis w/o using hipSOLVER's hipsolverRfHandl
#define cusolverRfAnalyze                                  hipsolverRfAnalyze
// NOTE: can't call rocsolver_dcsrrf_refactlu w/o using hipSOLVER's hipsolverRfHandl
#define cusolverRfRefactor                                 hipsolverRfRefactor
// no ROC analogue
#define cusolverRfAccessBundledFactorsDevice               hipsolverRfAccessBundledFactorsDevice
// no ROC analogue
#define cusolverRfExtractBundledFactorsHost                hipsolverRfExtractBundledFactorsHost
// no ROC analogue
#define cusolverRfExtractSplitFactorsHost                  hipsolverRfExtractSplitFactorsHost
// NOTE: can't call rocsolver_dcsrrf_solve w/o using hipSOLVER's hipsolverRfHandl
#define cusolverRfSolve                                    hipsolverRfSolve
// no ROC analogue
#define cusolverRfBatchSetupHost                           hipsolverRfBatchSetupHost
// no ROC analogue
#define cusolverRfBatchResetValues                         hipsolverRfBatchResetValues
// no ROC analogue
#define cusolverRfBatchAnalyze                             hipsolverRfBatchAnalyze
// no ROC analogue
#define cusolverRfBatchRefactor                            hipsolverRfBatchRefactor
// no ROC analogue
#define cusolverRfBatchSolve                               hipsolverRfBatchSolve
// no ROC analogue
#define cusolverRfBatchZeroPivot                           hipsolverRfBatchZeroPivot
#define cusolverSpCreate                                   hipsolverSpCreate
#define cusolverSpDestroy                                  hipsolverSpDestroy
#define cusolverSpSetStream                                hipsolverSpSetStream
#define cusolverSpGetStream                                hipsolverSpGetStream
#define cusolverSpXcsrissymHost                            hipsolverSpXcsrissymHost
#define cusolverSpScsrlsvluHost                            hipsolverSpScsrlsvluHost
#define cusolverSpDcsrlsvluHost                            hipsolverSpDcsrlsvluHost
#define cusolverSpCcsrlsvluHost                            hipsolverSpCcsrlsvluHost
#define cusolverSpZcsrlsvluHost                            hipsolverSpZcsrlsvluHost
#define cusolverSpScsrlsvqr                                hipsolverSpScsrlsvqr
#define cusolverSpDcsrlsvqr                                hipsolverSpDcsrlsvqr
#define cusolverSpCcsrlsvqr                                hipsolverSpCcsrlsvqr
#define cusolverSpZcsrlsvqr                                hipsolverSpZcsrlsvqr
#define cusolverSpScsrlsvqrHost                            hipsolverSpScsrlsvqrHost
#define cusolverSpDcsrlsvqrHost                            hipsolverSpDcsrlsvqrHost
#define cusolverSpCcsrlsvqrHost                            hipsolverSpCcsrlsvqrHost
#define cusolverSpZcsrlsvqrHost                            hipsolverSpZcsrlsvqrHost
#define cusolverSpScsrlsvcholHost                          hipsolverSpScsrlsvcholHost
#define cusolverSpDcsrlsvcholHost                          hipsolverSpDcsrlsvcholHost
#define cusolverSpCcsrlsvcholHost                          hipsolverSpCcsrlsvcholHost
#define cusolverSpZcsrlsvcholHost                          hipsolverSpZcsrlsvcholHost
#define cusolverSpScsrlsvchol                              hipsolverSpScsrlsvchol
#define cusolverSpDcsrlsvchol                              hipsolverSpDcsrlsvchol
#define cusolverSpCcsrlsvchol                              hipsolverSpCcsrlsvchol
#define cusolverSpZcsrlsvchol                              hipsolverSpZcsrlsvchol
#define cusolverSpScsrlsqvqrHost                           hipsolverSpScsrlsqvqrHost
#define cusolverSpDcsrlsqvqrHost                           hipsolverSpDcsrlsqvqrHost
#define cusolverSpCcsrlsqvqrHost                           hipsolverSpCcsrlsqvqrHost
#define cusolverSpZcsrlsqvqrHost                           hipsolverSpZcsrlsqvqrHost
#define cusolverSpScsreigvsiHost                           hipsolverSpScsreigvsiHost
#define cusolverSpDcsreigvsiHost                           hipsolverSpDcsreigvsiHost
#define cusolverSpCcsreigvsiHost                           hipsolverSpCcsreigvsiHost
#define cusolverSpZcsreigvsiHost                           hipsolverSpZcsreigvsiHost
#define cusolverSpScsreigvsi                               hipsolverSpScsreigvsi
#define cusolverSpDcsreigvsi                               hipsolverSpDcsreigvsi
#define cusolverSpCcsreigvsi                               hipsolverSpCcsreigvsi
#define cusolverSpZcsreigvsi                               hipsolverSpZcsreigvsi
#define cusolverSpScsreigsHost                             hipsolverSpScsreigsHost
#define cusolverSpDcsreigsHost                             hipsolverSpDcsreigsHost
#define cusolverSpCcsreigsHost                             hipsolverSpCcsreigsHost
#define cusolverSpZcsreigsHost                             hipsolverSpZcsreigsHost
#define cusolverSpXcsrsymrcmHost                           hipsolverSpXcsrsymrcmHost
#define cusolverSpXcsrsymmdqHost                           hipsolverSpXcsrsymmdqHost
#define cusolverSpXcsrsymamdHost                           hipsolverSpXcsrsymamdHost
#define cusolverSpXcsrmetisndHost                          hipsolverSpXcsrmetisndHost
#define cusolverSpScsrzfdHost                              hipsolverSpScsrzfdHost
#define cusolverSpDcsrzfdHost                              hipsolverSpDcsrzfdHost
#define cusolverSpCcsrzfdHost                              hipsolverSpCcsrzfdHost
#define cusolverSpZcsrzfdHost                              hipsolverSpZcsrzfdHost
#define cusolverSpXcsrperm_bufferSizeHost                  hipsolverSpXcsrperm_bufferSizeHost
#define cusolverSpXcsrpermHost                             hipsolverSpXcsrpermHost
#define cusolverSpCreateCsrqrInfo                          hipsolverSpCreateCsrqrInfo
#define cusolverSpDestroyCsrqrInfo                         hipsolverSpDestroyCsrqrInfo
#define cusolverSpXcsrqrAnalysisBatched                    hipsolverSpXcsrqrAnalysisBatched
#define cusolverSpScsrqrBufferInfoBatched                  hipsolverSpScsrqrBufferInfoBatched
#define cusolverSpDcsrqrBufferInfoBatched                  hipsolverSpDcsrqrBufferInfoBatched
#define cusolverSpCcsrqrBufferInfoBatched                  hipsolverSpCcsrqrBufferInfoBatched
#define cusolverSpZcsrqrBufferInfoBatched                  hipsolverSpZcsrqrBufferInfoBatched
#define cusolverSpScsrqrsvBatched                          hipsolverSpScsrqrsvBatched
#define cusolverSpDcsrqrsvBatched                          hipsolverSpDcsrqrsvBatched
#define cusolverSpCcsrqrsvBatched                          hipsolverSpCcsrqrsvBatched
#define cusolverSpZcsrqrsvBatched                          hipsolverSpZcsrqrsvBatched
#define cusolverSpCreateCsrluInfoHost                      hipsolverSpCreateCsrluInfoHost
#define cusolverSpDestroyCsrluInfoHost                     hipsolverSpDestroyCsrluInfoHost
#define cusolverSpXcsrluAnalysisHost                       hipsolverSpXcsrluAnalysisHost
#define cusolverSpScsrluBufferInfoHost                     hipsolverSpScsrluBufferInfoHost
#define cusolverSpDcsrluBufferInfoHost                     hipsolverSpDcsrluBufferInfoHost
#define cusolverSpCcsrluBufferInfoHost                     hipsolverSpCcsrluBufferInfoHost
#define cusolverSpZcsrluBufferInfoHost                     hipsolverSpZcsrluBufferInfoHost
#define cusolverSpScsrluFactorHost                         hipsolverSpScsrluFactorHost
#define cusolverSpDcsrluFactorHost                         hipsolverSpDcsrluFactorHost
#define cusolverSpCcsrluFactorHost                         hipsolverSpCcsrluFactorHost
#define cusolverSpZcsrluFactorHost                         hipsolverSpZcsrluFactorHost
#define cusolverSpScsrluZeroPivotHost                      hipsolverSpScsrluZeroPivotHost
#define cusolverSpDcsrluZeroPivotHost                      hipsolverSpDcsrluZeroPivotHost
#define cusolverSpCcsrluZeroPivotHost                      hipsolverSpCcsrluZeroPivotHost
#define cusolverSpZcsrluZeroPivotHost                      hipsolverSpZcsrluZeroPivotHost
#define cusolverSpScsrluSolveHost                          hipsolverSpScsrluSolveHost
#define cusolverSpDcsrluSolveHost                          hipsolverSpDcsrluSolveHost
#define cusolverSpCcsrluSolveHost                          hipsolverSpCcsrluSolveHost
#define cusolverSpZcsrluSolveHost                          hipsolverSpZcsrluSolveHost
#define cusolverSpXcsrluNnzHost                            hipsolverSpXcsrluNnzHost
#define cusolverSpScsrluExtractHost                        hipsolverSpScsrluExtractHost
#define cusolverSpDcsrluExtractHost                        hipsolverSpDcsrluExtractHost
#define cusolverSpCcsrluExtractHost                        hipsolverSpCcsrluExtractHost
#define cusolverSpZcsrluExtractHost                        hipsolverSpZcsrluExtractHost
#define cusolverSpCreateCsrqrInfoHost                      hipsolverSpCreateCsrqrInfoHost
#define cusolverSpDestroyCsrqrInfoHost                     hipsolverSpDestroyCsrqrInfoHost
#define cusolverSpXcsrqrAnalysisHost                       hipsolverSpXcsrqrAnalysisHost
#define cusolverSpScsrqrBufferInfoHost                     hipsolverSpScsrqrBufferInfoHost
#define cusolverSpDcsrqrBufferInfoHost                     hipsolverSpDcsrqrBufferInfoHost
#define cusolverSpCcsrqrBufferInfoHost                     hipsolverSpCcsrqrBufferInfoHost
#define cusolverSpZcsrqrBufferInfoHost                     hipsolverSpZcsrqrBufferInfoHost
#define cusolverSpScsrqrSetupHost                          hipsolverSpScsrqrSetupHost
#define cusolverSpDcsrqrSetupHost                          hipsolverSpDcsrqrSetupHost
#define cusolverSpCcsrqrSetupHost                          hipsolverSpCcsrqrSetupHost
#define cusolverSpZcsrqrSetupHost                          hipsolverSpZcsrqrSetupHost
#define cusolverSpScsrqrFactorHost                         hipsolverSpScsrqrFactorHost
#define cusolverSpDcsrqrFactorHost                         hipsolverSpDcsrqrFactorHost
#define cusolverSpCcsrqrFactorHost                         hipsolverSpCcsrqrFactorHost
#define cusolverSpZcsrqrFactorHost                         hipsolverSpZcsrqrFactorHost
#define cusolverSpScsrqrZeroPivotHost                      hipsolverSpScsrqrZeroPivotHost
#define cusolverSpDcsrqrZeroPivotHost                      hipsolverSpDcsrqrZeroPivotHost
#define cusolverSpCcsrqrZeroPivotHost                      hipsolverSpCcsrqrZeroPivotHost
#define cusolverSpZcsrqrZeroPivotHost                      hipsolverSpZcsrqrZeroPivotHost
#define cusolverSpScsrqrSolveHost                          hipsolverSpScsrqrSolveHost
#define cusolverSpDcsrqrSolveHost                          hipsolverSpDcsrqrSolveHost
#define cusolverSpCcsrqrSolveHost                          hipsolverSpCcsrqrSolveHost
#define cusolverSpZcsrqrSolveHost                          hipsolverSpZcsrqrSolveHost
#define cusolverSpXcsrqrAnalysis                           hipsolverSpXcsrqrAnalysis
#define cusolverSpScsrqrBufferInfo                         hipsolverSpScsrqrBufferInfo
#define cusolverSpDcsrqrBufferInfo                         hipsolverSpDcsrqrBufferInfo
#define cusolverSpCcsrqrBufferInfo                         hipsolverSpCcsrqrBufferInfo
#define cusolverSpZcsrqrBufferInfo                         hipsolverSpZcsrqrBufferInfo
#define cusolverSpScsrqrSetup                              hipsolverSpScsrqrSetup
#define cusolverSpDcsrqrSetup                              hipsolverSpDcsrqrSetup
#define cusolverSpCcsrqrSetup                              hipsolverSpCcsrqrSetup
#define cusolverSpZcsrqrSetup                              hipsolverSpZcsrqrSetup
#define cusolverSpScsrqrFactor                             hipsolverSpScsrqrFactor
#define cusolverSpDcsrqrFactor                             hipsolverSpDcsrqrFactor
#define cusolverSpCcsrqrFactor                             hipsolverSpCcsrqrFactor
#define cusolverSpZcsrqrFactor                             hipsolverSpZcsrqrFactor
#define cusolverSpScsrqrZeroPivot                          hipsolverSpScsrqrZeroPivot
#define cusolverSpDcsrqrZeroPivot                          hipsolverSpDcsrqrZeroPivot
#define cusolverSpCcsrqrZeroPivot                          hipsolverSpCcsrqrZeroPivot
#define cusolverSpZcsrqrZeroPivot                          hipsolverSpZcsrqrZeroPivot
#define cusolverSpScsrqrSolve                              hipsolverSpScsrqrSolve
#define cusolverSpDcsrqrSolve                              hipsolverSpDcsrqrSolve
#define cusolverSpCcsrqrSolve                              hipsolverSpCcsrqrSolve
#define cusolverSpZcsrqrSolve                              hipsolverSpZcsrqrSolve
#define cusolverSpCreateCsrcholInfoHost                    hipsolverSpCreateCsrcholInfoHost
#define cusolverSpDestroyCsrcholInfoHost                   hipsolverSpDestroyCsrcholInfoHost
#define cusolverSpXcsrcholAnalysisHost                     hipsolverSpXcsrcholAnalysisHost
#define cusolverSpScsrcholBufferInfoHost                   hipsolverSpScsrcholBufferInfoHost
#define cusolverSpDcsrcholBufferInfoHost                   hipsolverSpDcsrcholBufferInfoHost
#define cusolverSpCcsrcholBufferInfoHost                   hipsolverSpCcsrcholBufferInfoHost
#define cusolverSpZcsrcholBufferInfoHost                   hipsolverSpZcsrcholBufferInfoHost
#define cusolverSpScsrcholFactorHost                       hipsolverSpScsrcholFactorHost
#define cusolverSpDcsrcholFactorHost                       hipsolverSpDcsrcholFactorHost
#define cusolverSpCcsrcholFactorHost                       hipsolverSpCcsrcholFactorHost
#define cusolverSpZcsrcholFactorHost                       hipsolverSpZcsrcholFactorHost
#define cusolverSpScsrcholZeroPivotHost                    hipsolverSpScsrcholZeroPivotHost
#define cusolverSpDcsrcholZeroPivotHost                    hipsolverSpDcsrcholZeroPivotHost
#define cusolverSpCcsrcholZeroPivotHost                    hipsolverSpCcsrcholZeroPivotHost
#define cusolverSpZcsrcholZeroPivotHost                    hipsolverSpZcsrcholZeroPivotHost
#define cusolverSpScsrcholSolveHost                        hipsolverSpScsrcholSolveHost
#define cusolverSpDcsrcholSolveHost                        hipsolverSpDcsrcholSolveHost
#define cusolverSpCcsrcholSolveHost                        hipsolverSpCcsrcholSolveHost
#define cusolverSpZcsrcholSolveHost                        hipsolverSpZcsrcholSolveHost
#define cusolverSpCreateCsrcholInfo                        hipsolverSpCreateCsrcholInfo
#define cusolverSpDestroyCsrcholInfo                       hipsolverSpDestroyCsrcholInfo
#define cusolverSpXcsrcholAnalysis                         hipsolverSpXcsrcholAnalysis
#define cusolverSpScsrcholBufferInfo                       hipsolverSpScsrcholBufferInfo
#define cusolverSpDcsrcholBufferInfo                       hipsolverSpDcsrcholBufferInfo
#define cusolverSpCcsrcholBufferInfo                       hipsolverSpCcsrcholBufferInfo
#define cusolverSpZcsrcholBufferInfo                       hipsolverSpZcsrcholBufferInfo
#define cusolverSpScsrcholFactor                           hipsolverSpScsrcholFactor
#define cusolverSpDcsrcholFactor                           hipsolverSpDcsrcholFactor
#define cusolverSpCcsrcholFactor                           hipsolverSpCcsrcholFactor
#define cusolverSpZcsrcholFactor                           hipsolverSpZcsrcholFactor
#define cusolverSpScsrcholZeroPivot                        hipsolverSpScsrcholZeroPivot
#define cusolverSpDcsrcholZeroPivot                        hipsolverSpDcsrcholZeroPivot
#define cusolverSpCcsrcholZeroPivot                        hipsolverSpCcsrcholZeroPivot
#define cusolverSpZcsrcholZeroPivot                        hipsolverSpZcsrcholZeroPivot
#define cusolverSpScsrcholSolve                            hipsolverSpScsrcholSolve
#define cusolverSpDcsrcholSolve                            hipsolverSpDcsrcholSolve
#define cusolverSpCcsrcholSolve                            hipsolverSpCcsrcholSolve
#define cusolverSpZcsrcholSolve                            hipsolverSpZcsrcholSolve
#define cusolverSpScsrcholDiag                             hipsolverSpScsrcholDiag
#define cusolverSpDcsrcholDiag                             hipsolverSpDcsrcholDiag
#define cusolverSpCcsrcholDiag                             hipsolverSpCcsrcholDiag
#define cusolverSpZcsrcholDiag                             hipsolverSpZcsrcholDiag


#define CUSOLVER_VERSION 10299

// struct cusolverDnParams;
// typedef struct cusolverDnParams* cusolverDnParams_t;

// #define cusolverDnCreateParams(...) CUSOLVER_STATUS_SUCCESS;
// #define cusolverDnDestroyParams(...) CUSOLVER_STATUS_SUCCESS;

#endif
