

// pragma once may fail if this file is included through links so going with the std way

#ifndef HICUP_RUNTIME_H
#define HICUP_RUNTIME_H

#ifdef ENV_VISIBLE_DEVICES
#undef ENV_VISIBLE_DEVICES
#endif
#define ENV_VISIBLE_DEVICES "ROCR_VISIBLE_DEVICES"

#define __HIP_PLATFORM_AMD__

#include <hip/hip_runtime.h>

  // 1. Structs

  // no analogue
#define cudaChannelFormatDesc                                            hipChannelFormatDesc
  // no analogue
#define cudaDeviceProp                                                   hipDeviceProp_t

  // no analogue
#define cudaEglFrame                                                     hipEglFrame
#define cudaEglFrame_st                                                  hipEglFrame

  // no analogue
#define cudaEglPlaneDesc                                                 hipEglPlaneDesc
#define cudaEglPlaneDesc_st                                              hipEglPlaneDesc

  // no analogue
#define cudaExtent                                                       hipExtent

  // CUDA_EXTERNAL_MEMORY_BUFFER_DESC
#define cudaExternalMemoryBufferDesc                                     hipExternalMemoryBufferDesc

  // CUDA_EXTERNAL_MEMORY_HANDLE_DESC
#define cudaExternalMemoryHandleDesc                                     hipExternalMemoryHandleDesc

  // CUDA_EXTERNAL_MEMORY_MIPMAPPED_ARRAY_DESC
#define cudaExternalMemoryMipmappedArrayDesc                             HIP_EXTERNAL_MEMORY_MIPMAPPED_ARRAY_DESC

  // CUDA_EXTERNAL_SEMAPHORE_HANDLE_DESC
#define cudaExternalSemaphoreHandleDesc                                  hipExternalSemaphoreHandleDesc

  // CUDA_EXTERNAL_SEMAPHORE_SIGNAL_PARAMS
#define cudaExternalSemaphoreSignalParams                                hipExternalSemaphoreSignalParams
#define cudaExternalSemaphoreSignalParams_v1                             hipExternalSemaphoreSignalParams

  // CUDA_EXTERNAL_SEMAPHORE_WAIT_PARAMS
#define cudaExternalSemaphoreWaitParams                                  hipExternalSemaphoreWaitParams
#define cudaExternalSemaphoreWaitParams_v1                               hipExternalSemaphoreWaitParams

  // no analogue
#define cudaFuncAttributes                                               hipFuncAttributes

  // CUDA_HOST_NODE_PARAMS
#define cudaHostNodeParams                                               hipHostNodeParams
  // CUDA_HOST_NODE_PARAMS_v2
#define cudaHostNodeParamsV2                                             hipHostNodeParams_v2

  // CUipcEventHandle
#define cudaIpcEventHandle_t                                             hipIpcEventHandle_t
  // CUipcEventHandle_st
#define cudaIpcEventHandle_st                                            hipIpcEventHandle_st

  // CUipcMemHandle
#define cudaIpcMemHandle_t                                               hipIpcMemHandle_t
  // CUipcMemHandle_st
#define cudaIpcMemHandle_st                                              hipIpcMemHandle_st

  // CUDA_KERNEL_NODE_PARAMS
#define cudaKernelNodeParams                                             hipKernelNodeParams
  // CUDA_KERNEL_NODE_PARAMS_v2_st
#define cudaKernelNodeParamsV2                                           hipKernelNodeParams_v2

  // no analogue
  // CUDA_LAUNCH_PARAMS struct differs
#define cudaLaunchParams                                                 hipLaunchParams

  // no analogue
  // NOTE: HIP struct is bigger and contains cudaMemcpy3DParms only in the beginning
#define cudaMemcpy3DParms                                                hipMemcpy3DParms

  // no analogue
#define cudaMemcpy3DPeerParms                                            hipMemcpy3DPeerParms

  // CUDA_MEMSET_NODE_PARAMS
#define cudaMemsetParams                                                 hipMemsetParams
  // CUDA_MEMSET_NODE_PARAMS_v2
#define cudaMemsetParamsV2                                               hipMemsetParams_v2

  // no analogue
#define cudaPitchedPtr                                                   hipPitchedPtr

  // no analogue
#define cudaPointerAttributes                                            hipPointerAttribute_t

  // no analogue
#define cudaPos                                                          hipPos

  // no analogue
  // NOTE: CUDA_RESOURCE_DESC struct differs
#define cudaResourceDesc                                                 hipResourceDesc

  // NOTE: CUDA_RESOURCE_VIEW_DESC has reserved bytes in the end
#define cudaResourceViewDesc                                             hipResourceViewDesc

  // no analogue
  // NOTE: CUDA_TEXTURE_DESC differs
#define cudaTextureDesc                                                  hipTextureDesc

  // NOTE: the same struct and its name
#define CUuuid_st                                                        hipUUID_t

  // NOTE: possibly CUsurfref is analogue
#define surfaceReference                                                 surfaceReference

  // NOTE: possibly CUtexref_st is analogue
#define textureReference                                                 textureReference
#define texture                                                          texture

  // the same - CUevent_st
// #define CUevent_st                                                       ihipEvent_t
  // CUevent
#define cudaEvent_t                                                      hipEvent_t

  // CUextMemory_st
#define CUexternalMemory_st                                              hipExtMemory_st
  // CUexternalMemory
#define cudaExternalMemory_t                                             hipExternalMemory_t

  // CUextSemaphore_st
#define CUexternalSemaphore_st                                           hipExtSemaphore_st
  // CUexternalSemaphore
#define cudaExternalSemaphore_t                                          hipExternalSemaphore_t

  // the same - CUgraph_st
// #define CUgraph_st                                                       ihipGraph
  // CUgraph
#define cudaGraph_t                                                      hipGraph_t

  // the same -CUgraphExec_st
#define CUgraphExec_st                                                   hipGraphExec
  // CUgraphExec
#define cudaGraphExec_t                                                  hipGraphExec_t

  // CUgraphicsResource_st
#define cudaGraphicsResource                                             hipGraphicsResource
  // CUgraphicsResource
#define cudaGraphicsResource_t                                           hipGraphicsResource_t

  // the same - CUgraphNode_st
#define CUgraphNode_st                                                   hipGraphNode
  // CUgraphNode
#define cudaGraphNode_t                                                  hipGraphNode_t

  // CUeglStreamConnection_st
#define CUeglStreamConnection_st                                         hipEglStreamConnection
  // CUeglStreamConnection
#define cudaEglStreamConnection                                          hipEglStreamConnection

  // CUarray_st
#define cudaArray                                                        hipArray
  // CUarray
#define cudaArray_t                                                      hipArray_t
  // no analogue
#define cudaArray_const_t                                                hipArray_const_t

  // CUmipmappedArray_st
#define cudaMipmappedArray                                               hipMipmappedArray
  // CUmipmappedArray
#define cudaMipmappedArray_t                                             hipMipmappedArray_t
  // no analogue
#define cudaMipmappedArray_const_t                                       hipMipmappedArray_const_t

  // the same - CUstream_st
// #define CUstream_st                                                      ihipStream_t
  // CUstream
#define cudaStream_t                                                     hipStream_t

  // CUfunction
#define cudaFunction_t                                                   hipFunction_t

  // CUaccessPolicyWindow_st
#define cudaAccessPolicyWindow                                           hipAccessPolicyWindow

  // CUDA_ARRAY_SPARSE_PROPERTIES_st
#define cudaArraySparseProperties                                        hipArraySparseProperties

  // CUmemLocation_st
#define cudaMemLocation                                                  hipMemLocation

  // CUmemAccessDesc_st
#define cudaMemAccessDesc                                                hipMemAccessDesc

  // CUmemPoolProps_st
#define cudaMemPoolProps                                                 hipMemPoolProps

  // CUmemPoolPtrExportData_st
#define cudaMemPoolPtrExportData                                         hipMemPoolPtrExportData

  // CUDA_EXT_SEM_SIGNAL_NODE_PARAMS_st
#define cudaExternalSemaphoreSignalNodeParams                            hipExternalSemaphoreSignalNodeParams
  // CUDA_EXT_SEM_SIGNAL_NODE_PARAMS_v2_st
#define cudaExternalSemaphoreSignalNodeParamsV2                          hipExternalSemaphoreSignalNodeParams

  // CUDA_EXT_SEM_WAIT_NODE_PARAMS_st
#define cudaExternalSemaphoreWaitNodeParams                              hipExternalSemaphoreWaitNodeParams
  // CUDA_EXT_SEM_WAIT_NODE_PARAMS_v2_st
#define cudaExternalSemaphoreWaitNodeParamsV2                            hipExternalSemaphoreWaitNodeParams

  // CUDA_MEM_ALLOC_NODE_PARAMS_st
#define cudaMemAllocNodeParams                                           hipMemAllocNodeParams
  // CUDA_MEM_ALLOC_NODE_PARAMS_v2_st
#define cudaMemAllocNodeParamsV2                                         hipMemAllocNodeParams_v2

  // CUDA_MEM_FREE_NODE_PARAMS_st
#define cudaMemFreeNodeParams                                            hipMemFreeNodeParams

  // CUDA_CHILD_GRAPH_NODE_PARAMS_st
#define cudaChildGraphNodeParams                                         hipChildGraphNodeParams

  // CUDA_EVENT_RECORD_NODE_PARAMS_st
#define cudaEventRecordNodeParams                                        hipEventRecordNodeParams

  // CUDA_EVENT_WAIT_NODE_PARAMS_st
#define cudaEventWaitNodeParams                                          hipEventWaitNodeParams

  // CUgraphNodeParams_st
#define cudaGraphNodeParams                                              hipGraphNodeParams

  // CUDA_ARRAY_MEMORY_REQUIREMENTS_st
#define cudaArrayMemoryRequirements                                      hipArrayMemoryRequirements

  // CUlaunchMemSyncDomainMap_st
#define cudaLaunchMemSyncDomainMap_st                                    hipLaunchMemSyncDomainMap
  // CUlaunchMemSyncDomainMap
#define cudaLaunchMemSyncDomainMap                                       hipLaunchMemSyncDomainMap

  // the same CUkern_st
#define CUkern_st                                                        hipKernel
  // CUkernel
#define cudaKernel_t                                                     hipKernel

  // CUDA_MEMCPY_NODE_PARAMS
#define cudaMemcpyNodeParams                                             hiMemcpyNodeParams

  // CUDA_CONDITIONAL_NODE_PARAMS
#define cudaConditionalNodeParams                                        hipConditionalNodeParams

  // CUgraphEdgeData_st
#define cudaGraphEdgeData_st                                             hipGraphEdgeData
  // CUgraphEdgeData
#define cudaGraphEdgeData                                                hipGraphEdgeData

  // 2. Unions

  // CUstreamAttrValue
#define cudaStreamAttrValue                                              hipStreamAttrValue

  // CUkernelNodeAttrValue
#define cudaKernelNodeAttrValue                                          hipKernelNodeAttrValue

  // CUlaunchAttributeValue
#define cudaLaunchAttributeValue                                         hipLaunchAttributeValue

  // CUlaunchAttribute_st
#define cudaLaunchAttribute_st                                           hipLaunchAttribute
  // CUlaunchAttribute
#define cudaLaunchAttribute                                              hipLaunchAttribute

  // CUlaunchConfig_st
#define cudaLaunchConfig_st                                              hipLaunchConfig
  // CUlaunchConfig
#define cudaLaunchConfig_t                                               hipLaunchConfig

  // CUDA_GRAPH_INSTANTIATE_PARAMS_st
#define cudaGraphInstantiateParams_st                                    hipGraphInstantiateParams
  // CUDA_GRAPH_INSTANTIATE_PARAMS
#define cudaGraphInstantiateParams                                       hipGraphInstantiateParams

  // CUgraphExecUpdateResultInfo_st
#define cudaGraphExecUpdateResultInfo_st                                 hipGraphExecUpdateResultInfo
  // CUgraphExecUpdateResultInfo
#define cudaGraphExecUpdateResultInfo                                    hipGraphExecUpdateResultInfo

  // CUmemFabricHandle_st
#define cudaMemFabricHandle_st                                           hipMemFabricHandle
  // CUmemFabricHandle
#define cudaMemFabricHandle_t                                            hipMemFabricHandle

  // 3. Enums

  // no analogue
#define cudaCGScope                                                      hipCGScope
  // cudaCGScope enum values
#define cudaCGScopeInvalid                                               hipCGScopeInvalid
#define cudaCGScopeGrid                                                  hipCGScopeGrid
#define cudaCGScopeMultiGrid                                             hipCGScopeMultiGrid

  // no analogue
#define cudaChannelFormatKind                                            hipChannelFormatKind
  // cudaChannelFormatKind enum values
#define cudaChannelFormatKindSigned                                      hipChannelFormatKindSigned
#define cudaChannelFormatKindUnsigned                                    hipChannelFormatKindUnsigned
#define cudaChannelFormatKindFloat                                       hipChannelFormatKindFloat
#define cudaChannelFormatKindNone                                        hipChannelFormatKindNone
#define cudaChannelFormatKindNV12                                        hipChannelFormatKindNV12
#define cudaChannelFormatKindUnsignedNormalized8X1                       hipChannelFormatKindUnsignedNormalized8X1
#define cudaChannelFormatKindUnsignedNormalized8X2                       hipChannelFormatKindUnsignedNormalized8X2
#define cudaChannelFormatKindUnsignedNormalized8X4                       hipChannelFormatKindUnsignedNormalized8X4
#define cudaChannelFormatKindUnsignedNormalized16X1                      hipChannelFormatKindUnsignedNormalized16X1
#define cudaChannelFormatKindUnsignedNormalized16X2                      hipChannelFormatKindUnsignedNormalized16X2
#define cudaChannelFormatKindUnsignedNormalized16X4                      hipChannelFormatKindUnsignedNormalized16X4
#define cudaChannelFormatKindSignedNormalized8X1                         hipChannelFormatKindSignedNormalized8X1
#define cudaChannelFormatKindSignedNormalized8X2                         hipChannelFormatKindSignedNormalized8X2
#define cudaChannelFormatKindSignedNormalized8X4                         hipChannelFormatKindSignedNormalized8X4
#define cudaChannelFormatKindSignedNormalized16X1                        hipChannelFormatKindSignedNormalized16X1
#define cudaChannelFormatKindSignedNormalized16X2                        hipChannelFormatKindSignedNormalized16X2
#define cudaChannelFormatKindSignedNormalized16X4                        hipChannelFormatKindSignedNormalized16X4
#define cudaChannelFormatKindUnsignedBlockCompressed1                    hipChannelFormatKindUnsignedBlockCompressed1
#define cudaChannelFormatKindUnsignedBlockCompressed1SRGB                hipChannelFormatKindUnsignedBlockCompressed1SRGB
#define cudaChannelFormatKindUnsignedBlockCompressed2                    hipChannelFormatKindUnsignedBlockCompressed2
#define cudaChannelFormatKindUnsignedBlockCompressed2SRGB                hipChannelFormatKindUnsignedBlockCompressed2SRGB
#define cudaChannelFormatKindUnsignedBlockCompressed3                    hipChannelFormatKindUnsignedBlockCompressed3
#define cudaChannelFormatKindUnsignedBlockCompressed3SRGB                hipChannelFormatKindUnsignedBlockCompressed3SRGB
#define cudaChannelFormatKindUnsignedBlockCompressed4                    hipChannelFormatKindUnsignedBlockCompressed4
#define cudaChannelFormatKindSignedBlockCompressed4                      hipChannelFormatKindSignedBlockCompressed4
#define cudaChannelFormatKindUnsignedBlockCompressed5                    hipChannelFormatKindUnsignedBlockCompressed5
#define cudaChannelFormatKindSignedBlockCompressed5                      hipChannelFormatKindSignedBlockCompressed5
#define cudaChannelFormatKindUnsignedBlockCompressed6H                   hipChannelFormatKindUnsignedBlockCompressed6H
#define cudaChannelFormatKindSignedBlockCompressed6H                     hipChannelFormatKindSignedBlockCompressed6H
#define cudaChannelFormatKindUnsignedBlockCompressed7                    hipChannelFormatKindUnsignedBlockCompressed7
#define cudaChannelFormatKindUnsignedBlockCompressed7SRGB                hipChannelFormatKindUnsignedBlockCompressed7SRGB

  // CUcomputemode
#define cudaComputeMode                                                  hipComputeMode
  // cudaComputeMode enum values
  // CU_COMPUTEMODE_DEFAULT
#define cudaComputeModeDefault                                           hipComputeModeDefault
  // CU_COMPUTEMODE_EXCLUSIVE
#define cudaComputeModeExclusive                                         hipComputeModeExclusive
  // CU_COMPUTEMODE_PROHIBITED
#define cudaComputeModeProhibited                                        hipComputeModeProhibited
  // CU_COMPUTEMODE_EXCLUSIVE_PROCESS
#define cudaComputeModeExclusiveProcess                                  hipComputeModeExclusiveProcess

  // CUdevice_attribute
#define cudaDeviceAttr                                                   hipDeviceAttribute_t
  // cudaDeviceAttr enum values
  // CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK
#define cudaDevAttrMaxThreadsPerBlock                                    hipDeviceAttributeMaxThreadsPerBlock
  // CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X
#define cudaDevAttrMaxBlockDimX                                          hipDeviceAttributeMaxBlockDimX
  // CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y
#define cudaDevAttrMaxBlockDimY                                          hipDeviceAttributeMaxBlockDimY
  // CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z
#define cudaDevAttrMaxBlockDimZ                                          hipDeviceAttributeMaxBlockDimZ
  // CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X
#define cudaDevAttrMaxGridDimX                                           hipDeviceAttributeMaxGridDimX
  // CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y
#define cudaDevAttrMaxGridDimY                                           hipDeviceAttributeMaxGridDimY
  // CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z
#define cudaDevAttrMaxGridDimZ                                           hipDeviceAttributeMaxGridDimZ
  // CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK
#define cudaDevAttrMaxSharedMemoryPerBlock                               hipDeviceAttributeMaxSharedMemoryPerBlock
  // CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY
#define cudaDevAttrTotalConstantMemory                                   hipDeviceAttributeTotalConstantMemory
  // CU_DEVICE_ATTRIBUTE_WARP_SIZE
#define cudaDevAttrWarpSize                                              hipDeviceAttributeWarpSize
  // CU_DEVICE_ATTRIBUTE_MAX_PITCH
#define cudaDevAttrMaxPitch                                              hipDeviceAttributeMaxPitch
  // CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK
#define cudaDevAttrMaxRegistersPerBlock                                  hipDeviceAttributeMaxRegistersPerBlock
  // CU_DEVICE_ATTRIBUTE_CLOCK_RATE
#define cudaDevAttrClockRate                                             hipDeviceAttributeClockRate
  // CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT
#define cudaDevAttrTextureAlignment                                      hipDeviceAttributeTextureAlignment
  // CU_DEVICE_ATTRIBUTE_GPU_OVERLAP
  // NOTE: Is not deprecated as CUDA Driver's API analogue CU_DEVICE_ATTRIBUTE_GPU_OVERLAP
#define cudaDevAttrGpuOverlap                                            hipDeviceAttributeAsyncEngineCount
  // CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT
#define cudaDevAttrMultiProcessorCount                                   hipDeviceAttributeMultiprocessorCount
  // CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT
#define cudaDevAttrKernelExecTimeout                                     hipDeviceAttributeKernelExecTimeout
  // CU_DEVICE_ATTRIBUTE_INTEGRATED
#define cudaDevAttrIntegrated                                            hipDeviceAttributeIntegrated
  // CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY
#define cudaDevAttrCanMapHostMemory                                      hipDeviceAttributeCanMapHostMemory
  // CU_DEVICE_ATTRIBUTE_COMPUTE_MODE
#define cudaDevAttrComputeMode                                           hipDeviceAttributeComputeMode
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_WIDTH
#define cudaDevAttrMaxTexture1DWidth                                     hipDeviceAttributeMaxTexture1DWidth
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_WIDTH
#define cudaDevAttrMaxTexture2DWidth                                     hipDeviceAttributeMaxTexture2DWidth
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_HEIGHT
#define cudaDevAttrMaxTexture2DHeight                                    hipDeviceAttributeMaxTexture2DHeight
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH
#define cudaDevAttrMaxTexture3DWidth                                     hipDeviceAttributeMaxTexture3DWidth
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT
#define cudaDevAttrMaxTexture3DHeight                                    hipDeviceAttributeMaxTexture3DHeight
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH
#define cudaDevAttrMaxTexture3DDepth                                     hipDeviceAttributeMaxTexture3DDepth
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_WIDTH
  // CUDA only
#define cudaDevAttrMaxTexture2DLayeredWidth                              hipDeviceAttributeMaxTexture2DLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_HEIGHT
  // CUDA only
#define cudaDevAttrMaxTexture2DLayeredHeight                             hipDeviceAttributeMaxTexture2DLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_LAYERS
#define cudaDevAttrMaxTexture2DLayeredLayers                             hipDeviceAttributeMaxTexture2DLayeredLayers
  // CU_DEVICE_ATTRIBUTE_SURFACE_ALIGNMENT
  // CUDA only
#define cudaDevAttrSurfaceAlignment                                      hipDeviceAttributeSurfaceAlignment
  // CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS
#define cudaDevAttrConcurrentKernels                                     hipDeviceAttributeConcurrentKernels
  // CU_DEVICE_ATTRIBUTE_ECC_ENABLED
#define cudaDevAttrEccEnabled                                            hipDeviceAttributeEccEnabled
  // CU_DEVICE_ATTRIBUTE_PCI_BUS_ID
#define cudaDevAttrPciBusId                                              hipDeviceAttributePciBusId
  // CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID
#define cudaDevAttrPciDeviceId                                           hipDeviceAttributePciDeviceId
  // CU_DEVICE_ATTRIBUTE_TCC_DRIVER
  // CUDA only
#define cudaDevAttrTccDriver                                             hipDeviceAttributeTccDriver
  // CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE
#define cudaDevAttrMemoryClockRate                                       hipDeviceAttributeMemoryClockRate
  // CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH
#define cudaDevAttrGlobalMemoryBusWidth                                  hipDeviceAttributeMemoryBusWidth
  // CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE
#define cudaDevAttrL2CacheSize                                           hipDeviceAttributeL2CacheSize
  // CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR
#define cudaDevAttrMaxThreadsPerMultiProcessor                           hipDeviceAttributeMaxThreadsPerMultiProcessor
  // CU_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT
  // CUDA only
#define cudaDevAttrAsyncEngineCount                                      hipDeviceAttributeAsyncEngineCount
  // CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING
  // CUDA only
#define cudaDevAttrUnifiedAddressing                                     hipDeviceAttributeUnifiedAddressing
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_WIDTH
  // CUDA only
#define cudaDevAttrMaxTexture1DLayeredWidth                              hipDeviceAttributeMaxTexture1DLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_LAYERS
#define cudaDevAttrMaxTexture1DLayeredLayers                             hipDeviceAttributeMaxTexture1DLayeredLayers
  // 44 - no
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_GATHER_WIDTH
  // CUDA only
#define cudaDevAttrMaxTexture2DGatherWidth                               hipDeviceAttributeMaxTexture2DGather
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_GATHER_HEIGHT
  // CUDA only
#define cudaDevAttrMaxTexture2DGatherHeight                              hipDeviceAttributeMaxTexture2DGather
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH_ALTERNATE
  // CUDA only
#define cudaDevAttrMaxTexture3DWidthAlt                                  hipDeviceAttributeMaxTexture3DAlt
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT_ALTERNATE
  // CUDA only
#define cudaDevAttrMaxTexture3DHeightAlt                                 hipDeviceAttributeMaxTexture3DAlt
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH_ALTERNATE
  // CUDA only
#define cudaDevAttrMaxTexture3DDepthAlt                                  hipDeviceAttributeMaxTexture3DAlt
  // CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID
#define cudaDevAttrPciDomainId                                           hipDeviceAttributePciDomainID
  // CU_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT
#define cudaDevAttrTexturePitchAlignment                                 hipDeviceAttributeTexturePitchAlignment
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_WIDTH
  // CUDA only
#define cudaDevAttrMaxTextureCubemapWidth                                hipDeviceAttributeMaxTextureCubemap
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_LAYERED_WIDTH
  // CUDA only
#define cudaDevAttrMaxTextureCubemapLayeredWidth                         hipDeviceAttributeMaxTextureCubemapLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_LAYERED_LAYERS
#define cudaDevAttrMaxTextureCubemapLayeredLayers                        hipDeviceAttributeMaxTextureCubemapLayeredLayers
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_WIDTH
#define cudaDevAttrMaxSurface1DWidth                                     hipDeviceAttributeMaxSurface1D
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_WIDTH
#define cudaDevAttrMaxSurface2DWidth                                     hipDeviceAttributeMaxSurface2D
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_HEIGHT
#define cudaDevAttrMaxSurface2DHeight                                    hipDeviceAttributeMaxSurface2D
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_WIDTH
#define cudaDevAttrMaxSurface3DWidth                                     hipDeviceAttributeMaxSurface3D
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_HEIGHT
#define cudaDevAttrMaxSurface3DHeight                                    hipDeviceAttributeMaxSurface3D
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_DEPTH
#define cudaDevAttrMaxSurface3DDepth                                     hipDeviceAttributeMaxSurface3D
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_LAYERED_WIDTH
  // CUDA only
#define cudaDevAttrMaxSurface1DLayeredWidth                              hipDeviceAttributeMaxSurface1DLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_LAYERED_LAYERS
#define cudaDevAttrMaxSurface1DLayeredLayers                             hipDeviceAttributeMaxSurface1DLayeredLayers
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_WIDTH
  // CUDA only
#define cudaDevAttrMaxSurface2DLayeredWidth                              hipDeviceAttributeMaxSurface2DLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_HEIGHT
  // CUDA only
#define cudaDevAttrMaxSurface2DLayeredHeight                             hipDeviceAttributeMaxSurface2DLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_LA  YERS
#define cudaDevAttrMaxSurface2DLayeredLayers                             hipDeviceAttributeMaxSurface2DLayeredLayers
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_WIDTH
  // CUDA only
#define cudaDevAttrMaxSurfaceCubemapWidth                                hipDeviceAttributeMaxSurfaceCubemap
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_LAYERED_WIDTH
  // CUDA only
#define cudaDevAttrMaxSurfaceCubemapLayeredWidth                         hipDeviceAttributeMaxSurfaceCubemapLayered
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_LAYERED_LAYERS
#define cudaDevAttrMaxSurfaceCubemapLayeredLayers                        hipDeviceAttributeMaxSurfaceCubemapLayeredLayers
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LINEAR_WIDTH
#define cudaDevAttrMaxTexture1DLinearWidth                               hipDeviceAttributeMaxTexture1DLinear
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_WIDTH
  // CUDA only
#define cudaDevAttrMaxTexture2DLinearWidth                               hipDeviceAttributeMaxTexture2DLinear
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_HEIGHT
  // CUDA only
#define cudaDevAttrMaxTexture2DLinearHeight                              hipDeviceAttributeMaxTexture2DLinear
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_PITCH
  // CUDA only
#define cudaDevAttrMaxTexture2DLinearPitch                               hipDeviceAttributeMaxTexture2DLinear
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_MIPMAPPED_WIDTH
  // CUDA only
#define cudaDevAttrMaxTexture2DMipmappedWidth                            hipDeviceAttributeMaxTexture2DMipmap
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_MIPMAPPED_HEIGHT
  // CUDA only
#define cudaDevAttrMaxTexture2DMipmappedHeight                           hipDeviceAttributeMaxTexture2DMipmap
  // CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR
#define cudaDevAttrComputeCapabilityMajor                                hipDeviceAttributeComputeCapabilityMajor
  // CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR
#define cudaDevAttrComputeCapabilityMinor                                hipDeviceAttributeComputeCapabilityMinor
  // CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_MIPMAPPED_WIDTH
  // CUDA only
#define cudaDevAttrMaxTexture1DMipmappedWidth                            hipDeviceAttributeMaxTexture1DMipmap
  // CU_DEVICE_ATTRIBUTE_STREAM_PRIORITIES_SUPPORTED
  // CUDA only
#define cudaDevAttrStreamPrioritiesSupported                             hipDeviceAttributeStreamPrioritiesSupported
  // CU_DEVICE_ATTRIBUTE_GLOBAL_L1_CACHE_SUPPORTED
  // CUDA only
#define cudaDevAttrGlobalL1CacheSupported                                hipDeviceAttributeGlobalL1CacheSupported
  // CU_DEVICE_ATTRIBUTE_LOCAL_L1_CACHE_SUPPORTED
#define cudaDevAttrLocalL1CacheSupported                                 hipDeviceAttributeLocalL1CacheSupported
  // CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR
#define cudaDevAttrMaxSharedMemoryPerMultiprocessor                      hipDeviceAttributeMaxSharedMemoryPerMultiprocessor
  // CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR
#define cudaDevAttrMaxRegistersPerMultiprocessor                         hipDeviceAttributeMaxRegistersPerMultiprocessor
  // CU_DEVICE_ATTRIBUTE_MANAGED_MEMORY
#define cudaDevAttrManagedMemory                                         hipDeviceAttributeManagedMemory
  // CU_DEVICE_ATTRIBUTE_MULTI_GPU_BOARD
#define cudaDevAttrIsMultiGpuBoard                                       hipDeviceAttributeIsMultiGpuBoard
  // CU_DEVICE_ATTRIBUTE_MULTI_GPU_BOARD_GROUP_ID
  // CUDA only
#define cudaDevAttrMultiGpuBoardGroupID                                  hipDeviceAttributeMultiGpuBoardGroupID
  // CU_DEVICE_ATTRIBUTE_HOST_NATIVE_ATOMIC_SUPPORTED
  // CUDA only
#define cudaDevAttrHostNativeAtomicSupported                             hipDeviceAttributeHostNativeAtomicSupported
  // CU_DEVICE_ATTRIBUTE_SINGLE_TO_DOUBLE_PRECISION_PERF_RATIO
  // CUDA only
#define cudaDevAttrSingleToDoublePrecisionPerfRatio                      hipDeviceAttributeSingleToDoublePrecisionPerfRatio
  // CU_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS
#define cudaDevAttrPageableMemoryAccess                                  hipDeviceAttributePageableMemoryAccess
  // CU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS
#define cudaDevAttrConcurrentManagedAccess                               hipDeviceAttributeConcurrentManagedAccess
  // CU_DEVICE_ATTRIBUTE_COMPUTE_PREEMPTION_SUPPORTED
  // CUDA only
#define cudaDevAttrComputePreemptionSupported                            hipDeviceAttributeComputePreemptionSupported
  // CU_DEVICE_ATTRIBUTE_CAN_USE_HOST_POINTER_FOR_REGISTERED_MEM
  // CUDA only
#define cudaDevAttrCanUseHostPointerForRegisteredMem                     hipDeviceAttributeCanUseHostPointerForRegisteredMem
  // CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS
#define cudaDevAttrReserved92                                            hipDeviceAttributeCanUseStreamMemOps
  // CU_DEVICE_ATTRIBUTE_CAN_USE_64_BIT_STREAM_MEM_OPS
#define cudaDevAttrReserved93                                            hipDeviceAttributeCanUse64BitStreamMemOps
  // CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_WAIT_VALUE_NOR
#define cudaDevAttrReserved94                                            hipDeviceAttributeCanUseStreamWaitValue
  // CU_DEVICE_ATTRIBUTE_COOPERATIVE_LAUNCH
#define cudaDevAttrCooperativeLaunch                                     hipDeviceAttributeCooperativeLaunch
  // CU_DEVICE_ATTRIBUTE_COOPERATIVE_MULTI_DEVICE_LAUNCH
#define cudaDevAttrCooperativeMultiDeviceLaunch                          hipDeviceAttributeCooperativeMultiDeviceLaunch
  // CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK_OPTIN
  // CUDA only
#define cudaDevAttrMaxSharedMemoryPerBlockOptin                          hipDeviceAttributeSharedMemPerBlockOptin
  // CU_DEVICE_ATTRIBUTE_CAN_FLUSH_REMOTE_WRITES
#define cudaDevAttrCanFlushRemoteWrites                                  hipDeviceAttributeCanFlushRemoteWrites
  // CU_DEVICE_ATTRIBUTE_HOST_REGISTER_SUPPORTED
#define cudaDevAttrHostRegisterSupported                                 hipDeviceAttributeHostRegisterSupported
  // CU_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS_USES_HOST_PAGE_TABLES
#define cudaDevAttrPageableMemoryAccessUsesHostPageTables                hipDeviceAttributePageableMemoryAccessUsesHostPageTables
  // CU_DEVICE_ATTRIBUTE_DIRECT_MANAGED_MEM_ACCESS_FROM_HOST
#define cudaDevAttrDirectManagedMemAccessFromHost                        hipDeviceAttributeDirectManagedMemAccessFromHost
  // CU_DEVICE_ATTRIBUTE_MAX_BLOCKS_PER_MULTIPROCESSOR
  // CUDA only
#define cudaDevAttrMaxBlocksPerMultiprocessor                            hipDeviceAttributeMaxBlocksPerMultiprocessor
  // CU_DEVICE_ATTRIBUTE_MAX_PERSISTING_L2_CACHE_SIZE
#define cudaDevAttrMaxPersistingL2CacheSize                              hipDeviceAttributeMaxPersistingL2CacheSize
  // CU_DEVICE_ATTRIBUTE_MAX_ACCESS_POLICY_WINDOW_SIZE
#define cudaDevAttrMaxAccessPolicyWindowSize                             hipDeviceAttributeMaxAccessPolicyWindowSize
  // CU_DEVICE_ATTRIBUTE_RESERVED_SHARED_MEMORY_PER_BLOCK
#define cudaDevAttrReservedSharedMemoryPerBlock                          hipDeviceAttributeReservedSharedMemoryPerBlock
  // CU_DEVICE_ATTRIBUTE_SPARSE_CUDA_ARRAY_SUPPORTED
#define cudaDevAttrSparseCudaArraySupported                              hipDeviceAttributeSparseCudaArraySupported
  // CU_DEVICE_ATTRIBUTE_READ_ONLY_HOST_REGISTER_SUPPORTED
#define cudaDevAttrHostRegisterReadOnlySupported                         hipDeviceAttributeReadOnlyHostRestigerSupported
  // CU_DEVICE_ATTRIBUTE_TIMELINE_SEMAPHORE_INTEROP_SUPPORTED
#define cudaDevAttrMaxTimelineSemaphoreInteropSupported                  hipDeviceAttributeMaxTimelineSemaphoreInteropSupported
  // CU_DEVICE_ATTRIBUTE_TIMELINE_SEMAPHORE_INTEROP_SUPPORTED
#define cudaDevAttrTimelineSemaphoreInteropSupported                     hipDeviceAttributeTimelineSemaphoreInteropSupported
  // CU_DEVICE_ATTRIBUTE_MEMORY_POOLS_SUPPORTED
#define cudaDevAttrMemoryPoolsSupported                                  hipDeviceAttributeMemoryPoolsSupported
  // CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_SUPPORTED
#define cudaDevAttrGPUDirectRDMASupported                                hipDeviceAttributeGPUDirectRDMASupported
  // CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_FLUSH_WRITES_OPTIONS
#define cudaDevAttrGPUDirectRDMAFlushWritesOptions                       hipDeviceAttributeGpuDirectRdmaFlushWritesOptions
  // CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_WRITES_ORDERING
#define cudaDevAttrGPUDirectRDMAWritesOrdering                           hipDeviceAttributeGpuDirectRdmaWritesOrdering
  // CU_DEVICE_ATTRIBUTE_MEMPOOL_SUPPORTED_HANDLE_TYPES
#define cudaDevAttrMemoryPoolSupportedHandleTypes                        hipDeviceAttributeMempoolSupportedHandleTypes
  // CU_DEVICE_ATTRIBUTE_CLUSTER_LAUNCH
#define cudaDevAttrClusterLaunch                                         hipDeviceAttributeClusterLaunch
  // CU_DEVICE_ATTRIBUTE_DEFERRED_MAPPING_CUDA_ARRAY_SUPPORTED
#define cudaDevAttrDeferredMappingCudaArraySupported                     hipDeviceAttributeDeferredMappingCudaArraySupported
  // CU_DEVICE_ATTRIBUTE_CAN_USE_64_BIT_STREAM_MEM_OPS_V2
#define cudaDevAttrReserved122                                           hipDevAttrReserved122
  // CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_WAIT_VALUE_NOR_V2
#define cudaDevAttrReserved123                                           hipDevAttrReserved123
  // CU_DEVICE_ATTRIBUTE_DMA_BUF_SUPPORTED
#define cudaDevAttrReserved124                                           hipDevAttrReserved124
  // CU_DEVICE_ATTRIBUTE_IPC_EVENT_SUPPORTED
#define cudaDevAttrIpcEventSupport                                       hipDevAttrIpcEventSupport
  // CU_DEVICE_ATTRIBUTE_MEM_SYNC_DOMAIN_COUNT
#define cudaDevAttrMemSyncDomainCount                                    hipDevAttrMemSyncDomainCount
  // CU_DEVICE_ATTRIBUTE_TENSOR_MAP_ACCESS_SUPPORTED
#define cudaDevAttrReserved127                                           hipDeviceAttributeTensorMapAccessSupported
  // CUDA only
#define cudaDevAttrReserved128                                           hipDevAttrReserved128
  // CU_DEVICE_ATTRIBUTE_UNIFIED_FUNCTION_POINTERS
#define cudaDevAttrReserved129                                           hipDeviceAttributeUnifiedFunctionPointers
  // CU_DEVICE_ATTRIBUTE_NUMA_CONFIG
#define cudaDevAttrNumaConfig                                            hipDeviceAttributeNumaConfig
  // CU_DEVICE_ATTRIBUTE_NUMA_ID
#define cudaDevAttrNumaId                                                hipDeviceAttributeNumaId
  // CU_DEVICE_ATTRIBUTE_MULTICAST_SUPPORTED
#define cudaDevAttrReserved132                                           hipDeviceAttributeMulticastSupported
  // CU_DEVICE_ATTRIBUTE_MPS_ENABLED
#define cudaDevAttrMpsEnabled                                            hipDeviceAttributeMpsEnables
  // CU_DEVICE_ATTRIBUTE_HOST_NUMA_ID
#define cudaDevAttrHostNumaId                                            hipDeviceAttributeHostNumaId
  // CU_DEVICE_ATTRIBUTE_MAX
#define cudaDevAttrMax                                                   hipDeviceAttributeMax

  // CUdevice_P2PAttribute
#define cudaDeviceP2PAttr                                                hipDeviceP2PAttr
  // cudaDeviceP2PAttr enum values
  // CU_DEVICE_P2P_ATTRIBUTE_PERFORMANCE_RANK = 0x01
#define cudaDevP2PAttrPerformanceRank                                    hipDevP2PAttrPerformanceRank
  // CU_DEVICE_P2P_ATTRIBUTE_ACCESS_SUPPORTED = 0x02
#define cudaDevP2PAttrAccessSupported                                    hipDevP2PAttrAccessSupported
  // CU_DEVICE_P2P_ATTRIBUTE_NATIVE_ATOMIC_SUPPORTED = 0x03
#define cudaDevP2PAttrNativeAtomicSupported                              hipDevP2PAttrNativeAtomicSupported
  // CU_DEVICE_P2P_ATTRIBUTE_CUDA_ARRAY_ACCESS_SUPPORTED = 0x04
#define cudaDevP2PAttrCudaArrayAccessSupported                           hipDevP2PAttrHipArrayAccessSupported

  // cudaEGL.h - presented only on Linux in nvidia-cuda-dev package
  // CUeglColorFormat
#define cudaEglColorFormat                                               hipEglColorFormat
  // cudaEglColorFormat enum values
  // CU_EGL_COLOR_FORMAT_YUV420_PLANAR = 0x00
#define cudaEglColorFormatYUV420Planar                                   hipEglColorFormatYUV420Planar
  // CU_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR= 0x01
#define cudaEglColorFormatYUV420SemiPlanar                               hipEglColorFormatYUV420SemiPlanar
  // CU_EGL_COLOR_FORMAT_YUV422_PLANAR = 0x02
#define cudaEglColorFormatYUV422Planar                                   hipEglColorFormatYUV422Planar
  // CU_EGL_COLOR_FORMAT_YUV422_SEMIPLANAR = 0x03
#define cudaEglColorFormatYUV422SemiPlanar                               hipEglColorFormatYUV422SemiPlanar
  // CU_EGL_COLOR_FORMAT_RGB = 0x04
#define cudaEglColorFormatRGB                                            hipEglColorFormatRGB
  // CU_EGL_COLOR_FORMAT_BGR = 0x05
#define cudaEglColorFormatBGR                                            hipEglColorFormatBGR
  // CU_EGL_COLOR_FORMAT_ARGB = 0x06
#define cudaEglColorFormatARGB                                           hipEglColorFormatARGB
  // CU_EGL_COLOR_FORMAT_RGBA = 0x07
#define cudaEglColorFormatRGBA                                           hipEglColorFormatRGBA
  // CU_EGL_COLOR_FORMAT_L = 0x08
#define cudaEglColorFormatL                                              hipEglColorFormatL
  // CU_EGL_COLOR_FORMAT_R = 0x09
#define cudaEglColorFormatR                                              hipEglColorFormatR
  // CU_EGL_COLOR_FORMAT_YUV444_PLANAR = 0x0A
#define cudaEglColorFormatYUV444Planar                                   hipEglColorFormatYUV444Planar
  // CU_EGL_COLOR_FORMAT_YUV444_SEMIPLANAR = 0x0B
#define cudaEglColorFormatYUV444SemiPlanar                               hipEglColorFormatYUV444SemiPlanar
  // CU_EGL_COLOR_FORMAT_YUYV_422 = 0x0C
#define cudaEglColorFormatYUYV422                                        hipEglColorFormatYUYV422
  // CU_EGL_COLOR_FORMAT_UYVY_422 = 0x0D
#define cudaEglColorFormatUYVY422                                        hipEglColorFormatUYVY422
  // CU_EGL_COLOR_FORMAT_ABGR = 0x0E
#define cudaEglColorFormatABGR                                           hipEglColorFormatABGR
  // CU_EGL_COLOR_FORMAT_BGRA = 0x0F
#define cudaEglColorFormatBGRA                                           hipEglColorFormatBGRA
  // CU_EGL_COLOR_FORMAT_A = 0x10
#define cudaEglColorFormatA                                              hipEglColorFormatA
  // CU_EGL_COLOR_FORMAT_RG = 0x11
#define cudaEglColorFormatRG                                             hipEglColorFormatRG
  // CU_EGL_COLOR_FORMAT_AYUV = 0x12
#define cudaEglColorFormatAYUV                                           hipEglColorFormatAYUV
  // CU_EGL_COLOR_FORMAT_YVU444_SEMIPLANAR = 0x13
#define cudaEglColorFormatYVU444SemiPlanar                               hipEglColorFormatYVU444SemiPlanar
  // CU_EGL_COLOR_FORMAT_YVU422_SEMIPLANAR = 0x14
#define cudaEglColorFormatYVU422SemiPlanar                               hipEglColorFormatYVU422SemiPlanar
  // CU_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR = 0x15
#define cudaEglColorFormatYVU420SemiPlanar                               hipEglColorFormatYVU420SemiPlanar
  // CU_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR = 0x16
#define cudaEglColorFormatY10V10U10_444SemiPlanar                        hipEglColorFormatY10V10U10_444SemiPlanar
  // CU_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR = 0x17
#define cudaEglColorFormatY10V10U10_420SemiPlanar                        hipEglColorFormatY10V10U10_420SemiPlanar
  // CU_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR = 0x18
#define cudaEglColorFormatY12V12U12_444SemiPlanar                        hipEglColorFormatY12V12U12_444SemiPlanar
  // CU_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR = 0x19
#define cudaEglColorFormatY12V12U12_420SemiPlanar                        hipEglColorFormatY12V12U12_420SemiPlanar
  // CU_EGL_COLOR_FORMAT_VYUY_ER = 0x1A
#define cudaEglColorFormatVYUY_ER                                        hipEglColorFormatVYUY_ER
  // CU_EGL_COLOR_FORMAT_UYVY_ER = 0x1B
#define cudaEglColorFormatUYVY_ER                                        hipEglColorFormatUYVY_ER
  // CU_EGL_COLOR_FORMAT_YUYV_ER = 0x1C
#define cudaEglColorFormatYUYV_ER                                        hipEglColorFormatYUYV_ER
  // CU_EGL_COLOR_FORMAT_YVYU_ER = 0x1D
#define cudaEglColorFormatYVYU_ER                                        hipEglColorFormatYVYU_ER
  // CU_EGL_COLOR_FORMAT_YUV_ER = 0x1E
#define cudaEglColorFormatYUV_ER                                         hipEglColorFormatYUV_ER
  // CU_EGL_COLOR_FORMAT_YUVA_ER = 0x1F
#define cudaEglColorFormatYUVA_ER                                        hipEglColorFormatYUVA_ER
  // CU_EGL_COLOR_FORMAT_AYUV_ER = 0x20
#define cudaEglColorFormatAYUV_ER                                        hipEglColorFormatAYUV_ER
  // CU_EGL_COLOR_FORMAT_YUV444_PLANAR_ER = 0x21
#define cudaEglColorFormatYUV444Planar_ER                                hipEglColorFormatYUV444Planar_ER
  // CU_EGL_COLOR_FORMAT_YUV422_PLANAR_ER = 0x22
#define cudaEglColorFormatYUV422Planar_ER                                hipEglColorFormatYUV422Planar_ER
  // CU_EGL_COLOR_FORMAT_YUV420_PLANAR_ER = 0x23
#define cudaEglColorFormatYUV420Planar_ER                                hipEglColorFormatYUV420Planar_ER
  // CU_EGL_COLOR_FORMAT_YUV444_SEMIPLANAR_ER = 0x24
#define cudaEglColorFormatYUV444SemiPlanar_ER                            hipEglColorFormatYUV444SemiPlanar_ER
  // CU_EGL_COLOR_FORMAT_YUV422_SEMIPLANAR_ER = 0x25
#define cudaEglColorFormatYUV422SemiPlanar_ER                            hipEglColorFormatYUV422SemiPlanar_ER
  // CU_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_ER = 0x26
#define cudaEglColorFormatYUV420SemiPlanar_ER                            hipEglColorFormatYUV420SemiPlanar_ER
  // CU_EGL_COLOR_FORMAT_YVU444_PLANAR_ER = 0x27
#define cudaEglColorFormatYVU444Planar_ER                                hipEglColorFormatYVU444Planar_ER
  // CU_EGL_COLOR_FORMAT_YVU422_PLANAR_ER = 0x28
#define cudaEglColorFormatYVU422Planar_ER                                hipEglColorFormatYVU422Planar_ER
  // CU_EGL_COLOR_FORMAT_YVU420_PLANAR_ER = 0x29
#define cudaEglColorFormatYVU420Planar_ER                                hipEglColorFormatYVU420Planar_ER
  // CU_EGL_COLOR_FORMAT_YVU444_SEMIPLANAR_ER = 0x2A
#define cudaEglColorFormatYVU444SemiPlanar_ER                            hipEglColorFormatYVU444SemiPlanar_ER
  // CU_EGL_COLOR_FORMAT_YVU422_SEMIPLANAR_ER = 0x2B
#define cudaEglColorFormatYVU422SemiPlanar_ER                            hipEglColorFormatYVU422SemiPlanar_ER
  // CU_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_ER = 0x2C
#define cudaEglColorFormatYVU420SemiPlanar_ER                            hipEglColorFormatYVU420SemiPlanar_ER
  // CU_EGL_COLOR_FORMAT_BAYER_RGGB = 0x2D
#define cudaEglColorFormatBayerRGGB                                      hipEglColorFormatBayerRGGB
  // CU_EGL_COLOR_FORMAT_BAYER_BGGR = 0x2E
#define cudaEglColorFormatBayerBGGR                                      hipEglColorFormatBayerBGGR
  // CU_EGL_COLOR_FORMAT_BAYER_GRBG = 0x2F
#define cudaEglColorFormatBayerGRBG                                      hipEglColorFormatBayerGRBG
  // CU_EGL_COLOR_FORMAT_BAYER_GBRG = 0x30
#define cudaEglColorFormatBayerGBRG                                      hipEglColorFormatBayerGBRG
  // CU_EGL_COLOR_FORMAT_BAYER10_RGGB = 0x31
#define cudaEglColorFormatBayer10RGGB                                    hipEglColorFormatBayer10RGGB
  // CU_EGL_COLOR_FORMAT_BAYER10_BGGR = 0x32
#define cudaEglColorFormatBayer10BGGR                                    hipEglColorFormatBayer10BGGR
  // CU_EGL_COLOR_FORMAT_BAYER10_GRBG = 0x33
#define cudaEglColorFormatBayer10GRBG                                    hipEglColorFormatBayer10GRBG
  // CU_EGL_COLOR_FORMAT_BAYER10_GBRG = 0x34
#define cudaEglColorFormatBayer10GBRG                                    hipEglColorFormatBayer10GBRG
  // CU_EGL_COLOR_FORMAT_BAYER12_RGGB = 0x35
#define cudaEglColorFormatBayer12RGGB                                    hipEglColorFormatBayer12RGGB
  // CU_EGL_COLOR_FORMAT_BAYER12_BGGR = 0x36
#define cudaEglColorFormatBayer12BGGR                                    hipEglColorFormatBayer12BGGR
  // CU_EGL_COLOR_FORMAT_BAYER12_GRBG = 0x37
#define cudaEglColorFormatBayer12GRBG                                    hipEglColorFormatBayer12GRBG
  // CU_EGL_COLOR_FORMAT_BAYER12_GBRG = 0x38
#define cudaEglColorFormatBayer12GBRG                                    hipEglColorFormatBayer12GBRG
  // CU_EGL_COLOR_FORMAT_BAYER14_RGGB = 0x39
#define cudaEglColorFormatBayer14RGGB                                    hipEglColorFormatBayer14RGGB
  // CU_EGL_COLOR_FORMAT_BAYER14_BGGR = 0x3A
#define cudaEglColorFormatBayer14BGGR                                    hipEglColorFormatBayer14BGGR
  // CU_EGL_COLOR_FORMAT_BAYER14_GRBG = 0x3B
#define cudaEglColorFormatBayer14GRBG                                    hipEglColorFormatBayer14GRBG
  // CU_EGL_COLOR_FORMAT_BAYER14_GBRG = 0x3C
#define cudaEglColorFormatBayer14GBRG                                    hipEglColorFormatBayer14GBRG
  // CU_EGL_COLOR_FORMAT_BAYER20_RGGB = 0x3D
#define cudaEglColorFormatBayer20RGGB                                    hipEglColorFormatBayer20RGGB
  // CU_EGL_COLOR_FORMAT_BAYER20_BGGR = 0x3E
#define cudaEglColorFormatBayer20BGGR                                    hipEglColorFormatBayer20BGGR
  // CU_EGL_COLOR_FORMAT_BAYER20_GRBG = 0x3F
#define cudaEglColorFormatBayer20GRBG                                    hipEglColorFormatBayer20GRBG
  // CU_EGL_COLOR_FORMAT_BAYER20_GBRG = 0x40
#define cudaEglColorFormatBayer20GBRG                                    hipEglColorFormatBayer20GBRG
  // CU_EGL_COLOR_FORMAT_YVU444_PLANAR = 0x41
#define cudaEglColorFormatYVU444Planar                                   hipEglColorFormatYVU444Planar
  // CU_EGL_COLOR_FORMAT_YVU422_PLANAR = 0x42
#define cudaEglColorFormatYVU422Planar                                   hipEglColorFormatYVU422Planar
  // CU_EGL_COLOR_FORMAT_YVU420_PLANAR = 0x43
#define cudaEglColorFormatYVU420Planar                                   hipEglColorFormatYVU420Planar
  // CU_EGL_COLOR_FORMAT_BAYER_ISP_RGGB = 0x44
#define cudaEglColorFormatBayerIspRGGB                                   hipEglColorFormatBayerIspRGGB
  // CU_EGL_COLOR_FORMAT_BAYER_ISP_BGGR = 0x45
#define cudaEglColorFormatBayerIspBGGR                                   hipEglColorFormatBayerIspBGGR
  // CU_EGL_COLOR_FORMAT_BAYER_ISP_GRBG = 0x46
#define cudaEglColorFormatBayerIspGRBG                                   hipEglColorFormatBayerIspGRBG
  // CU_EGL_COLOR_FORMAT_BAYER_ISP_GBRG = 0x47
#define cudaEglColorFormatBayerIspGBRG                                   hipEglColorFormatBayerIspGBRG

  // CUeglFrameType
#define cudaEglFrameType                                                 hipEglFrameType
  // cudaEglFrameType enum values
  // CU_EGL_FRAME_TYPE_ARRAY
#define cudaEglFrameTypeArray                                            hipEglFrameTypeArray
  // CU_EGL_FRAME_TYPE_PITCH
#define cudaEglFrameTypePitch                                            hipEglFrameTypePitch

  // CUeglResourceLocationFlags
#define cudaEglResourceLocationFlags                                     hipEglResourceLocationFlags
  // cudaEglResourceLocationFlagss enum values
  // CU_EGL_RESOURCE_LOCATION_SYSMEM
#define cudaEglResourceLocationSysmem                                    hipEglResourceLocationSysmem
  // CU_EGL_RESOURCE_LOCATION_VIDMEM
#define cudaEglResourceLocationVidmem                                    hipEglResourceLocationVidmem

  // CUresult
#define cudaError                                                        hipError_t
#define cudaError_t                                                      hipError_t
  // cudaError enum values
  // CUDA_SUCCESS
#define cudaSuccess                                                      hipSuccess
  // CUDA_ERROR_INVALID_VALUE
#define cudaErrorInvalidValue                                            hipErrorInvalidValue
  // CUDA_ERROR_OUT_OF_MEMORY
#define cudaErrorMemoryAllocation                                        hipErrorOutOfMemory
  // CUDA_ERROR_NOT_INITIALIZED
#define cudaErrorInitializationError                                     hipErrorNotInitialized
  // CUDA_ERROR_DEINITIALIZED
#define cudaErrorCudartUnloading                                         hipErrorDeinitialized
  // CUDA_ERROR_PROFILER_DISABLED
#define cudaErrorProfilerDisabled                                        hipErrorProfilerDisabled
  // Deprecated since CUDA 5.0
  // CUDA_ERROR_PROFILER_NOT_INITIALIZED
#define cudaErrorProfilerNotInitialized                                  hipErrorProfilerNotInitialized
  // Deprecated since CUDA 5.0
  // CUDA_ERROR_PROFILER_ALREADY_STARTED
#define cudaErrorProfilerAlreadyStarted                                  hipErrorProfilerAlreadyStarted
  // Deprecated since CUDA 5.0
  // CUDA_ERROR_PROFILER_ALREADY_STOPPED
#define cudaErrorProfilerAlreadyStopped                                  hipErrorProfilerAlreadyStopped
  // no analogue
#define cudaErrorInvalidConfiguration                                    hipErrorInvalidConfiguration
  // no analogue
#define cudaErrorInvalidPitchValue                                       hipErrorInvalidPitchValue
  // no analogue
#define cudaErrorInvalidSymbol                                           hipErrorInvalidSymbol
  // Deprecated since CUDA 10.1
  // no analogue
#define cudaErrorInvalidHostPointer                                      hipErrorInvalidHostPointer
  // Deprecated since CUDA 10.1
  // no analogue
#define cudaErrorInvalidDevicePointer                                    hipErrorInvalidDevicePointer
  // no analogue
#define cudaErrorInvalidTexture                                          hipErrorInvalidTexture
  // no analogue
#define cudaErrorInvalidTextureBinding                                   hipErrorInvalidTextureBinding
  // no analogue
#define cudaErrorInvalidChannelDescriptor                                hipErrorInvalidChannelDescriptor
  // no analogue
#define cudaErrorInvalidMemcpyDirection                                  hipErrorInvalidMemcpyDirection
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorAddressOfConstant                                       hipErrorAddressOfConstant
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorTextureFetchFailed                                      hipErrorTextureFetchFailed
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorTextureNotBound                                         hipErrorTextureNotBound
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorSynchronizationError                                    hipErrorSynchronizationError
  // no analogue
#define cudaErrorInvalidFilterSetting                                    hipErrorInvalidFilterSetting
  // no analogue
#define cudaErrorInvalidNormSetting                                      hipErrorInvalidNormSetting
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorMixedDeviceExecution                                    hipErrorMixedDeviceExecution
  // Deprecated since CUDA 4.1
  // no analogue
#define cudaErrorNotYetImplemented                                       hipErrorNotYetImplemented
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorMemoryValueTooLarge                                     hipErrorMemoryValueTooLarge
  // CUDA_ERROR_STUB_LIBRARY
#define cudaErrorStubLibrary                                             hipErrorStubLibrary
  // no analogue
#define cudaErrorInsufficientDriver                                      hipErrorInsufficientDriver
  // no analogue
#define cudaErrorCallRequiresNewerDriver                                 hipErrorCallRequiresNewerDriver
  // no analogue
#define cudaErrorInvalidSurface                                          hipErrorInvalidSurface
  // no analogue
#define cudaErrorDuplicateVariableName                                   hipErrorDuplicateVariableName
  // no analogue
#define cudaErrorDuplicateTextureName                                    hipErrorDuplicateTextureName
  // no analogue
#define cudaErrorDuplicateSurfaceName                                    hipErrorDuplicateSurfaceName
  // no analogue
#define cudaErrorDevicesUnavailable                                      hipErrorDevicesUnavailable
  // no analogue
#define cudaErrorIncompatibleDriverContext                               hipErrorIncompatibleDriverContext
  // no analogue
#define cudaErrorMissingConfiguration                                    hipErrorMissingConfiguration
  // Deprecated since CUDA 3.1
  // no analogue
#define cudaErrorPriorLaunchFailure                                      hipErrorPriorLaunchFailure
  // no analogue
#define cudaErrorLaunchMaxDepthExceeded                                  hipErrorLaunchMaxDepthExceeded
  // no analogue
#define cudaErrorLaunchFileScopedTex                                     hipErrorLaunchFileScopedTex
  // no analogue
#define cudaErrorLaunchFileScopedSurf                                    hipErrorLaunchFileScopedSurf
  // no analogue
#define cudaErrorSyncDepthExceeded                                       hipErrorSyncDepthExceeded
  // no analogue
#define cudaErrorLaunchPendingCountExceeded                              hipErrorLaunchPendingCountExceeded
  // no analogue
#define cudaErrorInvalidDeviceFunction                                   hipErrorInvalidDeviceFunction
  // CUDA_ERROR_NO_DEVICE
#define cudaErrorNoDevice                                                hipErrorNoDevice
  // CUDA_ERROR_INVALID_DEVICE
#define cudaErrorInvalidDevice                                           hipErrorInvalidDevice
  // CUDA_ERROR_DEVICE_NOT_LICENSED
#define cudaErrorDeviceNotLicensed                                       hipErrorDeviceNotLicensed
  // no analogue
#define cudaErrorSoftwareValidityNotEstablished                          hipErrorSoftwareValidityNotEstablished
  // no analogue
#define cudaErrorStartupFailure                                          hipErrorStartupFailure
  // CUDA_ERROR_INVALID_IMAGE
#define cudaErrorInvalidKernelImage                                      hipErrorInvalidImage
  // CUDA_ERROR_INVALID_CONTEXT
#define cudaErrorDeviceUninitialized                                     hipErrorInvalidContext
  // CUDA_ERROR_MAP_FAILED
#define cudaErrorMapBufferObjectFailed                                   hipErrorMapFailed
  // CUDA_ERROR_UNMAP_FAILED
#define cudaErrorUnmapBufferObjectFailed                                 hipErrorUnmapFailed
  // CUDA_ERROR_ARRAY_IS_MAPPED
#define cudaErrorArrayIsMapped                                           hipErrorArrayIsMapped
  // CUDA_ERROR_ALREADY_MAPPED
#define cudaErrorAlreadyMapped                                           hipErrorAlreadyMapped
  // CUDA_ERROR_NO_BINARY_FOR_GPU
#define cudaErrorNoKernelImageForDevice                                  hipErrorNoBinaryForGpu
  // CUDA_ERROR_ALREADY_ACQUIRED
#define cudaErrorAlreadyAcquired                                         hipErrorAlreadyAcquired
  // CUDA_ERROR_NOT_MAPPED
#define cudaErrorNotMapped                                               hipErrorNotMapped
  // CUDA_ERROR_NOT_MAPPED_AS_ARRAY
#define cudaErrorNotMappedAsArray                                        hipErrorNotMappedAsArray
  // CUDA_ERROR_NOT_MAPPED_AS_POINTER
#define cudaErrorNotMappedAsPointer                                      hipErrorNotMappedAsPointer
  // CUDA_ERROR_ECC_UNCORRECTABLE
#define cudaErrorECCUncorrectable                                        hipErrorECCNotCorrectable
  // CUDA_ERROR_UNSUPPORTED_LIMIT
#define cudaErrorUnsupportedLimit                                        hipErrorUnsupportedLimit
  // CUDA_ERROR_CONTEXT_ALREADY_IN_USE
#define cudaErrorDeviceAlreadyInUse                                      hipErrorContextAlreadyInUse
  // CUDA_ERROR_PEER_ACCESS_UNSUPPORTED
#define cudaErrorPeerAccessUnsupported                                   hipErrorPeerAccessUnsupported
  // CUDA_ERROR_INVALID_PTX
#define cudaErrorInvalidPtx                                              hipErrorInvalidKernelFile
  // CUDA_ERROR_INVALID_GRAPHICS_CONTEXT
#define cudaErrorInvalidGraphicsContext                                  hipErrorInvalidGraphicsContext
  // CUDA_ERROR_NVLINK_UNCORRECTABLE
#define cudaErrorNvlinkUncorrectable                                     hipErrorNvlinkUncorrectable
  // CUDA_ERROR_JIT_COMPILER_NOT_FOUND
#define cudaErrorJitCompilerNotFound                                     hipErrorJitCompilerNotFound
  // CUDA_ERROR_UNSUPPORTED_PTX_VERSION
#define cudaErrorUnsupportedPtxVersion                                   hipErrorUnsupportedPtxVersion
  // CUDA_ERROR_JIT_COMPILATION_DISABLED
#define cudaErrorJitCompilationDisabled                                  hipErrorJitCompilationDisabled
  // CUDA_ERROR_UNSUPPORTED_EXEC_AFFINITY
#define cudaErrorUnsupportedExecAffinity                                 hipErrorUnsupportedExecAffinity
  // CUDA_ERROR_UNSUPPORTED_DEVSIDE_SYNC
#define cudaErrorUnsupportedDevSideSync                                  hipErrorUnsupportedDevSideSync
  // CUDA_ERROR_INVALID_SOURCE
#define cudaErrorInvalidSource                                           hipErrorInvalidSource
  // CUDA_ERROR_FILE_NOT_FOUND
#define cudaErrorFileNotFound                                            hipErrorFileNotFound
  // CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND
#define cudaErrorSharedObjectSymbolNotFound                              hipErrorSharedObjectSymbolNotFound
  // CUDA_ERROR_SHARED_OBJECT_INIT_FAILED
#define cudaErrorSharedObjectInitFailed                                  hipErrorSharedObjectInitFailed
  // CUDA_ERROR_OPERATING_SYSTEM
#define cudaErrorOperatingSystem                                         hipErrorOperatingSystem
  // CUDA_ERROR_INVALID_HANDLE
#define cudaErrorInvalidResourceHandle                                   hipErrorInvalidHandle
  // CUDA_ERROR_ILLEGAL_STATE
#define cudaErrorIllegalState                                            hipErrorIllegalState
  // CUDA_ERROR_LOSSY_QUERY
#define cudaErrorLossyQuery                                              hipErrorLossyQuery
  // CUDA_ERROR_NOT_FOUND
#define cudaErrorSymbolNotFound                                          hipErrorNotFound
  // CUDA_ERROR_NOT_READY
#define cudaErrorNotReady                                                hipErrorNotReady
 // CUDA_ERROR_ILLEGAL_ADDRESS
#define cudaErrorIllegalAddress                                          hipErrorIllegalAddress
  // CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES
#define cudaErrorLaunchOutOfResources                                    hipErrorLaunchOutOfResources
  // CUDA_ERROR_LAUNCH_TIMEOUT
#define cudaErrorLaunchTimeout                                           hipErrorLaunchTimeOut
  // CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING
#define cudaErrorLaunchIncompatibleTexturing                             hipErrorLaunchIncompatibleTexturing
  // CUDA_ERROR_PEER_ACCESS_ALREADY_ENABLED
#define cudaErrorPeerAccessAlreadyEnabled                                hipErrorPeerAccessAlreadyEnabled
  // CUDA_ERROR_PEER_ACCESS_NOT_ENABLED
#define cudaErrorPeerAccessNotEnabled                                    hipErrorPeerAccessNotEnabled
  // CUDA_ERROR_PRIMARY_CONTEXT_ACTIVE
#define cudaErrorSetOnActiveProcess                                      hipErrorSetOnActiveProcess
  // CUDA_ERROR_CONTEXT_IS_DESTROYED
#define cudaErrorContextIsDestroyed                                      hipErrorContextIsDestroyed
  // CUDA_ERROR_ASSERT
#define cudaErrorAssert                                                  hipErrorAssert
  // CUDA_ERROR_TOO_MANY_PEERS
#define cudaErrorTooManyPeers                                            hipErrorTooManyPeers
  // CUDA_ERROR_HOST_MEMORY_ALREADY_REGISTERED
#define cudaErrorHostMemoryAlreadyRegistered                             hipErrorHostMemoryAlreadyRegistered
  // CUDA_ERROR_HOST_MEMORY_NOT_REGISTERED
#define cudaErrorHostMemoryNotRegistered                                 hipErrorHostMemoryNotRegistered
  // CUDA_ERROR_HARDWARE_STACK_ERROR
#define cudaErrorHardwareStackError                                      hipErrorHardwareStackError
  // CUDA_ERROR_ILLEGAL_INSTRUCTION
#define cudaErrorIllegalInstruction                                      hipErrorIllegalInstruction
  // CUDA_ERROR_MISALIGNED_ADDRESS
#define cudaErrorMisalignedAddress                                       hipErrorMisalignedAddress
  // CUDA_ERROR_INVALID_ADDRESS_SPACE
#define cudaErrorInvalidAddressSpace                                     hipErrorInvalidAddressSpace
  // CUDA_ERROR_INVALID_PC
#define cudaErrorInvalidPc                                               hipErrorInvalidPc
  // CUDA_ERROR_LAUNCH_FAILED
#define cudaErrorLaunchFailure                                           hipErrorLaunchFailure
  // CUDA_ERROR_COOPERATIVE_LAUNCH_TOO_LARGE
#define cudaErrorCooperativeLaunchTooLarge                               hipErrorCooperativeLaunchTooLarge
  // CUDA_ERROR_NOT_PERMITTED
#define cudaErrorNotPermitted                                            hipErrorNotPermitted
  // CUDA_ERROR_NOT_SUPPORTED
#define cudaErrorNotSupported                                            hipErrorNotSupported
  // CUDA_ERROR_SYSTEM_NOT_READY
#define cudaErrorSystemNotReady                                          hipErrorSystemNotReady
  // CUDA_ERROR_SYSTEM_DRIVER_MISMATCH
#define cudaErrorSystemDriverMismatch                                    hipErrorSystemDriverMismatch
  // CUDA_ERROR_COMPAT_NOT_SUPPORTED_ON_DEVICE
#define cudaErrorCompatNotSupportedOnDevice                              hipErrorCompatNotSupportedOnDevice
  // CUDA_ERROR_MPS_CONNECTION_FAILED
#define cudaErrorMpsConnectionFailed                                     hipErrorMpsConnectionFailed
  // CUDA_ERROR_MPS_RPC_FAILURE
#define cudaErrorMpsRpcFailure                                           hipErrorMpsRpcFailed
  // CUDA_ERROR_MPS_SERVER_NOT_READY
#define cudaErrorMpsServerNotReady                                       hipErrorMpsServerNotReady
  // CUDA_ERROR_MPS_MAX_CLIENTS_REACHED
#define cudaErrorMpsMaxClientsReached                                    hipErrorMpsMaxClientsReached
  // CUDA_ERROR_MPS_MAX_CONNECTIONS_REACHED
#define cudaErrorMpsMaxConnectionsReached                                hipErrorMpsMaxConnectionsReached
  // CUDA_ERROR_MPS_CLIENT_TERMINATED
#define cudaErrorMpsClientTerminated                                     hipErrorMpsClientTerminated
  // CUDA_ERROR_CDP_NOT_SUPPORTED
#define cudaErrorCdpNotSupported                                         hipErrorCdpNotUnsupported
  // CUDA_ERROR_CDP_VERSION_MISMATCH
#define cudaErrorCdpVersionMismatch                                      hipErrorCdpVersionMismatch
  // CUDA_ERROR_STREAM_CAPTURE_UNSUPPORTED
#define cudaErrorStreamCaptureUnsupported                                hipErrorStreamCaptureUnsupported
  // CUDA_ERROR_STREAM_CAPTURE_INVALIDATED
#define cudaErrorStreamCaptureInvalidated                                hipErrorStreamCaptureInvalidated
  // CUDA_ERROR_STREAM_CAPTURE_MERGE
#define cudaErrorStreamCaptureMerge                                      hipErrorStreamCaptureMerge
  // CUDA_ERROR_STREAM_CAPTURE_UNMATCHED
#define cudaErrorStreamCaptureUnmatched                                  hipErrorStreamCaptureUnmatched
  // CUDA_ERROR_STREAM_CAPTURE_UNJOINED
#define cudaErrorStreamCaptureUnjoined                                   hipErrorStreamCaptureUnjoined
  // CUDA_ERROR_STREAM_CAPTURE_ISOLATION
#define cudaErrorStreamCaptureIsolation                                  hipErrorStreamCaptureIsolation
  // CUDA_ERROR_STREAM_CAPTURE_IMPLICIT
#define cudaErrorStreamCaptureImplicit                                   hipErrorStreamCaptureImplicit
  // CUDA_ERROR_CAPTURED_EVENT
#define cudaErrorCapturedEvent                                           hipErrorCapturedEvent
  // CUDA_ERROR_STREAM_CAPTURE_WRONG_THREAD
#define cudaErrorStreamCaptureWrongThread                                hipErrorStreamCaptureWrongThread
  // CUDA_ERROR_TIMEOUT
#define cudaErrorTimeout                                                 hipErrorTimeout
  // CUDA_ERROR_GRAPH_EXEC_UPDATE_FAILURE
#define cudaErrorGraphExecUpdateFailure                                  hipErrorGraphExecUpdateFailure
  // CUDA_ERROR_EXTERNAL_DEVICE
#define cudaErrorExternalDevice                                          hipErrorExternalDevice
  // CUDA_ERROR_INVALID_CLUSTER_SIZE
#define cudaErrorInvalidClusterSize                                      hipErrorInvalidClusterSize
  // CUDA_ERROR_UNKNOWN
#define cudaErrorUnknown                                                 hipErrorUnknown
  // Deprecated since CUDA 4.1
#define cudaErrorApiFailureBase                                          hipErrorApiFailureBase

  // CUexternalMemoryHandleType
#define cudaExternalMemoryHandleType                                     hipExternalMemoryHandleType
  // cudaExternalMemoryHandleType enum values
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD
#define cudaExternalMemoryHandleTypeOpaqueFd                             hipExternalMemoryHandleTypeOpaqueFd
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32
#define cudaExternalMemoryHandleTypeOpaqueWin32                          hipExternalMemoryHandleTypeOpaqueWin32
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT
#define cudaExternalMemoryHandleTypeOpaqueWin32Kmt                       hipExternalMemoryHandleTypeOpaqueWin32Kmt
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP
#define cudaExternalMemoryHandleTypeD3D12Heap                            hipExternalMemoryHandleTypeD3D12Heap
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE
#define cudaExternalMemoryHandleTypeD3D12Resource                        hipExternalMemoryHandleTypeD3D12Resource
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_RESOURCE
#define cudaExternalMemoryHandleTypeD3D11Resource                        hipExternalMemoryHandleTypeD3D11Resource
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_RESOURCE_KMT
#define cudaExternalMemoryHandleTypeD3D11ResourceKmt                     hipExternalMemoryHandleTypeD3D11ResourceKmt
  // CU_EXTERNAL_MEMORY_HANDLE_TYPE_NVSCIBUF
#define cudaExternalMemoryHandleTypeNvSciBuf                             hipExternalMemoryHandleTypeNvSciBuf

  // CUexternalSemaphoreHandleType
#define cudaExternalSemaphoreHandleType                                  hipExternalSemaphoreHandleType
  // cudaExternalSemaphoreHandleType enum values
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD
#define cudaExternalSemaphoreHandleTypeOpaqueFd                          hipExternalSemaphoreHandleTypeOpaqueFd
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32
#define cudaExternalSemaphoreHandleTypeOpaqueWin32                       hipExternalSemaphoreHandleTypeOpaqueWin32
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT
#define cudaExternalSemaphoreHandleTypeOpaqueWin32Kmt                    hipExternalSemaphoreHandleTypeOpaqueWin32Kmt
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE
#define cudaExternalSemaphoreHandleTypeD3D12Fence                        hipExternalSemaphoreHandleTypeD3D12Fence
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_FENCE
#define cudaExternalSemaphoreHandleTypeD3D11Fence                        hipExternalSemaphoreHandleTypeD3D11Fence
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_NVSCISYNC
#define cudaExternalSemaphoreHandleTypeNvSciSync                         hipExternalSemaphoreHandleTypeNvSciSync
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_KEYED_MUTEX
#define cudaExternalSemaphoreHandleTypeKeyedMutex                        hipExternalSemaphoreHandleTypeKeyedMutex
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_KEYED_MUTEX_KMT
#define cudaExternalSemaphoreHandleTypeKeyedMutexKmt                     hipExternalSemaphoreHandleTypeKeyedMutexKmt
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_TIMELINE_SEMAPHORE_FD
#define cudaExternalSemaphoreHandleTypeTimelineSemaphoreFd               hipExternalSemaphoreHandleTypeTimelineSemaphoreFd
  // CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_TIMELINE_SEMAPHORE_WIN32
#define cudaExternalSemaphoreHandleTypeTimelineSemaphoreWin32            hipExternalSemaphoreHandleTypeTimelineSemaphoreWin32

  // CUfunction_attribute
  // NOTE: only last, starting from 8, values are presented and are equal to Driver's ones
#define cudaFuncAttribute                                                hipFuncAttribute
  // cudaFuncAttribute enum values
  // CU_FUNC_ATTRIBUTE_MAX_DYNAMIC_SHARED_SIZE_BYTES
#define cudaFuncAttributeMaxDynamicSharedMemorySize                      hipFuncAttributeMaxDynamicSharedMemorySize
  // CU_FUNC_ATTRIBUTE_PREFERRED_SHARED_MEMORY_CARVEOUT
#define cudaFuncAttributePreferredSharedMemoryCarveout                   hipFuncAttributePreferredSharedMemoryCarveout
  // CU_FUNC_ATTRIBUTE_CLUSTER_SIZE_MUST_BE_SET
#define cudaFuncAttributeClusterDimMustBeSet                             hipFuncAttributeClusterDimMustBeSet
  // CU_FUNC_ATTRIBUTE_REQUIRED_CLUSTER_WIDTH
#define cudaFuncAttributeRequiredClusterWidth                            hipFuncAttributeRequiredClusterWidth
  // CU_FUNC_ATTRIBUTE_REQUIRED_CLUSTER_HEIGHT
#define cudaFuncAttributeRequiredClusterHeight                           hipFuncAttributeRequiredClusterHeight
  // CU_FUNC_ATTRIBUTE_REQUIRED_CLUSTER_DEPTH
#define cudaFuncAttributeRequiredClusterDepth                            hipFuncAttributeRequiredClusterDepth
  // CU_FUNC_ATTRIBUTE_NON_PORTABLE_CLUSTER_SIZE_ALLOWED
#define cudaFuncAttributeNonPortableClusterSizeAllowed                   hipFuncAttributeNonPortableClusterSizeAllowed
  // CU_FUNC_ATTRIBUTE_CLUSTER_SCHEDULING_POLICY_PREFERENCE
#define cudaFuncAttributeClusterSchedulingPolicyPreference               hipFuncAttributeClusterSchedulingPolicyPreference
  // CU_FUNC_ATTRIBUTE_MAX
#define cudaFuncAttributeMax                                             hipFuncAttributeMax

  // CUfunc_cache
#define cudaFuncCache                                                    hipFuncCache_t
  // cudaFuncCache enum values
  // CU_FUNC_CACHE_PREFER_NONE = 0x00
#define cudaFuncCachePreferNone                                          hipFuncCachePreferNone
  // CU_FUNC_CACHE_PREFER_SHARED = 0x01
#define cudaFuncCachePreferShared                                        hipFuncCachePreferShared
  // CU_FUNC_CACHE_PREFER_L1 = 0x02
#define cudaFuncCachePreferL1                                            hipFuncCachePreferL1
  // CU_FUNC_CACHE_PREFER_EQUAL = 0x03
#define cudaFuncCachePreferEqual                                         hipFuncCachePreferEqual

  // CUarray_cubemap_face
#define cudaGraphicsCubeFace                                             hipGraphicsCubeFace
  // cudaGraphicsCubeFace enum values
  // CU_CUBEMAP_FACE_POSITIVE_X
#define cudaGraphicsCubeFacePositiveX                                    hipGraphicsCubeFacePositiveX
  // CU_CUBEMAP_FACE_NEGATIVE_X
#define cudaGraphicsCubeFaceNegativeX                                    hipGraphicsCubeFaceNegativeX
  // CU_CUBEMAP_FACE_POSITIVE_Y
#define cudaGraphicsCubeFacePositiveY                                    hipGraphicsCubeFacePositiveY
  // CU_CUBEMAP_FACE_NEGATIVE_Y
#define cudaGraphicsCubeFaceNegativeY                                    hipGraphicsCubeFaceNegativeY
  // CU_CUBEMAP_FACE_POSITIVE_Z
#define cudaGraphicsCubeFacePositiveZ                                    hipGraphicsCubeFacePositiveZ
  // CU_CUBEMAP_FACE_NEGATIVE_Z
#define cudaGraphicsCubeFaceNegativeZ                                    hipGraphicsCubeFaceNegativeZ

  // CUgraphicsMapResourceFlags
#define cudaGraphicsMapFlags                                             hipGraphicsMapFlags
  // cudaGraphicsMapFlags enum values
  // CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE = 0x00
#define cudaGraphicsMapFlagsNone                                         hipGraphicsMapFlagsNone
  // CU_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY = 0x01
#define cudaGraphicsMapFlagsReadOnly                                     hipGraphicsMapFlagsReadOnly
  // CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD = 0x02
#define cudaGraphicsMapFlagsWriteDiscard                                 hipGraphicsMapFlagsWriteDiscard

  // CUgraphicsRegisterFlags
#define cudaGraphicsRegisterFlags                                        hipGraphicsRegisterFlags
  // cudaGraphicsRegisterFlags enum values
  // CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE = 0x00
#define cudaGraphicsRegisterFlagsNone                                    hipGraphicsRegisterFlagsNone
  // CU_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY = 0x01
#define cudaGraphicsRegisterFlagsReadOnly                                hipGraphicsRegisterFlagsReadOnly
  // CU_GRAPHICS_REGISTER_FLAGS_WRITE_DISCARD = 0x02
#define cudaGraphicsRegisterFlagsWriteDiscard                            hipGraphicsRegisterFlagsWriteDiscard
  // CU_GRAPHICS_REGISTER_FLAGS_SURFACE_LDST = 0x04
#define cudaGraphicsRegisterFlagsSurfaceLoadStore                        hipGraphicsRegisterFlagsSurfaceLoadStore
  // CU_GRAPHICS_REGISTER_FLAGS_TEXTURE_GATHER = 0x08
#define cudaGraphicsRegisterFlagsTextureGather                           hipGraphicsRegisterFlagsTextureGather

  // CUgraphNodeType
#define cudaGraphNodeType                                                hipGraphNodeType
  // cudaGraphNodeType enum values
  // CU_GRAPH_NODE_TYPE_KERNEL = 0
#define cudaGraphNodeTypeKernel                                          hipGraphNodeTypeKernel
  // CU_GRAPH_NODE_TYPE_MEMCPY = 1
#define cudaGraphNodeTypeMemcpy                                          hipGraphNodeTypeMemcpy
  // CU_GRAPH_NODE_TYPE_MEMSET = 2
#define cudaGraphNodeTypeMemset                                          hipGraphNodeTypeMemset
  // CU_GRAPH_NODE_TYPE_HOST = 3
#define cudaGraphNodeTypeHost                                            hipGraphNodeTypeHost
  // CU_GRAPH_NODE_TYPE_GRAPH = 4
#define cudaGraphNodeTypeGraph                                           hipGraphNodeTypeGraph
  // CU_GRAPH_NODE_TYPE_EMPTY = 5
#define cudaGraphNodeTypeEmpty                                           hipGraphNodeTypeEmpty
  // CU_GRAPH_NODE_TYPE_WAIT_EVENT = 6
#define cudaGraphNodeTypeWaitEvent                                       hipGraphNodeTypeWaitEvent
  // CU_GRAPH_NODE_TYPE_EVENT_RECORD = 7
#define cudaGraphNodeTypeEventRecord                                     hipGraphNodeTypeEventRecord
  // CU_GRAPH_NODE_TYPE_EXT_SEMAS_SIGNAL = 8
#define cudaGraphNodeTypeExtSemaphoreSignal                              hipGraphNodeTypeExtSemaphoreSignal
  // CU_GRAPH_NODE_TYPE_EXT_SEMAS_WAIT = 9
#define cudaGraphNodeTypeExtSemaphoreWait                                hipGraphNodeTypeExtSemaphoreWait
  // CU_GRAPH_NODE_TYPE_MEM_ALLOC = 10
#define cudaGraphNodeTypeMemAlloc                                        hipGraphNodeTypeMemAlloc
  // CU_GRAPH_NODE_TYPE_MEM_FREE = 11
#define cudaGraphNodeTypeMemFree                                         hipGraphNodeTypeMemFree
  // CU_GRAPH_NODE_TYPE_CONDITIONAL = 13
#define cudaGraphNodeTypeConditional                                     hipGraphNodeTypeConditional
  // CU_GRAPH_NODE_TYPE_COUNT
#define cudaGraphNodeTypeCount                                           hipGraphNodeTypeCount

  // CUgraphExecUpdateResult
#define cudaGraphExecUpdateResult                                        hipGraphExecUpdateResult
  // cudaGraphExecUpdateResult enum values
  // CU_GRAPH_EXEC_UPDATE_SUCCESS
#define cudaGraphExecUpdateSuccess                                       hipGraphExecUpdateSuccess
  // CU_GRAPH_EXEC_UPDATE_ERROR
#define cudaGraphExecUpdateError                                         hipGraphExecUpdateError
  // CU_GRAPH_EXEC_UPDATE_ERROR_TOPOLOGY_CHANGED
#define cudaGraphExecUpdateErrorTopologyChanged                          hipGraphExecUpdateErrorTopologyChanged
  // CU_GRAPH_EXEC_UPDATE_ERROR_NODE_TYPE_CHANGED
#define cudaGraphExecUpdateErrorNodeTypeChanged                          hipGraphExecUpdateErrorNodeTypeChanged
  // CU_GRAPH_EXEC_UPDATE_ERROR_FUNCTION_CHANGED
#define cudaGraphExecUpdateErrorFunctionChanged                          hipGraphExecUpdateErrorFunctionChanged
  // CU_GRAPH_EXEC_UPDATE_ERROR_PARAMETERS_CHANGED
#define cudaGraphExecUpdateErrorParametersChanged                        hipGraphExecUpdateErrorParametersChanged
  // CU_GRAPH_EXEC_UPDATE_ERROR_NOT_SUPPORTED
#define cudaGraphExecUpdateErrorNotSupported                             hipGraphExecUpdateErrorNotSupported
  // CU_GRAPH_EXEC_UPDATE_ERROR_UNSUPPORTED_FUNCTION_CHANGE
#define cudaGraphExecUpdateErrorUnsupportedFunctionChange                hipGraphExecUpdateErrorUnsupportedFunctionChange
  // CU_GRAPH_EXEC_UPDATE_ERROR_ATTRIBUTES_CHANGED
#define cudaGraphExecUpdateErrorAttributesChanged                        hipGraphExecUpdateErrorAttributesChanged

  // CUlimit
#define cudaLimit                                                        hipLimit_t
  // cudaLimit enum values
  // CU_LIMIT_STACK_SIZE
#define cudaLimitStackSize                                               hipLimitStackSize
  // CU_LIMIT_PRINTF_FIFO_SIZE
#define cudaLimitPrintfFifoSize                                          hipLimitPrintfFifoSize
  // CU_LIMIT_MALLOC_HEAP_SIZE
#define cudaLimitMallocHeapSize                                          hipLimitMallocHeapSize
  // CU_LIMIT_DEV_RUNTIME_SYNC_DEPTH
#define cudaLimitDevRuntimeSyncDepth                                     hipLimitDevRuntimeSyncDepth
  // CU_LIMIT_DEV_RUNTIME_PENDING_LAUNCH_COUNT
#define cudaLimitDevRuntimePendingLaunchCount                            hipLimitDevRuntimePendingLaunchCount
  // CU_LIMIT_MAX_L2_FETCH_GRANULARITY
#define cudaLimitMaxL2FetchGranularity                                   hipLimitMaxL2FetchGranularity
  // CU_LIMIT_PERSISTING_L2_CACHE_SIZE
#define cudaLimitPersistingL2CacheSize                                   hipLimitPersistingL2CacheSize

  // no analogue
#define cudaMemcpyKind                                                   hipMemcpyKind
  // cudaMemcpyKind enum values
#define cudaMemcpyHostToHost                                             hipMemcpyHostToHost
#define cudaMemcpyHostToDevice                                           hipMemcpyHostToDevice
#define cudaMemcpyDeviceToHost                                           hipMemcpyDeviceToHost
#define cudaMemcpyDeviceToDevice                                         hipMemcpyDeviceToDevice
#define cudaMemcpyDefault                                                hipMemcpyDefault

  // CUmem_advise
#define cudaMemoryAdvise                                                 hipMemoryAdvise
  // cudaMemoryAdvise enum values
  // CU_MEM_ADVISE_SET_READ_MOSTLY
#define cudaMemAdviseSetReadMostly                                       hipMemAdviseSetReadMostly
  // CU_MEM_ADVISE_UNSET_READ_MOSTLY
#define cudaMemAdviseUnsetReadMostly                                     hipMemAdviseUnsetReadMostly
  // CU_MEM_ADVISE_SET_PREFERRED_LOCATION
#define cudaMemAdviseSetPreferredLocation                                hipMemAdviseSetPreferredLocation
  // CU_MEM_ADVISE_UNSET_PREFERRED_LOCATION
#define cudaMemAdviseUnsetPreferredLocation                              hipMemAdviseUnsetPreferredLocation
  // CU_MEM_ADVISE_SET_ACCESSED_BY
#define cudaMemAdviseSetAccessedBy                                       hipMemAdviseSetAccessedBy
  // CU_MEM_ADVISE_UNSET_ACCESSED_BY
#define cudaMemAdviseUnsetAccessedBy                                     hipMemAdviseUnsetAccessedBy

  // no analogue
  // NOTE: CUmemorytype is partial analogue
#define cudaMemoryType                                                   hipMemoryType
  // cudaMemoryType enum values
#define cudaMemoryTypeUnregistered                                       hipMemoryTypeUnregistered
#define cudaMemoryTypeHost                                               hipMemoryTypeHost
#define cudaMemoryTypeDevice                                             hipMemoryTypeDevice
#define cudaMemoryTypeManaged                                            hipMemoryTypeManaged

  // CUmem_range_attribute
#define cudaMemRangeAttribute                                            hipMemRangeAttribute
  // cudaMemRangeAttribute enum values
  // CU_MEM_RANGE_ATTRIBUTE_READ_MOSTLY
#define cudaMemRangeAttributeReadMostly                                  hipMemRangeAttributeReadMostly
  // CU_MEM_RANGE_ATTRIBUTE_PREFERRED_LOCATION
#define cudaMemRangeAttributePreferredLocation                           hipMemRangeAttributePreferredLocation
  // CU_MEM_RANGE_ATTRIBUTE_ACCESSED_BY
#define cudaMemRangeAttributeAccessedBy                                  hipMemRangeAttributeAccessedBy
  // CU_MEM_RANGE_ATTRIBUTE_LAST_PREFETCH_LOCATION
#define cudaMemRangeAttributeLastPrefetchLocation                        hipMemRangeAttributeLastPrefetchLocation
  // CU_MEM_RANGE_ATTRIBUTE_PREFERRED_LOCATION_TYPE
#define cudaMemRangeAttributePreferredLocationType                       hipMemRangeAttributePreferredLocationType
  // CU_MEM_RANGE_ATTRIBUTE_PREFERRED_LOCATION_ID
#define cudaMemRangeAttributePreferredLocationId                         hipMemRangeAttributePreferredLocationId
  // CU_MEM_RANGE_ATTRIBUTE_LAST_PREFETCH_LOCATION_TYPE
#define cudaMemRangeAttributeLastPrefetchLocationType                    hipMemRangeAttributeLastPrefetchLocationType
  // CU_MEM_RANGE_ATTRIBUTE_LAST_PREFETCH_LOCATION_ID
#define cudaMemRangeAttributeLastPrefetchLocationId                      hipMemRangeAttributeLastPrefetchLocationId

  // no analogue
#define cudaOutputMode                                                   hipOutputMode
#define cudaOutputMode_t                                                 hipOutputMode
  // cudaOutputMode enum values
#define cudaKeyValuePair                                                 hipKeyValuePair
#define cudaCSV                                                          hipCSV

  // CUresourcetype
#define cudaResourceType                                                 hipResourceType
  // cudaResourceType enum values
  // CU_RESOURCE_TYPE_ARRAY
#define cudaResourceTypeArray                                            hipResourceTypeArray
  // CU_RESOURCE_TYPE_MIPMAPPED_ARRAY
#define cudaResourceTypeMipmappedArray                                   hipResourceTypeMipmappedArray
  // CU_RESOURCE_TYPE_LINEAR
#define cudaResourceTypeLinear                                           hipResourceTypeLinear
  // CU_RESOURCE_TYPE_PITCH2D
#define cudaResourceTypePitch2D                                          hipResourceTypePitch2D

  // CUresourceViewFormat
#define cudaResourceViewFormat                                           hipResourceViewFormat
  // enum cudaResourceViewFormat
  // CU_RES_VIEW_FORMAT_NONE
#define cudaResViewFormatNone                                            hipResViewFormatNone
  // CU_RES_VIEW_FORMAT_UINT_1X8
#define cudaResViewFormatUnsignedChar1                                   hipResViewFormatUnsignedChar1
  // CU_RES_VIEW_FORMAT_UINT_2X8
#define cudaResViewFormatUnsignedChar2                                   hipResViewFormatUnsignedChar2
  // CU_RES_VIEW_FORMAT_UINT_4X8
#define cudaResViewFormatUnsignedChar4                                   hipResViewFormatUnsignedChar4
  // CU_RES_VIEW_FORMAT_SINT_1X8
#define cudaResViewFormatSignedChar1                                     hipResViewFormatSignedChar1
  // CU_RES_VIEW_FORMAT_SINT_2X8
#define cudaResViewFormatSignedChar2                                     hipResViewFormatSignedChar2
  // CU_RES_VIEW_FORMAT_SINT_4X8
#define cudaResViewFormatSignedChar4                                     hipResViewFormatSignedChar4
  // CU_RES_VIEW_FORMAT_UINT_1X16
#define cudaResViewFormatUnsignedShort1                                  hipResViewFormatUnsignedShort1
  // CU_RES_VIEW_FORMAT_UINT_2X16
#define cudaResViewFormatUnsignedShort2                                  hipResViewFormatUnsignedShort2
  // CU_RES_VIEW_FORMAT_UINT_4X16
#define cudaResViewFormatUnsignedShort4                                  hipResViewFormatUnsignedShort4
  // CU_RES_VIEW_FORMAT_SINT_1X16
#define cudaResViewFormatSignedShort1                                    hipResViewFormatSignedShort1
  // CU_RES_VIEW_FORMAT_SINT_2X16
#define cudaResViewFormatSignedShort2                                    hipResViewFormatSignedShort2
  // CU_RES_VIEW_FORMAT_SINT_4X16
#define cudaResViewFormatSignedShort4                                    hipResViewFormatSignedShort4
  // CU_RES_VIEW_FORMAT_UINT_1X32
#define cudaResViewFormatUnsignedInt1                                    hipResViewFormatUnsignedInt1
  // CU_RES_VIEW_FORMAT_UINT_2X32
#define cudaResViewFormatUnsignedInt2                                    hipResViewFormatUnsignedInt2
  // CU_RES_VIEW_FORMAT_UINT_4X32
#define cudaResViewFormatUnsignedInt4                                    hipResViewFormatUnsignedInt4
  // CU_RES_VIEW_FORMAT_SINT_1X32
#define cudaResViewFormatSignedInt1                                      hipResViewFormatSignedInt1
  // CU_RES_VIEW_FORMAT_SINT_2X32
#define cudaResViewFormatSignedInt2                                      hipResViewFormatSignedInt2
  // CU_RES_VIEW_FORMAT_SINT_4X32
#define cudaResViewFormatSignedInt4                                      hipResViewFormatSignedInt4
  // CU_RES_VIEW_FORMAT_FLOAT_1X16
#define cudaResViewFormatHalf1                                           hipResViewFormatHalf1
  // CU_RES_VIEW_FORMAT_FLOAT_2X16
#define cudaResViewFormatHalf2                                           hipResViewFormatHalf2
  // CU_RES_VIEW_FORMAT_FLOAT_4X16
#define cudaResViewFormatHalf4                                           hipResViewFormatHalf4
  // CU_RES_VIEW_FORMAT_FLOAT_1X32
#define cudaResViewFormatFloat1                                          hipResViewFormatFloat1
  // CU_RES_VIEW_FORMAT_FLOAT_2X32
#define cudaResViewFormatFloat2                                          hipResViewFormatFloat2
  // CU_RES_VIEW_FORMAT_FLOAT_4X32
#define cudaResViewFormatFloat4                                          hipResViewFormatFloat4
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC1
#define cudaResViewFormatUnsignedBlockCompressed1                        hipResViewFormatUnsignedBlockCompressed1
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC2
#define cudaResViewFormatUnsignedBlockCompressed2                        hipResViewFormatUnsignedBlockCompressed2
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC3
#define cudaResViewFormatUnsignedBlockCompressed3                        hipResViewFormatUnsignedBlockCompressed3
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC4
#define cudaResViewFormatUnsignedBlockCompressed4                        hipResViewFormatUnsignedBlockCompressed4
  // CU_RES_VIEW_FORMAT_SIGNED_BC4
#define cudaResViewFormatSignedBlockCompressed4                          hipResViewFormatSignedBlockCompressed4
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC5
#define cudaResViewFormatUnsignedBlockCompressed5                        hipResViewFormatUnsignedBlockCompressed5
  // CU_RES_VIEW_FORMAT_SIGNED_BC5
#define cudaResViewFormatSignedBlockCompressed5                          hipResViewFormatSignedBlockCompressed5
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC6H
#define cudaResViewFormatUnsignedBlockCompressed6H                       hipResViewFormatUnsignedBlockCompressed6H
  // CU_RES_VIEW_FORMAT_SIGNED_BC6H
#define cudaResViewFormatSignedBlockCompressed6H                         hipResViewFormatSignedBlockCompressed6H
  // CU_RES_VIEW_FORMAT_UNSIGNED_BC7
#define cudaResViewFormatUnsignedBlockCompressed7                        hipResViewFormatUnsignedBlockCompressed7

  // CUshared_carveout
#define cudaSharedCarveout                                               hipSharedCarveout
  // cudaSharedCarveout enum values
  // CU_SHAREDMEM_CARVEOUT_DEFAULT
#define cudaSharedmemCarveoutDefault                                     hipSharedmemCarveoutDefault
  // CU_SHAREDMEM_CARVEOUT_MAX_SHARED
#define cudaSharedmemCarveoutMaxShared                                   hipSharedmemCarveoutMaxShared
  // CU_SHAREDMEM_CARVEOUT_MAX_L1
#define cudaSharedmemCarveoutMaxL1                                       hipSharedmemCarveoutMaxL1

  // CUsharedconfig
#define cudaSharedMemConfig                                              hipSharedMemConfig
  // cudaSharedMemConfig enum values
  // CU_SHARED_MEM_CONFIG_DEFAULT_BANK_SIZE = 0x00
#define cudaSharedMemBankSizeDefault                                     hipSharedMemBankSizeDefault
  // CU_SHARED_MEM_CONFIG_FOUR_BYTE_BANK_SIZE = 0x01
#define cudaSharedMemBankSizeFourByte                                    hipSharedMemBankSizeFourByte
  // CU_SHARED_MEM_CONFIG_EIGHT_BYTE_BANK_SIZE = 0x02
#define cudaSharedMemBankSizeEightByte                                   hipSharedMemBankSizeEightByte

  // CUstreamCaptureStatus
#define cudaStreamCaptureStatus                                          hipStreamCaptureStatus
  // cudaStreamCaptureStatus enum values
  // CU_STREAM_CAPTURE_STATUS_NONE
#define cudaStreamCaptureStatusNone                                      hipStreamCaptureStatusNone
  // CU_STREAM_CAPTURE_STATUS_ACTIVE
#define cudaStreamCaptureStatusActive                                    hipStreamCaptureStatusActive
  // CU_STREAM_CAPTURE_STATUS_INVALIDATED
#define cudaStreamCaptureStatusInvalidated                               hipStreamCaptureStatusInvalidated

  // CUstreamCaptureMode
#define cudaStreamCaptureMode                                            hipStreamCaptureMode
  // cudaStreamCaptureMode enum values
  // CU_STREAM_CAPTURE_MODE_GLOBAL
#define cudaStreamCaptureModeGlobal                                      hipStreamCaptureModeGlobal
  // CU_STREAM_CAPTURE_MODE_THREAD_LOCAL
#define cudaStreamCaptureModeThreadLocal                                 hipStreamCaptureModeThreadLocal
  // CU_STREAM_CAPTURE_MODE_RELAXED
#define cudaStreamCaptureModeRelaxed                                     hipStreamCaptureModeRelaxed

  // no analogue
#define cudaSurfaceBoundaryMode                                          hipSurfaceBoundaryMode
  // cudaSurfaceBoundaryMode enum values
#define cudaBoundaryModeZero                                             hipBoundaryModeZero
#define cudaBoundaryModeClamp                                            hipBoundaryModeClamp
#define cudaBoundaryModeTrap                                             hipBoundaryModeTrap

  // no analogue
#define cudaSurfaceFormatMode                                            hipSurfaceFormatMode
  // enum cudaSurfaceFormatMode
#define cudaFormatModeForced                                             hipFormatModeForced
#define cudaFormatModeAuto                                               hipFormatModeAuto

  // CUaddress_mode_enum
#define cudaTextureAddressMode                                           hipTextureAddressMode
  // cudaTextureAddressMode enum values
  // CU_TR_ADDRESS_MODE_WRAP
#define cudaAddressModeWrap                                              hipAddressModeWrap
  // CU_TR_ADDRESS_MODE_CLAMP
#define cudaAddressModeClamp                                             hipAddressModeClamp
  // CU_TR_ADDRESS_MODE_MIRROR
#define cudaAddressModeMirror                                            hipAddressModeMirror
  // CU_TR_ADDRESS_MODE_BORDER
#define cudaAddressModeBorder                                            hipAddressModeBorder

  // CUfilter_mode
#define cudaTextureFilterMode                                            hipTextureFilterMode
  // cudaTextureFilterMode enum values
  // CU_TR_FILTER_MODE_POINT
#define cudaFilterModePoint                                              hipFilterModePoint
  // CU_TR_FILTER_MODE_LINEAR
#define cudaFilterModeLinear                                             hipFilterModeLinear

  // no analogue
#define cudaTextureReadMode                                              hipTextureReadMode
  // cudaTextureReadMode enum values
#define cudaReadModeElementType                                          hipReadModeElementType
#define cudaReadModeNormalizedFloat                                      hipReadModeNormalizedFloat

  // CUGLDeviceList
#define cudaGLDeviceList                                                 hipGLDeviceList
  // cudaGLDeviceList enum values
  // CU_GL_DEVICE_LIST_ALL = 0x01
#define cudaGLDeviceListAll                                              hipGLDeviceListAll
  // CU_GL_DEVICE_LIST_CURRENT_FRAME = 0x02
#define cudaGLDeviceListCurrentFrame                                     hipGLDeviceListCurrentFrame
  // CU_GL_DEVICE_LIST_NEXT_FRAME = 0x03
#define cudaGLDeviceListNextFrame                                        hipGLDeviceListNextFrame

  // CUGLmap_flags
#define cudaGLMapFlags                                                   hipGLMapFlags
  // cudaGLMapFlags enum values
  // CU_GL_MAP_RESOURCE_FLAGS_NONE = 0x00
#define cudaGLMapFlagsNone                                               hipGLMapFlagsNone
  // CU_GL_MAP_RESOURCE_FLAGS_READ_ONLY = 0x01
#define cudaGLMapFlagsReadOnly                                           hipGLMapFlagsReadOnly
  // CU_GL_MAP_RESOURCE_FLAGS_WRITE_DISCARD = 0x02
#define cudaGLMapFlagsWriteDiscard                                       hipGLMapFlagsWriteDiscard

  // CUd3d9DeviceList
#define cudaD3D9DeviceList                                               hipD3D9DeviceList
  // CUd3d9DeviceList enum values
  // CU_D3D9_DEVICE_LIST_ALL = 0x01
#define cudaD3D9DeviceListAll                                            HIP_D3D9_DEVICE_LIST_ALL
  // CU_D3D9_DEVICE_LIST_CURRENT_FRAME = 0x02
#define cudaD3D9DeviceListCurrentFrame                                   HIP_D3D9_DEVICE_LIST_CURRENT_FRAME
  // CU_D3D9_DEVICE_LIST_NEXT_FRAME = 0x03
#define cudaD3D9DeviceListNextFrame                                      HIP_D3D9_DEVICE_LIST_NEXT_FRAME

  // CUd3d9map_flags
#define cudaD3D9MapFlags                                                 hipD3D9MapFlags
  // cudaD3D9MapFlags enum values
  // CU_D3D9_MAPRESOURCE_FLAGS_NONE = 0x00
#define cudaD3D9MapFlagsNone                                             HIP_D3D9_MAPRESOURCE_FLAGS_NONE
  // CU_D3D9_MAPRESOURCE_FLAGS_READONLY = 0x01
#define cudaD3D9MapFlagsReadOnly                                         HIP_D3D9_MAPRESOURCE_FLAGS_READONLY
  // CU_D3D9_MAPRESOURCE_FLAGS_WRITEDISCARD = 0x02
#define cudaD3D9MapFlagsWriteDiscard                                     HIP_D3D9_MAPRESOURCE_FLAGS_WRITEDISCARD

  // CUd3d9Register_flags
#define cudaD3D9RegisterFlags                                            hipD3D9RegisterFlags
  // cudaD3D9RegisterFlags enum values
  // CU_D3D9_REGISTER_FLAGS_NONE = 0x00
#define cudaD3D9RegisterFlagsNone                                        HIP_D3D9_REGISTER_FLAGS_NONE
  // CU_D3D9_REGISTER_FLAGS_ARRAY = 0x01
#define cudaD3D9RegisterFlagsArray                                       HIP_D3D9_REGISTER_FLAGS_ARRAY

  // CUd3d10DeviceList
#define cudaD3D10DeviceList                                              hipd3d10DeviceList
  // cudaD3D10DeviceList enum values
  // CU_D3D10_DEVICE_LIST_ALL = 0x01
#define cudaD3D10DeviceListAll                                           HIP_D3D10_DEVICE_LIST_ALL
  // CU_D3D10_DEVICE_LIST_CURRENT_FRAME = 0x02
#define cudaD3D10DeviceListCurrentFrame                                  HIP_D3D10_DEVICE_LIST_CURRENT_FRAME
  // CU_D3D10_DEVICE_LIST_NEXT_FRAME = 0x03
#define cudaD3D10DeviceListNextFrame                                     HIP_D3D10_DEVICE_LIST_NEXT_FRAME

  // CUd3d10map_flags
#define cudaD3D10MapFlags                                                hipD3D10MapFlags
  // cudaD3D10MapFlags enum values
  // CU_D3D10_MAPRESOURCE_FLAGS_NONE = 0x00
#define cudaD3D10MapFlagsNone                                            HIP_D3D10_MAPRESOURCE_FLAGS_NONE
  // CU_D3D10_MAPRESOURCE_FLAGS_READONLY = 0x01
#define cudaD3D10MapFlagsReadOnly                                        HIP_D3D10_MAPRESOURCE_FLAGS_READONLY
  // CU_D3D10_MAPRESOURCE_FLAGS_WRITEDISCARD = 0x02
#define cudaD3D10MapFlagsWriteDiscard                                    HIP_D3D10_MAPRESOURCE_FLAGS_WRITEDISCARD

  // CUd3d10Register_flags
#define cudaD3D10RegisterFlags                                           hipD3D10RegisterFlags
  // cudaD3D10RegisterFlags enum values
  // CU_D3D10_REGISTER_FLAGS_NONE = 0x00
#define cudaD3D10RegisterFlagsNone                                       HIP_D3D10_REGISTER_FLAGS_NONE
  // CU_D3D10_REGISTER_FLAGS_ARRAY = 0x01
#define cudaD3D10RegisterFlagsArray                                      HIP_D3D10_REGISTER_FLAGS_ARRAY

  // CUd3d11DeviceList
#define cudaD3D11DeviceList                                              hipd3d11DeviceList
  // cudaD3D11DeviceList enum values
  // CU_D3D11_DEVICE_LIST_ALL = 0x01
#define cudaD3D11DeviceListAll                                           HIP_D3D11_DEVICE_LIST_ALL
  // CU_D3D11_DEVICE_LIST_CURRENT_FRAME = 0x02
#define cudaD3D11DeviceListCurrentFrame                                  HIP_D3D11_DEVICE_LIST_CURRENT_FRAME
  // CU_D3D11_DEVICE_LIST_NEXT_FRAME = 0x03
#define cudaD3D11DeviceListNextFrame                                     HIP_D3D11_DEVICE_LIST_NEXT_FRAME

  // no analogue
#define libraryPropertyType                                              hipLibraryPropertyType_t
#define libraryPropertyType_t                                            hipLibraryPropertyType_t

  // CUaccessProperty
#define cudaAccessProperty                                               hipAccessProperty
  // CU_ACCESS_PROPERTY_NORMAL
#define cudaAccessPropertyNormal                                         hipAccessPropertyNormal
  // CU_ACCESS_PROPERTY_STREAMING
#define cudaAccessPropertyStreaming                                      hipAccessPropertyStreaming
  // CU_ACCESS_PROPERTY_PERSISTING
#define cudaAccessPropertyPersisting                                     hipAccessPropertyPersisting

  // CUsynchronizationPolicy
#define cudaSynchronizationPolicy                                        hipSynchronizationPolicy
  // CU_SYNC_POLICY_AUTO
#define cudaSyncPolicyAuto                                               hipSyncPolicyAuto
  // CU_SYNC_POLICY_SPIN
#define cudaSyncPolicySpin                                               hipSyncPolicySpin
  // CU_SYNC_POLICY_YIELD
#define cudaSyncPolicyYield                                              hipSyncPolicyYield
  // CU_SYNC_POLICY_BLOCKING_SYNC
#define cudaSyncPolicyBlockingSync                                       hipSyncPolicyBlockingSync

  // CUkernelNodeAttrID
#define cudaKernelNodeAttrID                                             hipKernelNodeAttrID
  // CU_KERNEL_NODE_ATTRIBUTE_ACCESS_POLICY_WINDOW
#define cudaKernelNodeAttributeAccessPolicyWindow                        hipKernelNodeAttributeAccessPolicyWindow
  // CU_KERNEL_NODE_ATTRIBUTE_COOPERATIVE
#define cudaKernelNodeAttributeCooperative                               hipKernelNodeAttributeCooperative
  // CU_KERNEL_NODE_ATTRIBUTE_PRIORITY
#define cudaKernelNodeAttributePriority                                  hipKernelNodeAttributePriority

  // CUmemPool_attribute
#define cudaMemPoolAttr                                                  hipMemPoolAttr
  // cudaMemPoolAttr enum values
  // CU_MEMPOOL_ATTR_REUSE_FOLLOW_EVENT_DEPENDENCIES
#define cudaMemPoolReuseFollowEventDependencies                          hipMemPoolReuseFollowEventDependencies
  // CU_MEMPOOL_ATTR_REUSE_ALLOW_OPPORTUNISTIC
#define cudaMemPoolReuseAllowOpportunistic                               hipMemPoolReuseAllowOpportunistic
  // CU_MEMPOOL_ATTR_REUSE_ALLOW_INTERNAL_DEPENDENCIES
#define cudaMemPoolReuseAllowInternalDependencies                        hipMemPoolReuseAllowInternalDependencies
  // CU_MEMPOOL_ATTR_RELEASE_THRESHOLD
#define cudaMemPoolAttrReleaseThreshold                                  hipMemPoolAttrReleaseThreshold
  // CU_MEMPOOL_ATTR_RESERVED_MEM_CURRENT
#define cudaMemPoolAttrReservedMemCurrent                                hipMemPoolAttrReservedMemCurrent
  // CU_MEMPOOL_ATTR_RESERVED_MEM_HIGH
#define cudaMemPoolAttrReservedMemHigh                                   hipMemPoolAttrReservedMemHigh
  // CU_MEMPOOL_ATTR_USED_MEM_CURRENT
#define cudaMemPoolAttrUsedMemCurrent                                    hipMemPoolAttrUsedMemCurrent
  // CU_MEMPOOL_ATTR_USED_MEM_HIGH
#define cudaMemPoolAttrUsedMemHigh                                       hipMemPoolAttrUsedMemHigh

  // CUmemLocationType
#define cudaMemLocationType                                              hipMemLocationType
  // cudaMemLocationType enum values
  // CU_MEM_LOCATION_TYPE_INVALID
#define cudaMemLocationTypeInvalid                                       hipMemLocationTypeInvalid
  // CU_MEM_LOCATION_TYPE_DEVICE
#define cudaMemLocationTypeDevice                                        hipMemLocationTypeDevice
  // CU_MEM_LOCATION_TYPE_HOST
#define cudaMemLocationTypeHost                                          hipMemLocationTypeHost
  // CU_MEM_LOCATION_TYPE_HOST_NUMA
#define cudaMemLocationTypeHostNuma                                      hipMemLocationTypeHostNuma
  // CU_MEM_LOCATION_TYPE_HOST_NUMA_CURRENT
#define cudaMemLocationTypeHostNumaCurrent                               hipMemLocationTypeHostNumaCurrent

  // CUmemAllocationType
#define cudaMemAllocationType                                            hipMemAllocationType
  // CUmemAllocationType enum values
  // CU_MEM_ALLOCATION_TYPE_INVALID
#define cudaMemAllocationTypeInvalid                                     hipMemAllocationTypeInvalid
  // CU_MEM_ALLOCATION_TYPE_PINNED
#define cudaMemAllocationTypePinned                                      hipMemAllocationTypePinned
  // CU_MEM_ALLOCATION_TYPE_MAX
#define cudaMemAllocationTypeMax                                         hipMemAllocationTypeMax

  // CUmemAccess_flags
#define cudaMemAccessFlags                                               hipMemAccessFlags
  // cudaMemAccessFlags enum values
  // CU_MEM_ACCESS_FLAGS_PROT_NONE
#define cudaMemAccessFlagsProtNone                                       hipMemAccessFlagsProtNone
  // CU_MEM_ACCESS_FLAGS_PROT_READ
#define cudaMemAccessFlagsProtRead                                       hipMemAccessFlagsProtRead
  // CU_MEM_ACCESS_FLAGS_PROT_READWRITE
#define cudaMemAccessFlagsProtReadWrite                                  hipMemAccessFlagsProtReadWrite

  // CUmemAllocationHandleType
#define cudaMemAllocationHandleType                                      hipMemAllocationHandleType
  // cudaMemAllocationHandleType enum values
  // CU_MEM_HANDLE_TYPE_NONE
#define cudaMemHandleTypeNone                                            hipMemHandleTypeNone
  // CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR
#define cudaMemHandleTypePosixFileDescriptor                             hipMemHandleTypePosixFileDescriptor
  // CU_MEM_HANDLE_TYPE_WIN32
#define cudaMemHandleTypeWin32                                           hipMemHandleTypeWin32
  // CU_MEM_HANDLE_TYPE_WIN32_KMT
#define cudaMemHandleTypeWin32Kmt                                        hipMemHandleTypeWin32Kmt

  // CUstreamUpdateCaptureDependencies_flags
#define cudaStreamUpdateCaptureDependenciesFlags                         hipStreamUpdateCaptureDependenciesFlags
  // cudaStreamUpdateCaptureDependenciesFlags enum values
  // CU_STREAM_ADD_CAPTURE_DEPENDENCIES
#define cudaStreamAddCaptureDependencies                                 hipStreamAddCaptureDependencies
  // CU_STREAM_SET_CAPTURE_DEPENDENCIES
#define cudaStreamSetCaptureDependencies                                 hipStreamSetCaptureDependencies

  // CUuserObject_flags
#define cudaUserObjectFlags                                              hipUserObjectFlags
  // cudaUserObjectFlags enum values
  // CU_USER_OBJECT_NO_DESTRUCTOR_SYNC
#define cudaUserObjectNoDestructorSync                                   hipUserObjectNoDestructorSync

  // CUuserObjectRetain_flags
#define cudaUserObjectRetainFlags                                        hipUserObjectRetainFlags
  // cudaUserObjectRetainFlags enum values
  // CU_GRAPH_USER_OBJECT_MOVE
#define cudaGraphUserObjectMove                                          hipGraphUserObjectMove

  // CUflushGPUDirectRDMAWritesOptions
#define cudaFlushGPUDirectRDMAWritesOptions                              hipFlushGPUDirectRDMAWritesOptions
  // cudaFlushGPUDirectRDMAWritesOptions enum values
  // CU_FLUSH_GPU_DIRECT_RDMA_WRITES_OPTION_HOST
#define cudaFlushGPUDirectRDMAWritesOptionHost                           hipFlushGPUDirectRDMAWritesOptionHost
  // CU_FLUSH_GPU_DIRECT_RDMA_WRITES_OPTION_MEMOPS
#define cudaFlushGPUDirectRDMAWritesOptionMemOps                         hipFlushGPUDirectRDMAWritesOptionMemOps

  // CUGPUDirectRDMAWritesOrdering
#define cudaGPUDirectRDMAWritesOrdering                                  hipGPUDirectRDMAWritesOrdering
  // cudaGPUDirectRDMAWritesOrdering enum values
  // CU_GPU_DIRECT_RDMA_WRITES_ORDERING_NONE
#define cudaGPUDirectRDMAWritesOrderingNone                              hipGPUDirectRDMAWritesOrderingNone
  // CU_GPU_DIRECT_RDMA_WRITES_ORDERING_OWNER
#define cudaGPUDirectRDMAWritesOrderingOwner                             hipGPUDirectRDMAWritesOrderingOwner
  // CU_GPU_DIRECT_RDMA_WRITES_ORDERING_ALL_DEVICES
#define cudaGPUDirectRDMAWritesOrderingAllDevices                        hipGPUDirectRDMAWritesOrderingAllDevices

  // CUflushGPUDirectRDMAWritesScope
#define cudaFlushGPUDirectRDMAWritesScope                                hipFlushGPUDirectRDMAWritesScope
  // cudaFlushGPUDirectRDMAWritesScope enum values
  // CU_FLUSH_GPU_DIRECT_RDMA_WRITES_TO_OWNER
#define cudaFlushGPUDirectRDMAWritesToOwner                              hipFlushGPUDirectRDMAWritesToOwner
  // CU_FLUSH_GPU_DIRECT_RDMA_WRITES_TO_ALL_DEVICES
#define cudaFlushGPUDirectRDMAWritesToAllDevices                         hipFlushGPUDirectRDMAWritesToAllDevices

  // CUflushGPUDirectRDMAWritesTarget
#define cudaFlushGPUDirectRDMAWritesTarget                               hipFlushGPUDirectRDMAWritesTarget
  // cudaFlushGPUDirectRDMAWritesTarget enum values
  // CU_FLUSH_GPU_DIRECT_RDMA_WRITES_TARGET_CURRENT_CTX
#define cudaFlushGPUDirectRDMAWritesTargetCurrentDevice                  hipFlushGPUDirectRDMAWritesTargetCurrentDevice

  // CUdriverProcAddress_flags
#define cudaGetDriverEntryPointFlags                                     hipGetDriverEntryPointFlags
  // cudaGetDriverEntryPointFlags enum values
  // CU_GET_PROC_ADDRESS_DEFAULT
#define cudaEnableDefault                                                hipEnableDefault
  // CU_GET_PROC_ADDRESS_LEGACY_STREAM
#define cudaEnableLegacyStream                                           hipEnableLegacyStream
  // CU_GET_PROC_ADDRESS_PER_THREAD_DEFAULT_STREAM
#define cudaEnablePerThreadDefaultStream                                 hipEnablePerThreadDefaultStream

  // CUgraphDebugDot_flags
#define cudaGraphDebugDotFlags                                           hipGraphDebugDotFlags
  // cudaGraphDebugDotFlags enum values
  // CU_GRAPH_DEBUG_DOT_FLAGS_VERBOSE
#define cudaGraphDebugDotFlagsVerbose                                    hipGraphDebugDotFlagsVerbose
  // CU_GRAPH_DEBUG_DOT_FLAGS_KERNEL_NODE_PARAMS
#define cudaGraphDebugDotFlagsKernelNodeParams                           hipGraphDebugDotFlagsKernelNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_MEMCPY_NODE_PARAMS
#define cudaGraphDebugDotFlagsMemcpyNodeParams                           hipGraphDebugDotFlagsMemcpyNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_MEMSET_NODE_PARAMS
#define cudaGraphDebugDotFlagsMemsetNodeParams                           hipGraphDebugDotFlagsMemsetNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_HOST_NODE_PARAMS
#define cudaGraphDebugDotFlagsHostNodeParams                             hipGraphDebugDotFlagsHostNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_EVENT_NODE_PARAMS
#define cudaGraphDebugDotFlagsEventNodeParams                            hipGraphDebugDotFlagsEventNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_EXT_SEMAS_SIGNAL_NODE_PARAMS
#define cudaGraphDebugDotFlagsExtSemasSignalNodeParams                   hipGraphDebugDotFlagsExtSemasSignalNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_EXT_SEMAS_WAIT_NODE_PARAMS
#define cudaGraphDebugDotFlagsExtSemasWaitNodeParams                     hipGraphDebugDotFlagsExtSemasWaitNodeParams
  // CU_GRAPH_DEBUG_DOT_FLAGS_KERNEL_NODE_ATTRIBUTES
#define cudaGraphDebugDotFlagsKernelNodeAttributes                       hipGraphDebugDotFlagsKernelNodeAttributes
  // CU_GRAPH_DEBUG_DOT_FLAGS_HANDLES
#define cudaGraphDebugDotFlagsHandles                                    hipGraphDebugDotFlagsHandles
  // CU_GRAPH_DEBUG_DOT_FLAGS_CONDITIONAL_NODE_PARAMS
#define cudaGraphDebugDotFlagsConditionalNodeParams                      hipGraphDebugDotFlagsConditionalNodeParams

  // CUgraphMem_attribute
#define cudaGraphMemAttributeType                                        hipGraphMemAttributeType
  // cudaGraphMemAttributeType enum values
  // CU_GRAPH_MEM_ATTR_USED_MEM_CURRENT
#define cudaGraphMemAttrUsedMemCurrent                                   hipGraphMemAttrUsedMemCurrent
  // CU_GRAPH_MEM_ATTR_USED_MEM_HIGH
#define cudaGraphMemAttrUsedMemHigh                                      hipGraphMemAttrUsedMemHigh
  // CU_GRAPH_MEM_ATTR_RESERVED_MEM_CURRENT
#define cudaGraphMemAttrReservedMemCurrent                               hipGraphMemAttrReservedMemCurrent
  // CU_GRAPH_MEM_ATTR_RESERVED_MEM_HIGH
#define cudaGraphMemAttrReservedMemHigh                                  hipGraphMemAttrReservedMemHigh

  // CUgraphInstantiate_flags
#define cudaGraphInstantiateFlags                                        hipGraphInstantiateFlags
  // cudaGraphInstantiateFlags enum values
  // CUDA_GRAPH_INSTANTIATE_FLAG_AUTO_FREE_ON_LAUNCH
#define cudaGraphInstantiateFlagAutoFreeOnLaunch                         hipGraphInstantiateFlagAutoFreeOnLaunch
  // CUDA_GRAPH_INSTANTIATE_FLAG_UPLOAD
#define cudaGraphInstantiateFlagUpload                                   hipGraphInstantiateFlagUpload
  // CUDA_GRAPH_INSTANTIATE_FLAG_DEVICE_LAUNCH
#define cudaGraphInstantiateFlagDeviceLaunch                             hipGraphInstantiateFlagDeviceLaunch
  // CUDA_GRAPH_INSTANTIATE_FLAG_USE_NODE_PRIORITY
#define cudaGraphInstantiateFlagUseNodePriority                          hipGraphInstantiateFlagUseNodePriority

  // CUclusterSchedulingPolicy
#define cudaClusterSchedulingPolicy                                      hipClusterSchedulingPolicy
  // cudaClusterSchedulingPolicy enum values
  // CU_CLUSTER_SCHEDULING_POLICY_DEFAULT
#define cudaClusterSchedulingPolicyDefault                               hipClusterSchedulingPolicyDefault
  // CU_CLUSTER_SCHEDULING_POLICY_SPREAD
#define cudaClusterSchedulingPolicySpread                                hipClusterSchedulingPolicySpread
  // CU_CLUSTER_SCHEDULING_POLICY_LOAD_BALANCING
#define cudaClusterSchedulingPolicyLoadBalancing                         hipClusterSchedulingPolicyLoadBalancing

  // CUlaunchAttributeID
#define cudaLaunchAttributeID                                            hipLaunchAttributeID
  // cudaLaunchAttributeID enum values
  // CU_LAUNCH_ATTRIBUTE_IGNORE
#define cudaLaunchAttributeIgnore                                        hipLaunchAttributeIgnore
  // CU_LAUNCH_ATTRIBUTE_ACCESS_POLICY_WINDOW
#define cudaLaunchAttributeAccessPolicyWindow                            hipLaunchAttributeAccessPolicyWindow
  // CU_LAUNCH_ATTRIBUTE_COOPERATIVE
#define cudaLaunchAttributeCooperative                                   hipLaunchAttributeCooperative
  // CU_LAUNCH_ATTRIBUTE_SYNCHRONIZATION_POLICY
#define cudaLaunchAttributeSynchronizationPolicy                         hipLaunchAttributeSynchronizationPolicy
  // CU_LAUNCH_ATTRIBUTE_CLUSTER_DIMENSION
#define cudaLaunchAttributeClusterDimension                              hipLaunchAttributeClusterDimension
  // CU_LAUNCH_ATTRIBUTE_CLUSTER_SCHEDULING_POLICY_PREFERENCE
#define cudaLaunchAttributeClusterSchedulingPolicyPreference             hipLaunchAttributeClusterSchedulingPolicyPreference
  // CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_STREAM_SERIALIZATION
#define cudaLaunchAttributeProgrammaticStreamSerialization               hipLaunchAttributeProgrammaticStreamSerialization
  // CU_LAUNCH_ATTRIBUTE_PROGRAMMATIC_EVENT
#define cudaLaunchAttributeProgrammaticEvent                             hipLaunchAttributeProgrammaticEvent
  // CU_LAUNCH_ATTRIBUTE_PRIORITY
#define cudaLaunchAttributePriority                                      hipLaunchAttributePriority
  // CU_LAUNCH_ATTRIBUTE_MEM_SYNC_DOMAIN_MAP
#define cudaLaunchAttributeMemSyncDomainMap                              hipLaunchAttributeMemSyncDomainMap
  // CU_LAUNCH_ATTRIBUTE_MEM_SYNC_DOMAIN
#define cudaLaunchAttributeMemSyncDomain                                 hipLaunchAttributeMemSyncDomain
  // CU_LAUNCH_ATTRIBUTE_LAUNCH_COMPLETION_EVENT
#define cudaLaunchAttributeLaunchCompletionEvent                         hipLaunchAttributeLaunchCompletionEvent

  // CUgraphInstantiateResult
#define cudaGraphInstantiateResult                                       hipGraphInstantiateResult
  // cudaGraphInstantiateResult enum values
  // CUDA_GRAPH_INSTANTIATE_SUCCESS
#define cudaGraphInstantiateSuccess                                      hipGraphInstantiateSuccess
  // CUDA_GRAPH_INSTANTIATE_ERROR
#define cudaGraphInstantiateError                                        hipGraphInstantiateError
  // CUDA_GRAPH_INSTANTIATE_INVALID_STRUCTURE
#define cudaGraphInstantiateInvalidStructure                             hipGraphInstantiateInvalidStructure
  // CUDA_GRAPH_INSTANTIATE_NODE_OPERATION_NOT_SUPPORTED
#define cudaGraphInstantiateNodeOperationNotSupported                    hipGraphInstantiateNodeOperationNotSupported
  // CUDA_GRAPH_INSTANTIATE_MULTIPLE_CTXS_NOT_SUPPORTED
#define cudaGraphInstantiateMultipleDevicesNotSupported                  hipGraphInstantiateMultipleDevicesNotSupported

  // no analogues
#define cudaDriverEntryPointQueryResult                                  hipDriverEntryPointQueryResult
  // cudaDriverEntryPointQueryResult enum values
#define cudaDriverEntryPointSuccess                                      cudaDriverEntryPointSuccess
#define cudaDriverEntryPointSymbolNotFound                               hipDriverEntryPointSymbolNotFound
#define cudaDriverEntryPointVersionNotSufficent                          hipDriverEntryPointVersionNotSufficent

  // CUlaunchMemSyncDomain
#define cudaLaunchMemSyncDomain                                          hipLaunchMemSyncDomain
  // cudaLaunchMemSyncDomain enum values
  // CU_LAUNCH_MEM_SYNC_DOMAIN_DEFAULT
#define cudaLaunchMemSyncDomainDefault                                   hipLaunchMemSyncDomainDefault
  // CU_LAUNCH_MEM_SYNC_DOMAIN_REMOTE
#define cudaLaunchMemSyncDomainRemote                                    hipLaunchMemSyncDomainRemote

  // CUdeviceNumaConfig
#define cudaDeviceNumaConfig                                             hipDeviceNumaConfig
  // cudaDeviceNumaConfig enum values
  // CU_DEVICE_NUMA_CONFIG_NONE
#define cudaDeviceNumaConfigNone                                         hipDeviceNumaConfigNone
  // CU_DEVICE_NUMA_CONFIG_NUMA_NODE
#define cudaDeviceNumaConfigNumaNode                                     hipDeviceNumaConfigNumaNode

  // no analogues
#define cudaGraphConditionalHandleFlags                                  hipGraphConditionalHandleFlags
  // cudaGraphConditionalHandleFlags enum values
  //
#define cudaGraphCondAssignDefault                                       hipGraphCondAssignDefault

  // CUgraphConditionalNodeType
#define cudaGraphConditionalNodeType                                     hipGraphConditionalNodeType
  // CUgraphConditionalNodeType enum values
  // CU_GRAPH_COND_TYPE_IF
#define cudaGraphCondTypeIf                                              hipGraphCondTypeIf
  // CU_GRAPH_COND_TYPE_WHILE
#define cudaGraphCondTypeWhile                                           hipGraphCondTypeWhile

  // CUgraphDependencyType
#define cudaGraphDependencyType                                          hipGraphDependencyType
  // CUgraphDependencyType_enum
#define cudaGraphDependencyType_enum                                     hipGraphDependencyType
  // CUgraphDependencyType enum values
  // CU_GRAPH_DEPENDENCY_TYPE_DEFAULT
#define cudaGraphDependencyTypeDefault                                   hipGraphDependencyTypeDefault
  // CU_GRAPH_DEPENDENCY_TYPE_PROGRAMMATIC
#define cudaGraphDependencyTypeProgrammatic                              hipGraphDependencyTypeProgrammatic

  // 4. Typedefs

  // CUhostFn
#define cudaHostFn_t                                                     hipHostFn_t

  // CUstreamCallback
#define cudaStreamCallback_t                                             hipStreamCallback_t

  // CUsurfObject
#define cudaSurfaceObject_t                                              hipSurfaceObject_t

  // CUtexObject
#define cudaTextureObject_t                                              hipTextureObject_t

  // CUuuid
#define cudaUUID_t                                                       hipUUID

  // CUmemoryPool
#define cudaMemPool_t                                                    hipMemPool_t

  // CUuserObject
#define cudaUserObject_t                                                 hipUserObject_t

  // CUgraphConditionalHandle
#define cudaGraphConditionalHandle                                       hipGraphConditionalHandle

  // 5. Defines

  // no analogue
#define CUDA_EGL_MAX_PLANES                                              HIP_EGL_MAX_PLANES
  // CU_IPC_HANDLE_SIZE
#define CUDA_IPC_HANDLE_SIZE                                             HIP_IPC_HANDLE_SIZE
  // no analogue
#define cudaArrayDefault                                                 hipArrayDefault
  // CUDA_ARRAY3D_LAYERED
#define cudaArrayLayered                                                 hipArrayLayered
  // CUDA_ARRAY3D_SURFACE_LDST
#define cudaArraySurfaceLoadStore                                        hipArraySurfaceLoadStore
  // CUDA_ARRAY3D_CUBEMAP
#define cudaArrayCubemap                                                 hipArrayCubemap
  // CUDA_ARRAY3D_TEXTURE_GATHER
#define cudaArrayTextureGather                                           hipArrayTextureGather
  // CUDA_ARRAY3D_COLOR_ATTACHMENT
#define cudaArrayColorAttachment                                         hipArrayColorAttachment
  // CUDA_ARRAY3D_SPARSE
#define cudaArraySparse                                                  hipArraySparse
  // CUDA_ARRAY3D_DEFERRED_MAPPING
#define cudaArrayDeferredMapping                                         hipArrayDeferredMapping
  // CUDA_COOPERATIVE_LAUNCH_MULTI_DEVICE_NO_PRE_LAUNCH_SYNC
#define cudaCooperativeLaunchMultiDeviceNoPreSync                        hipCooperativeLaunchMultiDeviceNoPreSync
  // CUDA_COOPERATIVE_LAUNCH_MULTI_DEVICE_NO_POST_LAUNCH_SYNC
#define cudaCooperativeLaunchMultiDeviceNoPostSync                       hipCooperativeLaunchMultiDeviceNoPostSync
  // CU_DEVICE_CPU ((CUdevice)-1)
#define cudaCpuDeviceId                                                  hipCpuDeviceId
  // CU_DEVICE_INVALID ((CUdevice)-2)
#define cudaInvalidDeviceId                                              hipInvalidDeviceId
  // CU_CTX_BLOCKING_SYNC
  // NOTE: Deprecated since CUDA 4.0 and replaced with cudaDeviceScheduleBlockingSync
#define cudaDeviceBlockingSync                                           hipDeviceScheduleBlockingSync
  // CU_CTX_LMEM_RESIZE_TO_MAX
  // NOTE: hipDeviceLmemResizeToMax = 0x16
#define cudaDeviceLmemResizeToMax                                        hipDeviceLmemResizeToMax
  // CU_CTX_SYNC_MEMOPS
#define cudaDeviceSyncMemops                                             hipDeviceSyncMemops
  // CU_CTX_MAP_HOST
#define cudaDeviceMapHost                                                hipDeviceMapHost
  // CU_CTX_FLAGS_MASK
#define cudaDeviceMask                                                   hipDeviceMask
  // no analogue
#define cudaDevicePropDontCare                                           hipDevicePropDontCare
  // CU_CTX_SCHED_AUTO
#define cudaDeviceScheduleAuto                                           hipDeviceScheduleAuto
  // CU_CTX_SCHED_SPIN
#define cudaDeviceScheduleSpin                                           hipDeviceScheduleSpin
  // CU_CTX_SCHED_YIELD
#define cudaDeviceScheduleYield                                          hipDeviceScheduleYield
  // CU_CTX_SCHED_BLOCKING_SYNC
#define cudaDeviceScheduleBlockingSync                                   hipDeviceScheduleBlockingSync
  // CU_CTX_SCHED_MASK
#define cudaDeviceScheduleMask                                           hipDeviceScheduleMask
  // CU_EVENT_DEFAULT
#define cudaEventDefault                                                 hipEventDefault
  // CU_EVENT_BLOCKING_SYNC
#define cudaEventBlockingSync                                            hipEventBlockingSync
  // CU_EVENT_DISABLE_TIMING
#define cudaEventDisableTiming                                           hipEventDisableTiming
  // CU_EVENT_INTERPROCESS
#define cudaEventInterprocess                                            hipEventInterprocess
  // CU_EVENT_RECORD_DEFAULT
#define cudaEventRecordDefault                                           hipEventRecordDefault
  // CU_EVENT_RECORD_EXTERNAL
#define cudaEventRecordExternal                                          hipEventRecordExternal
  // CU_EVENT_WAIT_DEFAULT
#define cudaEventWaitDefault                                             hipEventWaitDefault
  // CU_EVENT_WAIT_EXTERNAL
#define cudaEventWaitExternal                                            hipEventWaitExternal
  // CUDA_EXTERNAL_MEMORY_DEDICATED
#define cudaExternalMemoryDedicated                                      hipExternalMemoryDedicated
  // CUDA_EXTERNAL_SEMAPHORE_SIGNAL_SKIP_NVSCIBUF_MEMSYNC
#define cudaExternalSemaphoreSignalSkipNvSciBufMemSync                   hipExternalSemaphoreSignalSkipNvSciBufMemSync
  // CUDA_EXTERNAL_SEMAPHORE_WAIT_SKIP_NVSCIBUF_MEMSYNC
#define cudaExternalSemaphoreWaitSkipNvSciBufMemSync                     hipExternalSemaphoreWaitSkipNvSciBufMemSync
  // CUDA_NVSCISYNC_ATTR_SIGNAL
#define cudaNvSciSyncAttrSignal                                          hipNvSciSyncAttrSignal
  // CUDA_NVSCISYNC_ATTR_WAIT
#define cudaNvSciSyncAttrWait                                            hipNvSciSyncAttrWait
  // no analogue
#define cudaHostAllocDefault                                             hipHostMallocDefault
  // CU_MEMHOSTALLOC_PORTABLE
#define cudaHostAllocPortable                                            hipHostMallocPortable
  // CU_MEMHOSTALLOC_DEVICEMAP
#define cudaHostAllocMapped                                              hipHostMallocMapped
  // CU_MEMHOSTALLOC_WRITECOMBINED
#define cudaHostAllocWriteCombined                                       hipHostMallocWriteCombined
  // no analogue
#define cudaHostRegisterDefault                                          hipHostRegisterDefault
  // CU_MEMHOSTREGISTER_PORTABLE
#define cudaHostRegisterPortable                                         hipHostRegisterPortable
  // CU_MEMHOSTREGISTER_DEVICEMAP
#define cudaHostRegisterMapped                                           hipHostRegisterMapped
  // CU_MEMHOSTREGISTER_IOMEMORY
#define cudaHostRegisterIoMemory                                         hipHostRegisterIoMemory
  // CU_MEMHOSTREGISTER_READ_ONLY
#define cudaHostRegisterReadOnly                                         hipHostRegisterReadOnly
  // CU_IPC_MEM_LAZY_ENABLE_PEER_ACCESS
#define cudaIpcMemLazyEnablePeerAccess                                   hipIpcMemLazyEnablePeerAccess
  // CU_MEM_ATTACH_GLOBAL
#define cudaMemAttachGlobal                                              hipMemAttachGlobal
  // CU_MEM_ATTACH_HOST
#define cudaMemAttachHost                                                hipMemAttachHost
  // CU_MEM_ATTACH_SINGLE
#define cudaMemAttachSingle                                              hipMemAttachSingle
  // no analogue
#define cudaTextureType1D                                                hipTextureType1D
  // no analogue
#define cudaTextureType2D                                                hipTextureType2D
  // no analogue
#define cudaTextureType3D                                                hipTextureType3D
  // no analogue
#define cudaTextureTypeCubemap                                           hipTextureTypeCubemap
  // no analogue
#define cudaTextureType1DLayered                                         hipTextureType1DLayered
  // no analogue
#define cudaTextureType2DLayered                                         hipTextureType2DLayered
  // no analogue
#define cudaTextureTypeCubemapLayered                                    hipTextureTypeCubemapLayered
  // CU_OCCUPANCY_DEFAULT
#define cudaOccupancyDefault                                             hipOccupancyDefault
  // CU_OCCUPANCY_DISABLE_CACHING_OVERRIDE
#define cudaOccupancyDisableCachingOverride                              hipOccupancyDisableCachingOverride
  // CU_STREAM_DEFAULT
#define cudaStreamDefault                                                hipStreamDefault
  // CU_STREAM_NON_BLOCKING
#define cudaStreamNonBlocking                                            hipStreamNonBlocking
  // CU_STREAM_LEGACY ((CUstream)0x1)
#define cudaStreamLegacy                                                 hipStreamLegacy
  // CU_STREAM_PER_THREAD ((CUstream)0x2)
#define cudaStreamPerThread                                              hipStreamPerThread
  // CU_ARRAY_SPARSE_PROPERTIES_SINGLE_MIPTAIL
#define cudaArraySparsePropertiesSingleMipTail                           hipArraySparsePropertiesSingleMipTail
  // CU_KERNEL_NODE_ATTRIBUTE_CLUSTER_DIMENSION
#define cudaKernelNodeAttributeClusterDimension                          hipKernelNodeAttributeClusterDimension
  // CU_KERNEL_NODE_ATTRIBUTE_CLUSTER_SCHEDULING_POLICY_PREFERENCE
#define cudaKernelNodeAttributeClusterSchedulingPolicyPreference         hipKernelNodeAttributeClusterSchedulingPolicyPreference
  // CU_KERNEL_NODE_ATTRIBUTE_MEM_SYNC_DOMAIN_MAP
#define cudaKernelNodeAttributeMemSyncDomainMap                          hipKernelNodeAttributeMemSyncDomainMap
  // CU_KERNEL_NODE_ATTRIBUTE_MEM_SYNC_DOMAIN
#define cudaKernelNodeAttributeMemSyncDomain                             hipKernelNodeAttributeMemSyncDomain
  //
#define cudaInitDeviceFlagsAreValid                                      hipInitDeviceFlagsAreValid
  // CUstreamAttrID
#define cudaStreamAttrID                                                 hipStreamAttrID
  // CU_STREAM_ATTRIBUTE_ACCESS_POLICY_WINDOW
#define cudaStreamAttributeAccessPolicyWindow                            hipStreamAttributeAccessPolicyWindow
  // CU_STREAM_ATTRIBUTE_SYNCHRONIZATION_POLICY
#define cudaStreamAttributeSynchronizationPolicy                         hipStreamAttributeSynchronizationPolicy
  // CU_STREAM_ATTRIBUTE_PRIORITY
#define cudaStreamAttributePriority                                      hipStreamAttributePriority
  // CU_STREAM_ATTRIBUTE_MEM_SYNC_DOMAIN_MAP
#define cudaStreamAttributeMemSyncDomainMap                              hipStreamAttributeMemSyncDomainMap
  // CU_STREAM_ATTRIBUTE_MEM_SYNC_DOMAIN
#define cudaStreamAttributeMemSyncDomain                                 hipStreamAttributeMemSyncDomain
  // CU_GRAPH_KERNEL_NODE_PORT_DEFAULT
#define cudaGraphKernelNodePortDefault                                   hipGraphKernelNodePortDefault
  // CU_GRAPH_KERNEL_NODE_PORT_PROGRAMMATIC
#define cudaGraphKernelNodePortProgrammatic                              hipGraphKernelNodePortProgrammatic
  // CU_GRAPH_KERNEL_NODE_PORT_LAUNCH_ORDER
#define cudaGraphKernelNodePortLaunchCompletion                          hipGraphKernelNodePortLaunchCompletion




  // 1. Device Management
  // no analogue
#define cudaChooseDevice                                        hipChooseDevice
  // cuFlushGPUDirectRDMAWrites
#define cudaDeviceFlushGPUDirectRDMAWrites                      hipDeviceFlushGPUDirectRDMAWrites
  // cuDeviceGetAttribute
#define cudaDeviceGetAttribute                                  hipDeviceGetAttribute
  // cuDeviceGetByPCIBusId
#define cudaDeviceGetByPCIBusId                                 hipDeviceGetByPCIBusId
  // no analogue
#define cudaDeviceGetCacheConfig                                hipDeviceGetCacheConfig
  // cuCtxGetLimit
#define cudaDeviceGetLimit                                      hipDeviceGetLimit
  // cuDeviceGetNvSciSyncAttributes
#define cudaDeviceGetNvSciSyncAttributes                        hipDeviceGetNvSciSyncAttributes
  // cuDeviceGetP2PAttribute
#define cudaDeviceGetP2PAttribute                               hipDeviceGetP2PAttribute
  // cuDeviceGetPCIBusId
#define cudaDeviceGetPCIBusId                                   hipDeviceGetPCIBusId
  // cuCtxGetSharedMemConfig
#define cudaDeviceGetSharedMemConfig                            hipDeviceGetSharedMemConfig
  // cuCtxGetStreamPriorityRange
#define cudaDeviceGetStreamPriorityRange                        hipDeviceGetStreamPriorityRange
  // no analogue
#define cudaDeviceReset                                         hipDeviceReset
  // no analogue
#define cudaDeviceSetCacheConfig                                hipDeviceSetCacheConfig
  // cuCtxSetLimit
#define cudaDeviceSetLimit                                      hipDeviceSetLimit
  // cuCtxSetSharedMemConfig
#define cudaDeviceSetSharedMemConfig                            hipDeviceSetSharedMemConfig
  // cuCtxSynchronize
#define cudaDeviceSynchronize                                   hipDeviceSynchronize
  // cuDeviceGet
  // NOTE: cuDeviceGet has no attr: int ordinal
#define cudaGetDevice                                           hipGetDevice
  // cuDeviceGetCount
#define cudaGetDeviceCount                                      hipGetDeviceCount
  // cuCtxGetFlags
#define cudaGetDeviceFlags                                      hipGetDeviceFlags
  // no analogue
  // NOTE: Not equal to cuDeviceGetProperties due to different attributes: CUdevprop and cudaDeviceProp
#define cudaGetDeviceProperties                                 hipGetDeviceProperties
  // cuIpcCloseMemHandle
#define cudaIpcCloseMemHandle                                   hipIpcCloseMemHandle
  // cuIpcGetEventHandle
#define cudaIpcGetEventHandle                                   hipIpcGetEventHandle
  // cuIpcGetMemHandle
#define cudaIpcGetMemHandle                                     hipIpcGetMemHandle
  // cuIpcOpenEventHandle
#define cudaIpcOpenEventHandle                                  hipIpcOpenEventHandle
  // cuIpcOpenMemHandle
#define cudaIpcOpenMemHandle                                    hipIpcOpenMemHandle
  // no analogue
#define cudaSetDevice                                           hipSetDevice
  // cuCtxGetFlags
#define cudaSetDeviceFlags                                      hipSetDeviceFlags
  // no analogue
#define cudaSetValidDevices                                     hipSetValidDevices
  // cuDeviceGetTexture1DLinearMaxWidth
#define cudaDeviceGetTexture1DLinearMaxWidth                    hipDeviceGetTexture1DLinearMaxWidth
  // cuDeviceGetDefaultMemPool
#define cudaDeviceGetDefaultMemPool                             hipDeviceGetDefaultMemPool
  // cuDeviceSetMemPool
#define cudaDeviceSetMemPool                                    hipDeviceSetMemPool
  // cuDeviceGetMemPool
#define cudaDeviceGetMemPool                                    hipDeviceGetMemPool
  //
#define cudaInitDevice                                          hipInitDevice

  // 2. Thread Management [DEPRECATED]
  // no analogue
#define cudaThreadExit                                          hipDeviceReset
  // no analogue
#define cudaThreadGetCacheConfig                                hipDeviceGetCacheConfig
  // no analogue
#define cudaThreadGetLimit                                      hipThreadGetLimit
  // no analogue
#define cudaThreadSetCacheConfig                                hipDeviceSetCacheConfig
  // no analogue
#define cudaThreadSetLimit                                      hipThreadSetLimit
  // cuCtxSynchronize
#define cudaThreadSynchronize                                   hipDeviceSynchronize

  // 3. Error Handling
  // no analogue
  // NOTE: cudaGetErrorName and cuGetErrorName have different signatures
#define cudaGetErrorName                                        hipGetErrorName
  // no analogue
  // NOTE: cudaGetErrorString and cuGetErrorString have different signatures
#define cudaGetErrorString                                      hipGetErrorString
  // no analogue
#define cudaGetLastError                                        hipGetLastError
  // no analogue
#define cudaPeekAtLastError                                     hipPeekAtLastError

  // 4. Stream Management
  // cuStreamAddCallback
#define cudaStreamAddCallback                                   hipStreamAddCallback
  // cuCtxResetPersistingL2Cache
#define cudaCtxResetPersistingL2Cache                           hipCtxResetPersistingL2Cache
  // cuStreamAttachMemAsync
#define cudaStreamAttachMemAsync                                hipStreamAttachMemAsync
  // cuStreamBeginCapture
#define cudaStreamBeginCapture                                  hipStreamBeginCapture
  // cuStreamBeginCaptureToGraph
#define cudaStreamBeginCaptureToGraph                           hipStreamBeginCaptureToGraph
  // cuStreamCopyAttributes
#define cudaStreamCopyAttributes                                hipStreamCopyAttributes
  // no analogue
  // NOTE: Not equal to cuStreamCreate due to different signatures
#define cudaStreamCreate                                        hipStreamCreate
  // cuStreamCreate
#define cudaStreamCreateWithFlags                               hipStreamCreateWithFlags
  // cuStreamCreateWithPriority
#define cudaStreamCreateWithPriority                            hipStreamCreateWithPriority
  // cuStreamDestroy
#define cudaStreamDestroy                                       hipStreamDestroy
  // cuStreamEndCapture
#define cudaStreamEndCapture                                    hipStreamEndCapture
  // cuStreamGetAttribute
#define cudaStreamGetAttribute                                  hipStreamGetAttribute
  // cuStreamSetAttribute
#define cudaStreamSetAttribute                                  hipStreamSetAttribute
  // cuStreamGetFlags
#define cudaStreamGetFlags                                      hipStreamGetFlags
  // cuStreamGetPriority
#define cudaStreamGetPriority                                   hipStreamGetPriority
  // cuStreamIsCapturing
#define cudaStreamIsCapturing                                   hipStreamIsCapturing
  // cuStreamGetCaptureInfo
#define cudaStreamGetCaptureInfo                                hipStreamGetCaptureInfo
  // cuStreamGetCaptureInfo_v3
#define cudaStreamGetCaptureInfo_v3                             hipStreamGetCaptureInfo_v3
  // cuStreamUpdateCaptureDependencies
#define cudaStreamUpdateCaptureDependencies                     hipStreamUpdateCaptureDependencies
  // cuStreamUpdateCaptureDependencies_v2
#define cudaStreamUpdateCaptureDependencies_v2                  hipStreamUpdateCaptureDependencies_v2
  // cuStreamQuery
#define cudaStreamQuery                                         hipStreamQuery
  // cuStreamSynchronize
#define cudaStreamSynchronize                                   hipStreamSynchronize
  // cuStreamWaitEvent
#define cudaStreamWaitEvent                                     hipStreamWaitEvent
  // cuThreadExchangeStreamCaptureMode
#define cudaThreadExchangeStreamCaptureMode                     hipThreadExchangeStreamCaptureMode
  // cuStreamGetId
#define cudaStreamGetId                                         hipStreamGetId

  // 5. Event Management
  // no analogue
  // NOTE: Not equal to cuEventCreate due to different signatures
// #define cudaEventCreate                                         hipEventCreate
#define cudaEventCreate                                         hipEventCreateWithFlags
  // cuEventCreate
#define cudaEventCreateWithFlags                                hipEventCreateWithFlags
  // cuEventDestroy
#define cudaEventDestroy                                        hipEventDestroy
  // cuEventElapsedTime
#define cudaEventElapsedTime                                    hipEventElapsedTime
  // cuEventQuery
#define cudaEventQuery                                          hipEventQuery
  // cuEventRecord
#define cudaEventRecord                                         hipEventRecord
  // cuEventSynchronize
#define cudaEventSynchronize                                    hipEventSynchronize
  // cuEventRecordWithFlags
#define cudaEventRecordWithFlags                                hipEventRecordWithFlags

  // 6. External Resource Interoperability
  // cuDestroyExternalMemory
#define cudaDestroyExternalMemory                               hipDestroyExternalMemory
  // cuDestroyExternalSemaphore
#define cudaDestroyExternalSemaphore                            hipDestroyExternalSemaphore
  // cuExternalMemoryGetMappedBuffer
#define cudaExternalMemoryGetMappedBuffer                       hipExternalMemoryGetMappedBuffer
  // cuExternalMemoryGetMappedMipmappedArray
#define cudaExternalMemoryGetMappedMipmappedArray               hipExternalMemoryGetMappedMipmappedArray
  // cuImportExternalMemory
#define cudaImportExternalMemory                                hipImportExternalMemory
  // cuImportExternalSemaphore
#define cudaImportExternalSemaphore                             hipImportExternalSemaphore
  // cuSignalExternalSemaphoresAsync
#define cudaSignalExternalSemaphoresAsync                       hipSignalExternalSemaphoresAsync
  // cuWaitExternalSemaphoresAsync
#define cudaWaitExternalSemaphoresAsync                         hipWaitExternalSemaphoresAsync

  // 7. Execution Control
  // no analogue
#define cudaFuncGetAttributes                                   hipFuncGetAttributes
  // no analogue
  // NOTE: Not equal to cuFuncSetAttribute due to different signatures
#define cudaFuncSetAttribute                                    hipFuncSetAttribute
  // no analogue
  // NOTE: Not equal to cuFuncSetCacheConfig due to different signatures
#define cudaFuncSetCacheConfig                                  hipFuncSetCacheConfig
  // no analogue
  // NOTE: Not equal to cuFuncSetSharedMemConfig due to different signatures
#define cudaFuncSetSharedMemConfig                              hipFuncSetSharedMemConfig
  // no analogue
#define cudaGetParameterBuffer                                  hipGetParameterBuffer
  // no analogue
#define cudaGetParameterBufferV2                                hipGetParameterBufferV2
  // no analogue
  // NOTE: Not equal to cuLaunchCooperativeKernel due to different signatures
#define cudaLaunchCooperativeKernel                             hipLaunchCooperativeKernel
  // no analogue
  // NOTE: Not equal to cuLaunchCooperativeKernelMultiDevice due to different signatures
#define cudaLaunchCooperativeKernelMultiDevice                  hipLaunchCooperativeKernelMultiDevice
  // cuLaunchHostFunc
#define cudaLaunchHostFunc                                      hipLaunchHostFunc
  // no analogue
  // NOTE: Not equal to cuLaunchKernel due to different signatures
#define cudaLaunchKernel                                        hipLaunchKernel
  // no analogue
#define cudaSetDoubleForDevice                                  hipSetDoubleForDevice
  // no analogue
#define cudaSetDoubleForHost                                    hipSetDoubleForHost
  // no analogue
  // NOTE: Not equal to cuLaunchKernelEx due to different signatures
#define cudaLaunchKernelExC                                     hipLaunchKernelExC
  // cuFuncGetName
#define cudaFuncGetName                                         hipFuncGetName

  // 8. Occupancy
  // cuOccupancyAvailableDynamicSMemPerBlock
#define cudaOccupancyAvailableDynamicSMemPerBlock               hipOccupancyAvailableDynamicSMemPerBlock
  // cuOccupancyMaxActiveBlocksPerMultiprocessor
#define cudaOccupancyMaxActiveBlocksPerMultiprocessor           hipOccupancyMaxActiveBlocksPerMultiprocessor
  // cuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
#define cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags  hipOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
  // cuOccupancyMaxPotentialBlockSize
#define cudaOccupancyMaxPotentialBlockSize                      hipOccupancyMaxPotentialBlockSize
  // cuOccupancyMaxPotentialBlockSizeWithFlags
#define cudaOccupancyMaxPotentialBlockSizeWithFlags             hipOccupancyMaxPotentialBlockSizeWithFlags
  // no analogue
#define cudaOccupancyMaxPotentialBlockSizeVariableSMem          hipOccupancyMaxPotentialBlockSizeVariableSMem
  // no analogue
#define cudaOccupancyMaxPotentialBlockSizeVariableSMemWithFlags hipOccupancyMaxPotentialBlockSizeVariableSMemWithFlags
  // cuOccupancyMaxPotentialClusterSize
#define cudaOccupancyMaxPotentialClusterSize                    hipOccupancyMaxPotentialClusterSize
  // cuOccupancyMaxActiveClusters
#define cudaOccupancyMaxActiveClusters                          hipOccupancyMaxActiveClusters

  // 9. Memory Management
  // no analogue
#define cudaArrayGetInfo                                        hipArrayGetInfo
  // cuMemFree
#define cudaFree                                                hipFree
  // no analogue
#define cudaFreeArray                                           hipFreeArray
  // cuMemFreeHost
#define cudaFreeHost                                            hipHostFree
  // no analogue
  // NOTE: Not equal to cuMipmappedArrayDestroy due to different signatures
#define cudaFreeMipmappedArray                                  hipFreeMipmappedArray
  // no analogue
  // NOTE: Not equal to cuMipmappedArrayGetLevel due to different signatures
#define cudaGetMipmappedArrayLevel                              hipGetMipmappedArrayLevel
  // no analogue
#define cudaGetSymbolAddress                                    hipGetSymbolAddress
  // no analogue
#define cudaGetSymbolSize                                       hipGetSymbolSize
  // cuMemHostAlloc
#define cudaHostAlloc                                           hipHostAlloc
  // cuMemHostGetDevicePointer
#define cudaHostGetDevicePointer                                hipHostGetDevicePointer
  // cuMemHostGetFlags
#define cudaHostGetFlags                                        hipHostGetFlags
  // cuMemHostRegister
#define cudaHostRegister                                        hipHostRegister
  // cuMemHostUnregister
#define cudaHostUnregister                                      hipHostUnregister
  // cuMemAlloc
#define cudaMalloc                                              hipMalloc
  // no analogue
#define cudaMalloc3D                                            hipMalloc3D
  // no analogue
#define cudaMalloc3DArray                                       hipMalloc3DArray
  // no analogue
#define cudaMallocArray                                         hipMallocArray
  // cuMemHostAlloc
#define cudaMallocHost                                          hipHostMalloc
  // cuMemAllocManaged
#define cudaMallocManaged                                       hipMallocManaged
  // no analogue
  // NOTE: Not equal to cuMipmappedArrayCreate due to different signatures
#define cudaMallocMipmappedArray                                hipMallocMipmappedArray
  // no analogue
  // NOTE: Not equal to cuMemAllocPitch due to different signatures
#define cudaMallocPitch                                         hipMallocPitch
  // cuMemAdvise
#define cudaMemAdvise                                           hipMemAdvise
  // cuMemAdvise_v2
#define cudaMemAdvise_v2                                        hipMemAdvise_v2
  // no analogue
  // NOTE: Not equal to cuMemcpy due to different signatures
#define cudaMemcpy                                              hipMemcpy
  // no analogue
  // NOTE: Not equal to cuMemcpy2D due to different signatures
#define cudaMemcpy2D                                            hipMemcpy2D
  // no analogue
#define cudaMemcpy2DArrayToArray                                hipMemcpy2DArrayToArray
  // no analogue
  // NOTE: Not equal to cuMemcpy2DAsync due to different signatures
#define cudaMemcpy2DAsync                                       hipMemcpy2DAsync
  // no analogue
#define cudaMemcpy2DFromArray                                   hipMemcpy2DFromArray
  // no analogue
#define cudaMemcpy2DFromArrayAsync                              hipMemcpy2DFromArrayAsync
  // no analogue
#define cudaMemcpy2DToArray                                     hipMemcpy2DToArray
  // no analogue
#define cudaMemcpy2DToArrayAsync                                hipMemcpy2DToArrayAsync
  // no analogue
  // NOTE: Not equal to cuMemcpy3D due to different signatures
#define cudaMemcpy3D                                            hipMemcpy3D
  // no analogue
  // NOTE: Not equal to cuMemcpy3DAsync due to different signatures
#define cudaMemcpy3DAsync                                       hipMemcpy3DAsync
  // no analogue
  // NOTE: Not equal to cuMemcpy3DPeer due to different signatures
#define cudaMemcpy3DPeer                                        hipMemcpy3DPeer
  // no analogue
  // NOTE: Not equal to cuMemcpy3DPeerAsync due to different signatures
#define cudaMemcpy3DPeerAsync                                   hipMemcpy3DPeerAsync
  // no analogue
  // NOTE: Not equal to cuMemcpyAsync due to different signatures
#define cudaMemcpyAsync                                         hipMemcpyAsync
  // no analogue
#define cudaMemcpyFromSymbol                                    hipMemcpyFromSymbol
  // no analogue
#define cudaMemcpyFromSymbolAsync                               hipMemcpyFromSymbolAsync
  // no analogue
  // NOTE: Not equal to cuMemcpyPeer due to different signatures
#define cudaMemcpyPeer                                          hipMemcpyPeer
  // no analogue
  // NOTE: Not equal to cuMemcpyPeerAsync due to different signatures
#define cudaMemcpyPeerAsync                                     hipMemcpyPeerAsync
  // no analogue
#define cudaMemcpyToSymbol                                      hipMemcpyToSymbol
  // no analogue
#define cudaMemcpyToSymbolAsync                                 hipMemcpyToSymbolAsync
  // cuMemGetInfo
#define cudaMemGetInfo                                          hipMemGetInfo
  // cuMemPrefetchAsync
#define cudaMemPrefetchAsync                                    hipMemPrefetchAsync
  // cuMemPrefetchAsync_v2
#define cudaMemPrefetchAsync_v2                                 hipMemPrefetchAsync_v2
  // cuMemRangeGetAttribute
#define cudaMemRangeGetAttribute                                hipMemRangeGetAttribute
  // cuMemRangeGetAttributes
#define cudaMemRangeGetAttributes                               hipMemRangeGetAttributes
  // cuMemsetD32 - hipMemsetD32
#define cudaMemset                                              hipMemset
  // no analogue
#define cudaMemset2D                                            hipMemset2D
  // no analogue
#define cudaMemset2DAsync                                       hipMemset2DAsync
  // no analogue
#define cudaMemset3D                                            hipMemset3D
  // no analogue
#define cudaMemset3DAsync                                       hipMemset3DAsync
  // cuMemsetD32Async
#define cudaMemsetAsync                                         hipMemsetAsync
  // no analogue
#define make_cudaExtent                                         make_hipExtent
  // no analogue
#define make_cudaPitchedPtr                                     make_hipPitchedPtr
  // no analogue
#define make_cudaPos                                            make_hipPos
  // cuArrayGetSparseProperties
#define cudaArrayGetSparseProperties                            hipArrayGetSparseProperties
  // cuArrayGetPlane
#define cudaArrayGetPlane                                       hipArrayGetPlane
  // cuArrayGetMemoryRequirements
#define cudaArrayGetMemoryRequirements                          hipArrayGetMemoryRequirements

  // 10. Memory Management [DEPRECATED]
  // no analogue
  // NOTE: Not equal to cuMemcpyAtoA due to different signatures
#define cudaMemcpyArrayToArray                                  hipMemcpyArrayToArray
  // no analogue
#define cudaMemcpyFromArray                                     hipMemcpyFromArray
  // no analogue
#define cudaMemcpyFromArrayAsync                                hipMemcpyFromArrayAsync
  // no analogue
#define cudaMemcpyToArray                                       hipMemcpyToArray
  // no analogue
#define cudaMemcpyToArrayAsync                                  hipMemcpyToArrayAsync

  // 11. Stream Ordered Memory Allocator

  // cuMemAllocAsync
#define cudaMallocAsync                                         hipMallocAsync
  // cuMemFreeAsync
#define cudaFreeAsync                                           hipFreeAsync
  // cuMemAllocFromPoolAsync
#define cudaMallocFromPoolAsync                                 hipMallocFromPoolAsync
  // cuMemPoolTrimTo
#define cudaMemPoolTrimTo                                       hipMemPoolTrimTo
  // cuMemPoolSetAttribute
#define cudaMemPoolSetAttribute                                 hipMemPoolSetAttribute
  // cuMemPoolGetAttribute
#define cudaMemPoolGetAttribute                                 hipMemPoolGetAttribute
  // cuMemPoolSetAccess
#define cudaMemPoolSetAccess                                    hipMemPoolSetAccess
  // cuMemPoolGetAccess
#define cudaMemPoolGetAccess                                    hipMemPoolGetAccess
  // cuMemPoolCreate
#define cudaMemPoolCreate                                       hipMemPoolCreate
  // cuMemPoolDestroy
#define cudaMemPoolDestroy                                      hipMemPoolDestroy
  // cuMemPoolExportToShareableHandle
#define cudaMemPoolExportToShareableHandle                      hipMemPoolExportToShareableHandle
  // cuMemPoolImportFromShareableHandle
#define cudaMemPoolImportFromShareableHandle                    hipMemPoolImportFromShareableHandle
  // cuMemPoolExportPointer
#define cudaMemPoolExportPointer                                hipMemPoolExportPointer
  // cuMemPoolImportPointer
#define cudaMemPoolImportPointer                                hipMemPoolImportPointer

  // 12. Unified Addressing
  // no analogue
  // NOTE: Not equal to cuPointerGetAttributes due to different signatures
#define cudaPointerGetAttributes                                hipPointerGetAttributes

  // 13. Peer Device Memory Access
  // cuDeviceCanAccessPeer
#define cudaDeviceCanAccessPeer                                 hipDeviceCanAccessPeer
  // no analogue
  // NOTE: Not equal to cuCtxDisablePeerAccess due to different signatures
#define cudaDeviceDisablePeerAccess                             hipDeviceDisablePeerAccess
  // no analogue
  // NOTE: Not equal to cuCtxEnablePeerAccess due to different signatures
#define cudaDeviceEnablePeerAccess                              hipDeviceEnablePeerAccess

  // 14. OpenGL Interoperability
  // cuGLGetDevices
#define cudaGLGetDevices                                        hipGLGetDevices
  // cuGraphicsGLRegisterBuffer
#define cudaGraphicsGLRegisterBuffer                            hipGraphicsGLRegisterBuffer
  // cuGraphicsGLRegisterImage
#define cudaGraphicsGLRegisterImage                             hipGraphicsGLRegisterImage
  // cuWGLGetDevice
#define cudaWGLGetDevice                                        hipWGLGetDevice

  // 15. OpenGL Interoperability [DEPRECATED]
  // no analogue
  // NOTE: Not equal to cuGLMapBufferObject due to different signatures
#define cudaGLMapBufferObject                                   hipGLMapBufferObject
  // no analogue
  // NOTE: Not equal to cuGLMapBufferObjectAsync due to different signatures
#define cudaGLMapBufferObjectAsync                              hipGLMapBufferObjectAsync
  // cuGLRegisterBufferObject
#define cudaGLRegisterBufferObject                              hipGLRegisterBufferObject
  // cuGLSetBufferObjectMapFlags
#define cudaGLSetBufferObjectMapFlags                           hipGLSetBufferObjectMapFlags
  // no analogue
#define cudaGLSetGLDevice                                       hipGLSetGLDevice
  // cuGLUnmapBufferObject
#define cudaGLUnmapBufferObject                                 hipGLUnmapBufferObject
  // cuGLUnmapBufferObjectAsync
#define cudaGLUnmapBufferObjectAsync                            hipGLUnmapBufferObjectAsync
  // cuGLUnregisterBufferObject
#define cudaGLUnregisterBufferObject                            hipGLUnregisterBufferObject

  // 16. Direct3D 9 Interoperability
  // cuD3D9GetDevice
#define cudaD3D9GetDevice                                       hipD3D9GetDevice
  // cuD3D9GetDevices
#define cudaD3D9GetDevices                                      hipD3D9GetDevices
  // cuD3D9GetDirect3DDevice
#define cudaD3D9GetDirect3DDevice                               hipD3D9GetDirect3DDevice
  // no analogue
#define cudaD3D9SetDirect3DDevice                               hipD3D9SetDirect3DDevice
  // cuGraphicsD3D9RegisterResource
#define cudaGraphicsD3D9RegisterResource                        hipGraphicsD3D9RegisterResource

  // 17. Direct3D 9 Interoperability[DEPRECATED]
  // cuD3D9MapResources
#define cudaD3D9MapResources                                    hipD3D9MapResources
  // cuD3D9RegisterResource
  // NOTE: cudaD3D9RegisterResource is not marked as deprecated function even in CUDA 11.0
#define cudaD3D9RegisterResource                                hipD3D9RegisterResource
  // cuD3D9ResourceGetMappedArray
#define cudaD3D9ResourceGetMappedArray                          hipD3D9ResourceGetMappedArray
  // cuD3D9ResourceGetMappedPitch
#define cudaD3D9ResourceGetMappedPitch                          hipD3D9ResourceGetMappedPitch
  // cuD3D9ResourceGetMappedPointer
#define cudaD3D9ResourceGetMappedPointer                        hipD3D9ResourceGetMappedPointer
  // cuD3D9ResourceGetMappedSize
#define cudaD3D9ResourceGetMappedSize                           hipD3D9ResourceGetMappedSize
  // cuD3D9ResourceGetSurfaceDimensions
#define cudaD3D9ResourceGetSurfaceDimensions                    hipD3D9ResourceGetSurfaceDimensions
  // cuD3D9ResourceSetMapFlags
#define cudaD3D9ResourceSetMapFlags                             hipD3D9ResourceSetMapFlags
  // cuD3D9UnmapResources
#define cudaD3D9UnmapResources                                  hipD3D9UnmapResources
  // cuD3D9UnregisterResource
#define cudaD3D9UnregisterResource                              hipD3D9UnregisterResource

  // 18. Direct3D 10 Interoperability
  // cuD3D10GetDevice
#define cudaD3D10GetDevice                                      hipD3D10GetDevice
  // cuD3D10GetDevices
#define cudaD3D10GetDevices                                     hipD3D10GetDevices
  // cuGraphicsD3D10RegisterResource
#define cudaGraphicsD3D10RegisterResource                       hipGraphicsD3D10RegisterResource

  // 19. Direct3D 10 Interoperability [DEPRECATED]
  // cuD3D10GetDirect3DDevice
#define cudaD3D10GetDirect3DDevice                              hipD3D10GetDirect3DDevice
  // cuD3D10MapResources
#define cudaD3D10MapResources                                   hipD3D10MapResources
  // cuD3D10RegisterResource
#define cudaD3D10RegisterResource                               hipD3D10RegisterResource
  // cuD3D10ResourceGetMappedArray
#define cudaD3D10ResourceGetMappedArray                         hipD3D10ResourceGetMappedArray
  // cuD3D10ResourceGetMappedPitch
#define cudaD3D10ResourceGetMappedPitch                         hipD3D10ResourceGetMappedPitch
  // cuD3D10ResourceGetMappedPointer
#define cudaD3D10ResourceGetMappedPointer                       hipD3D10ResourceGetMappedPointer
  // cuD3D10ResourceGetMappedSize
#define cudaD3D10ResourceGetMappedSize                          hipD3D10ResourceGetMappedSize
  // cuD3D10ResourceGetSurfaceDimensions
#define cudaD3D10ResourceGetSurfaceDimensions                   hipD3D10ResourceGetSurfaceDimensions
  // cuD3D10ResourceSetMapFlags
#define cudaD3D10ResourceSetMapFlags                            hipD3D10ResourceSetMapFlags
  // no analogue
#define cudaD3D10SetDirect3DDevice                              hipD3D10SetDirect3DDevice
  // cuD3D10UnmapResources
#define cudaD3D10UnmapResources                                 hipD3D10UnmapResources
  // cuD3D10UnregisterResource
#define cudaD3D10UnregisterResource                             hipD3D10UnregisterResource

  // 20. Direct3D 11 Interoperability
  // cuD3D11GetDevice
#define cudaD3D11GetDevice                                      hipD3D11GetDevice
  // cuD3D11GetDevices
#define cudaD3D11GetDevices                                     hipD3D11GetDevices
  // cuGraphicsD3D11RegisterResource
#define cudaGraphicsD3D11RegisterResource                       hipGraphicsD3D11RegisterResource

  // 21. Direct3D 11 Interoperability [DEPRECATED]
  // cuD3D11GetDirect3DDevice
#define cudaD3D11GetDirect3DDevice                              hipD3D11GetDirect3DDevice
  // no analogue
#define cudaD3D11SetDirect3DDevice                              hipD3D11SetDirect3DDevice

  // 22. VDPAU Interoperability
  // cuGraphicsVDPAURegisterOutputSurface
#define cudaGraphicsVDPAURegisterOutputSurface                  hipGraphicsVDPAURegisterOutputSurface
  // cuGraphicsVDPAURegisterVideoSurface
#define cudaGraphicsVDPAURegisterVideoSurface                   hipGraphicsVDPAURegisterVideoSurface
  // cuVDPAUGetDevice
#define cudaVDPAUGetDevice                                      hipVDPAUGetDevice
  // no analogue
#define cudaVDPAUSetVDPAUDevice                                 hipVDPAUSetDevice

  // 23. EGL Interoperability
  // cuEGLStreamConsumerAcquireFrame
#define cudaEGLStreamConsumerAcquireFrame                       hipEGLStreamConsumerAcquireFrame
  // cuEGLStreamConsumerConnect
#define cudaEGLStreamConsumerConnect                            hipEGLStreamConsumerConnect
  // cuEGLStreamConsumerConnectWithFlags
#define cudaEGLStreamConsumerConnectWithFlags                   hipEGLStreamConsumerConnectWithFlags
  // cuEGLStreamConsumerDisconnect
#define cudaEGLStreamConsumerDisconnect                         hipEGLStreamConsumerDisconnect
  // cuEGLStreamConsumerReleaseFrame
#define cudaEGLStreamConsumerReleaseFrame                       hipEGLStreamConsumerReleaseFrame
  // cuEGLStreamProducerConnect
#define cudaEGLStreamProducerConnect                            hipEGLStreamProducerConnect
  // cuEGLStreamProducerDisconnect
#define cudaEGLStreamProducerDisconnect                         hipEGLStreamProducerDisconnect
  // cuEGLStreamProducerPresentFrame
#define cudaEGLStreamProducerPresentFrame                       hipEGLStreamProducerPresentFrame
  // cuEGLStreamProducerReturnFrame
#define cudaEGLStreamProducerReturnFrame                        hipEGLStreamProducerReturnFrame
  // cuEventCreateFromEGLSync
#define cudaEventCreateFromEGLSync                              hipEventCreateFromEGLSync
  // cuGraphicsEGLRegisterImage
#define cudaGraphicsEGLRegisterImage                            hipGraphicsEGLRegisterImage
  // cuGraphicsResourceGetMappedEglFrame
#define cudaGraphicsResourceGetMappedEglFrame                   hipGraphicsResourceGetMappedEglFrame

  // 24. Graphics Interoperability
  // cuGraphicsMapResources
#define cudaGraphicsMapResources                                hipGraphicsMapResources
  // cuGraphicsResourceGetMappedMipmappedArray
#define cudaGraphicsResourceGetMappedMipmappedArray             hipGraphicsResourceGetMappedMipmappedArray
  // cuGraphicsResourceGetMappedPointer
#define cudaGraphicsResourceGetMappedPointer                    hipGraphicsResourceGetMappedPointer
  // cuGraphicsResourceSetMapFlags
#define cudaGraphicsResourceSetMapFlags                         hipGraphicsResourceSetMapFlags
  // cuGraphicsSubResourceGetMappedArray
#define cudaGraphicsSubResourceGetMappedArray                   hipGraphicsSubResourceGetMappedArray
  // cuGraphicsUnmapResources
#define cudaGraphicsUnmapResources                              hipGraphicsUnmapResources
  // cuGraphicsUnregisterResource
#define cudaGraphicsUnregisterResource                          hipGraphicsUnregisterResource

  // 25. Texture Object Management
  // no analogue
  // NOTE: Not equal to cuTexObjectCreate due to different signatures
#define cudaCreateTextureObject                                 hipCreateTextureObject
  // cuTexObjectDestroy
#define cudaDestroyTextureObject                                hipDestroyTextureObject
  // no analogue
  // NOTE: Not equal to cuTexObjectGetResourceDesc due to different signatures
#define cudaGetTextureObjectResourceDesc                        hipGetTextureObjectResourceDesc
  // cuTexObjectGetResourceViewDesc
#define cudaGetTextureObjectResourceViewDesc                    hipGetTextureObjectResourceViewDesc
  // no analogue
  // NOTE: Not equal to cuTexObjectGetTextureDesc due to different signatures
#define cudaGetTextureObjectTextureDesc                         hipGetTextureObjectTextureDesc
  //
#define cudaCreateTextureObject_v2                              hipCreateTextureObject_v2
  //
#define cudaGetTextureObjectTextureDesc_v2                      hipGetTextureObjectTextureDesc_v2
  // no analogue
#define cudaCreateChannelDesc                                   hipCreateChannelDesc
  // no analogue
#define cudaGetChannelDesc                                      hipGetChannelDesc

  // 26. Surface Object Management
  // no analogue
  // NOTE: Not equal to cuSurfObjectCreate due to different signatures
#define cudaCreateSurfaceObject                                 hipCreateSurfaceObject
  // cuSurfObjectDestroy
#define cudaDestroySurfaceObject                                hipDestroySurfaceObject
  // no analogue
  // NOTE: Not equal to cuSurfObjectGetResourceDesc due to different signatures
#define cudaGetSurfaceObjectResourceDesc                        hipGetSurfaceObjectResourceDesc

  // 27. Version Management
  // cuDriverGetVersion
#define cudaDriverGetVersion                                    hipDriverGetVersion
  // no analogue
#define cudaRuntimeGetVersion                                   hipRuntimeGetVersion

  // 28. Graph Management
  // cuGraphAddChildGraphNode
#define cudaGraphAddChildGraphNode                              hipGraphAddChildGraphNode
  // cuGraphAddDependencies
#define cudaGraphAddDependencies                                hipGraphAddDependencies
  // cuGraphAddDependencies_v2
#define cudaGraphAddDependencies_v2                             hipGraphAddDependencies_v2
  // cuGraphAddEmptyNode
#define cudaGraphAddEmptyNode                                   hipGraphAddEmptyNode
  // cuGraphAddHostNode
#define cudaGraphAddHostNode                                    hipGraphAddHostNode
  // cuGraphAddKernelNode
#define cudaGraphAddKernelNode                                  hipGraphAddKernelNode
  // no analogue
  // NOTE: Not equal to cuGraphAddMemcpyNode due to different signatures:
  // DRIVER: CUresult CUDAAPI cuGraphAddMemcpyNode(CUgraphNode *phGraphNode, CUgraph hGraph, const CUgraphNode *dependencies, size_t numDependencies, const CUDA_MEMCPY3D *copyParams, CUcontext ctx);
  // RUNTIME: cudaError_t CUDARTAPI cudaGraphAddMemcpyNode(cudaGraphNode_t *pGraphNode, cudaGraph_t graph, const cudaGraphNode_t *pDependencies, size_t numDependencies, const struct cudaMemcpy3DParms *pCopyParams);
#define cudaGraphAddMemcpyNode                                  hipGraphAddMemcpyNode
  // no analogue
  // NOTE: Not equal to cuGraphAddMemsetNode due to different signatures:
  // DRIVER: CUresult CUDAAPI cuGraphAddMemsetNode(CUgraphNode *phGraphNode, CUgraph hGraph, const CUgraphNode *dependencies, size_t numDependencies, const CUDA_MEMSET_NODE_PARAMS *memsetParams, CUcontext ctx);
  // RUNTIME: cudaError_t CUDARTAPI cudaGraphAddMemcpyNode(cudaGraphNode_t *pGraphNode, cudaGraph_t graph, const cudaGraphNode_t *pDependencies, size_t numDependencies, const struct cudaMemcpy3DParms *pCopyParams);
#define cudaGraphAddMemsetNode                                  hipGraphAddMemsetNode
  // cuGraphChildGraphNodeGetGraph
#define cudaGraphChildGraphNodeGetGraph                         hipGraphChildGraphNodeGetGraph
  // cuGraphClone
#define cudaGraphClone                                          hipGraphClone
  // cuGraphCreate
#define cudaGraphCreate                                         hipGraphCreate
  // cuGraphDebugDotPrint
#define cudaGraphDebugDotPrint                                  hipGraphDebugDotPrint
  // cuGraphDestroy
#define cudaGraphDestroy                                        hipGraphDestroy
  // cuGraphDestroyNode
#define cudaGraphDestroyNode                                    hipGraphDestroyNode
  // cuGraphExecDestroy
#define cudaGraphExecDestroy                                    hipGraphExecDestroy
  // cuGraphGetEdges
#define cudaGraphGetEdges                                       hipGraphGetEdges
  // cuGraphGetEdges_v2
#define cudaGraphGetEdges_v2                                    hipGraphGetEdges_v2
  // cuGraphGetNodes
#define cudaGraphGetNodes                                       hipGraphGetNodes
  // cuGraphGetRootNodes
#define cudaGraphGetRootNodes                                   hipGraphGetRootNodes
  // cuGraphHostNodeGetParams
#define cudaGraphHostNodeGetParams                              hipGraphHostNodeGetParams
  // cuGraphHostNodeSetParams
#define cudaGraphHostNodeSetParams                              hipGraphHostNodeSetParams
  // cuGraphInstantiate
  // NOTE: CUDA signature changed since 12.0
#define cudaGraphInstantiate                                    hipGraphInstantiate
  // cuGraphKernelNodeCopyAttributes
#define cudaGraphKernelNodeCopyAttributes                       hipGraphKernelNodeCopyAttributes
  // cuGraphKernelNodeGetAttribute
#define cudaGraphKernelNodeGetAttribute                         hipGraphKernelNodeGetAttribute
  // cuGraphKernelNodeSetAttribute
#define cudaGraphKernelNodeSetAttribute                         hipGraphKernelNodeSetAttribute
  // cuGraphExecKernelNodeSetParams
#define cudaGraphExecKernelNodeSetParams                        hipGraphExecKernelNodeSetParams
  // no analogue
  // NOTE: Not equal to cuGraphExecMemcpyNodeSetParams due to different signatures:
  // DRIVER: CUresult CUDAAPI cuGraphExecMemcpyNodeSetParams(CUgraphExec hGraphExec, CUgraphNode hNode, const CUDA_MEMCPY3D *copyParams, CUcontext ctx);
  // RUNTIME: cudaError_t CUDARTAPI cudaGraphExecMemcpyNodeSetParams(cudaGraphExec_t hGraphExec, cudaGraphNode_t node, const struct cudaMemcpy3DParms *pNodeParams);
#define cudaGraphExecMemcpyNodeSetParams                        hipGraphExecMemcpyNodeSetParams
  // no analogue
  // NOTE: Not equal to cuGraphExecMemsetNodeSetParams due to different signatures:
  // DRIVER: CUresult CUDAAPI cuGraphExecMemsetNodeSetParams(CUgraphExec hGraphExec, CUgraphNode hNode, const CUDA_MEMSET_NODE_PARAMS *memsetParams, CUcontext ctx);
  // RUNTIME: cudaError_t CUDARTAPI cudaGraphExecMemsetNodeSetParams(cudaGraphExec_t hGraphExec, cudaGraphNode_t node, const struct cudaMemsetParams *pNodeParams);
#define cudaGraphExecMemsetNodeSetParams                        hipGraphExecMemsetNodeSetParams
  // cuGraphExecHostNodeSetParams
#define cudaGraphExecHostNodeSetParams                          hipGraphExecHostNodeSetParams
  // cuGraphExecUpdate
  // NOTE: CUDA signature changed since 12.0
#define cudaGraphExecUpdate                                     hipGraphExecUpdate
  // cuGraphKernelNodeGetParams
#define cudaGraphKernelNodeGetParams                            hipGraphKernelNodeGetParams
  // cuGraphKernelNodeSetParams
#define cudaGraphKernelNodeSetParams                            hipGraphKernelNodeSetParams
  // cuGraphLaunch
#define cudaGraphLaunch                                         hipGraphLaunch
  // cuGraphMemcpyNodeGetParams
#define cudaGraphMemcpyNodeGetParams                            hipGraphMemcpyNodeGetParams
  // cuGraphMemcpyNodeSetParams
#define cudaGraphMemcpyNodeSetParams                            hipGraphMemcpyNodeSetParams
  // cuGraphMemsetNodeGetParams
#define cudaGraphMemsetNodeGetParams                            hipGraphMemsetNodeGetParams
  // cuGraphMemsetNodeSetParams
#define cudaGraphMemsetNodeSetParams                            hipGraphMemsetNodeSetParams
  // cuGraphNodeFindInClone
#define cudaGraphNodeFindInClone                                hipGraphNodeFindInClone
  // cuGraphNodeGetDependencies
#define cudaGraphNodeGetDependencies                            hipGraphNodeGetDependencies
  // cuGraphNodeGetDependencies_v2
#define cudaGraphNodeGetDependencies_v2                         hipGraphNodeGetDependencies_v2
  // cuGraphNodeGetDependentNodes
#define cudaGraphNodeGetDependentNodes                          hipGraphNodeGetDependentNodes
  // cuGraphNodeGetDependentNodes_v2
#define cudaGraphNodeGetDependentNodes_v2                       hipGraphNodeGetDependentNodes_v2
  // cuGraphNodeGetEnabled
#define cudaGraphNodeGetEnabled                                 hipGraphNodeGetEnabled
  // cuGraphNodeGetType
#define cudaGraphNodeGetType                                    hipGraphNodeGetType
  // cuGraphRemoveDependencies
#define cudaGraphRemoveDependencies                             hipGraphRemoveDependencies
  // cuGraphRemoveDependencies_v2
#define cudaGraphRemoveDependencies_v2                          hipGraphRemoveDependencies_v2
  // no analogue
#define cudaGraphAddMemcpyNodeToSymbol                          hipGraphAddMemcpyNodeToSymbol
  // no analogue
#define cudaGraphAddMemcpyNodeFromSymbol                        hipGraphAddMemcpyNodeFromSymbol
  // no analogue
#define cudaGraphAddMemcpyNode1D                                hipGraphAddMemcpyNode1D
  // no analogue
#define cudaGraphMemcpyNodeSetParamsToSymbol                    hipGraphMemcpyNodeSetParamsToSymbol
  // no analogue
#define cudaGraphMemcpyNodeSetParamsFromSymbol                  hipGraphMemcpyNodeSetParamsFromSymbol
  // no analogue
#define cudaGraphMemcpyNodeSetParams1D                          hipGraphMemcpyNodeSetParams1D
  // cuGraphAddEventRecordNode
#define cudaGraphAddEventRecordNode                             hipGraphAddEventRecordNode
  // cuGraphEventRecordNodeGetEvent
#define cudaGraphEventRecordNodeGetEvent                        hipGraphEventRecordNodeGetEvent
  // cuGraphEventRecordNodeSetEvent
#define cudaGraphEventRecordNodeSetEvent                        hipGraphEventRecordNodeSetEvent
  // cuGraphAddEventWaitNode
#define cudaGraphAddEventWaitNode                               hipGraphAddEventWaitNode
  // cuGraphEventWaitNodeGetEvent
#define cudaGraphEventWaitNodeGetEvent                          hipGraphEventWaitNodeGetEvent
  // cuGraphEventWaitNodeSetEvent
#define cudaGraphEventWaitNodeSetEvent                          hipGraphEventWaitNodeSetEvent
  // no analogue
#define cudaGraphExecMemcpyNodeSetParamsToSymbol                hipGraphExecMemcpyNodeSetParamsToSymbol
  // no analogue
#define cudaGraphExecMemcpyNodeSetParamsFromSymbol              hipGraphExecMemcpyNodeSetParamsFromSymbol
  // no analogue
#define cudaGraphExecMemcpyNodeSetParams1D                      hipGraphExecMemcpyNodeSetParams1D
  // cuGraphExecChildGraphNodeSetParams
#define cudaGraphExecChildGraphNodeSetParams                    hipGraphExecChildGraphNodeSetParams
  // cuGraphExecEventRecordNodeSetEvent
#define cudaGraphExecEventRecordNodeSetEvent                    hipGraphExecEventRecordNodeSetEvent
  // cuGraphExecEventWaitNodeSetEvent
#define cudaGraphExecEventWaitNodeSetEvent                      hipGraphExecEventWaitNodeSetEvent
  // cuGraphUpload
#define cudaGraphUpload                                         hipGraphUpload
  // cuGraphAddExternalSemaphoresSignalNode
#define cudaGraphAddExternalSemaphoresSignalNode                hipGraphAddExternalSemaphoresSignalNode
  // cuGraphExternalSemaphoresSignalNodeGetParams
#define cudaGraphExternalSemaphoresSignalNodeGetParams          hipGraphExternalSemaphoresSignalNodeGetParams
  // cuGraphExternalSemaphoresSignalNodeSetParams
#define cudaGraphExternalSemaphoresSignalNodeSetParams          hipGraphExternalSemaphoresSignalNodeSetParams
  // cuGraphAddExternalSemaphoresWaitNode
#define cudaGraphAddExternalSemaphoresWaitNode                  hipGraphAddExternalSemaphoresWaitNode
  // cuGraphExternalSemaphoresWaitNodeGetParams
#define cudaGraphExternalSemaphoresWaitNodeGetParams            hipGraphExternalSemaphoresWaitNodeGetParams
  // cuGraphExternalSemaphoresWaitNodeSetParams
#define cudaGraphExternalSemaphoresWaitNodeSetParams            hipGraphExternalSemaphoresWaitNodeSetParams
  // cuGraphExecExternalSemaphoresSignalNodeSetParams
#define cudaGraphExecExternalSemaphoresSignalNodeSetParams      hipGraphExecExternalSemaphoresSignalNodeSetParams
  // cuGraphExecExternalSemaphoresWaitNodeSetParams
#define cudaGraphExecExternalSemaphoresWaitNodeSetParams        hipGraphExecExternalSemaphoresWaitNodeSetParams
  // cuUserObjectCreate
#define cudaUserObjectCreate                                    hipUserObjectCreate
  // cuUserObjectRetain
#define cudaUserObjectRetain                                    hipUserObjectRetain
  // cuUserObjectRelease
#define cudaUserObjectRelease                                   hipUserObjectRelease
  // cuGraphRetainUserObject
#define cudaGraphRetainUserObject                               hipGraphRetainUserObject
  // cuGraphReleaseUserObject
#define cudaGraphReleaseUserObject                              hipGraphReleaseUserObject
  // cuGraphAddMemAllocNode
#define cudaGraphAddMemAllocNode                                hipGraphAddMemAllocNode
  // cuGraphMemAllocNodeGetParams
#define cudaGraphMemAllocNodeGetParams                          hipGraphMemAllocNodeGetParams
  // cuGraphAddMemFreeNode
#define cudaGraphAddMemFreeNode                                 hipGraphAddMemFreeNode
  // cuGraphMemFreeNodeGetParams
#define cudaGraphMemFreeNodeGetParams                           hipGraphMemFreeNodeGetParams
  // cuDeviceGraphMemTrim
#define cudaDeviceGraphMemTrim                                  hipDeviceGraphMemTrim
  // cuDeviceGetGraphMemAttribute
#define cudaDeviceGetGraphMemAttribute                          hipDeviceGetGraphMemAttribute
  // cuDeviceSetGraphMemAttribute
#define cudaDeviceSetGraphMemAttribute                          hipDeviceSetGraphMemAttribute
  // cuGraphInstantiateWithFlags
  // NOTE: CUDA signature changed since 12.0
#define cudaGraphInstantiateWithFlags                           hipGraphInstantiateWithFlags
  // cuGraphNodeSetEnabled
#define cudaGraphNodeSetEnabled                                 hipGraphNodeSetEnabled
  // cuGraphInstantiateWithParams
#define cudaGraphInstantiateWithParams                          hipGraphInstantiateWithParams
  // cuGraphExecGetFlags
#define cudaGraphExecGetFlags                                   hipGraphExecGetFlags
  // cuGraphAddNode
#define cudaGraphAddNode                                        hipGraphAddNode
  // cuGraphAddNode_v2
#define cudaGraphAddNode_v2                                     hipGraphAddNode_v2
  // cuGraphNodeSetParams
#define cudaGraphNodeSetParams                                  hipGraphNodeSetParams
  // cuGraphExecNodeSetParams
#define cudaGraphExecNodeSetParams                              hipGraphExecNodeSetParams
  // cuGraphConditionalHandleCreate
#define cudaGraphConditionalHandleCreate                        hipGraphConditionalHandleCreate

  // 29. Driver Entry Point Access
  // cuGetProcAddress
#define cudaGetDriverEntryPoint                                 hipGetProcAddress

  // 30. C++ API Routines
#define cudaGetKernel                                           hipGetKernel

  // 31. Interactions with the CUDA Driver API
#define cudaGetFuncBySymbol                                     hipGetFuncBySymbol

  // 32. Profiler Control
  // cuProfilerStart
#define cudaProfilerStart                                       hipProfilerStart
  // cuProfilerStop
#define cudaProfilerStop                                        hipProfilerStop

  // 33. Data types used by CUDA Runtime
  // NOTE: in a separate file

  // 34. Execution Control [REMOVED]
  // NOTE: Removed in CUDA 10.1
  // no analogue
#define cudaConfigureCall                                       hipConfigureCall
  // no analogue
  // NOTE: Not equal to cuLaunch due to different signatures
#define cudaLaunch                                              hipLaunchByPtr
  // no analogue
#define cudaSetupArgument                                       hipSetupArgument

  // 35. Texture Reference Management [REMOVED]
  // NOTE: Removed in CUDA 12.0
  // no analogue
#define cudaBindTexture                                         hipBindTexture
  // no analogue
#define cudaBindTexture2D                                       hipBindTexture2D
  // no analogue
#define cudaBindTextureToArray                                  hipBindTextureToArray
  // no analogue
#define cudaBindTextureToMipmappedArray                         hipBindTextureToMipmappedArray
  // no analogue
#define cudaGetTextureAlignmentOffset                           hipGetTextureAlignmentOffset
  // no analogue
#define cudaGetTextureReference                                 hipGetTextureReference
  // no analogue
#define cudaUnbindTexture                                       hipUnbindTexture

  // 36. Surface Reference Management [REMOVED]
  // NOTE: Removed in CUDA 12.0
  // no analogue
#define cudaBindSurfaceToArray                                  hipBindSurfaceToArray
  // no analogue
#define cudaGetSurfaceReference                                 hipGetSurfaceReference

  // 37. Profiler Control [REMOVED]
  // cuProfilerInitialize
#define cudaProfilerInitialize                                  hipProfilerInitialize



#define hipHostAlloc hipHostMalloc


#endif