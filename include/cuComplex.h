
#pragma once

#define ROCM_MATHLIBS_API_USE_HIP_COMPLEX

#include <hip/hip_complex.h>

#define cuFloatComplex  hipFloatComplex
#define cuDoubleComplex hipDoubleComplex
#define cuComplex       hipComplex

#define cuCrealf               hipCrealf
#define cuCimagf               hipCimagf
#define make_cuFloatComplex    make_hipFloatComplex
#define cuConjf                hipConjf
#define cuCaddf                hipCaddf
#define cuCsubf                hipCsubf
#define cuCmulf                hipCmulf
#define cuCdivf                hipCdivf
#define cuCabsf                hipCabsf
#define cuCreal                hipCreal
#define cuCimag                hipCimag
#define make_cuDoubleComplex   make_hipDoubleComplex
#define cuConj                 hipConj
#define cuCadd                 hipCadd
#define cuCsub                 hipCsub
#define cuCmul                 hipCmul
#define cuCdiv                 hipCdiv
#define cuCabs                 hipCabs
#define make_cuComplex         make_hipComplex
#define cuComplexFloatToDouble hipComplexFloatToDoubl
#define cuComplexDoubleToFloat hipComplexDoubleToFloat
#define cuCfmaf                hipCfmaf
#define cuCfma                 hipCfma
